<?xml version="1.0" encoding="UTF-8"?>
<cherrytree>
  <bookmarks list=""/>
  <node name="FirstRib Linux" unique_id="1" prog_lang="custom-colors" tags="" readonly="0" nosearch_me="0" nosearch_ch="0" custom_icon_id="0" is_bold="0" foreground="" ts_creation="1667721392" ts_lastsave="1667721425">
    <node name="Core Build System Notes" unique_id="2" prog_lang="custom-colors" tags="" readonly="0" nosearch_me="0" nosearch_ch="0" custom_icon_id="0" is_bold="0" foreground="" ts_creation="1667721425" ts_lastsave="1667727025">
      <rich_text scale="h2" weight="heavy">Building a firstrib linux system is simplicity itself</rich_text>
      <rich_text>

</rich_text>
      <rich_text scale="h3" weight="heavy">There are really only two main steps:</rich_text>
      <rich_text>

</rich_text>
      <rich_text weight="heavy">1</rich_text>
      <rich_text>. The main part of a frugally installed Linux distro is its root filesystem, which you build with the single FirstRib build system script named build_firstrib_rootfs.sh (though you normally also include a simple text file plugin, which ‘tells’ build_firstrib_rootfs.sh what extra packages and configurations you want to include in your build design).

</rich_text>
      <rich_text weight="heavy">2</rich_text>
      <rich_text>. Configuring your favourite boot loader (such as grub2, limine, or grub4dos) to boot your root filesystem build via FirstRib magic initrd (which has often been named initrd.gz, but FirstRib system is now moving towards using the more generic “initrd.img” name that might be compressed with any of gz, or xz, or zst or whatever...).

For example:

To build a FirstRib Arch Linux based system, simply put a copy of the build_firstrib_rootfs.sh script, along with a simple text plugin, into an empty directory in which you wish to create your frugal installed distro and run it on your Linux host system with command:

  </rich_text>
      <rich_text style="italic">./build_firstrib_rootfs.sh arch default amd64 f_&lt;some_Arch_Linux_based&gt;.plug</rich_text>
      <rich_text>
  
For brief script usage help simply use command: </rich_text>
      <rich_text style="italic">./build_firstrib_rootfs.sh --help</rich_text>
      <rich_text>
  
That simple text .plug file (f_&lt;some_Arch_Linux_based&gt;.plug), for the most part, simply contains extra packages you wish to install in your build along with suitable configuration commands.

Then sit back, drink a coffee or whatever, and your Arch Linux based FirstRib distro root filesystem will be automatically built for you ready for booting via FirstRib initrd
  
</rich_text>
      <rich_text scale="h3">So, to build a brand new up-to-date new root filesystem for the Arch-based Openshot/Tint2 downloadable distro FR_AOT2 you just need to run the following command on your Linux host system:</rich_text>
      <rich_text>
  
  </rich_text>
      <rich_text style="italic" weight="heavy">./build_firstrib_rootfs.sh arch default amd64 f_00_Arch_amd64-openboxFull_jgmenu_&lt;version-release&gt;.plug</rich_text>
      <rich_text>

Of course, if thus re-building Your FirstRib distro's root filesystem, you will likely want to delete any previous old root filesystem (e.g. rm 07firstrib_rootfs.sfs or rm -rf 07firstrib_rootfs/ or whatever you called it).

In fact if you have already downloaded an FR_AOT2 iso </rich_text>
      <rich_text weight="heavy">you can find a copy of the build script components, used at the time it was built, provided in /root/FirstRib_build_scripts.</rich_text>
      <rich_text> Note however that all FirstRib components are maintained and developed with newest versions of all such scripts and helper utilities being found at: </rich_text>
      <rich_text link="webs https://gitlab.com/firstrib/firstrib/latest">https://gitlab.com/firstrib/firstrib/latest</rich_text>
      <rich_text>. You can of course modify the build plug file prior to using it, which allows you to add or remove applications and/or change configuration details to suit your wishes.

</rich_text>
      <rich_text scale="h3">IMPORTANT FINAL STEPS PRIOR TO BOOTING:</rich_text>
      <rich_text>

</rich_text>
      <rich_text weight="heavy">a</rich_text>
      <rich_text>. You need to add a two-digit number to the front of whatever the root filesystem's name is. For example, the automatically created firstrib_rootfs/ needs to be renamed to, say, 08firstrib_rootfs. That two-digit number tells the initrd overlayfs setup what layer that firstrib_rootfs will be mounted at (07 or 08 are common choices to allow room for addon layers both above and below).

</rich_text>
      <rich_text weight="heavy">b</rich_text>
      <rich_text>. Your root filesystem must be supplemented with a suitable kernel, firmware and modules. These can be provided as packages installed by the build plug file. A copy of the vmlinuz thus installed needs to be provided in the main build directory (outside of the root filesystem build). An alternative way to provide the vmlinuz/modules and firmware is via the likes of a huge vmlinuz kernel along with, for example, two addons 00modules and 01firmware (that will thus occupy overlayfs layers 00 and 01).

</rich_text>
      <rich_text weight="heavy">c</rich_text>
      <rich_text>. If you are not using a ‘huge’ kernel that has all likely storage media drivers built in you will need to also place modules needed for booting into the initrd.img in its /usr/lib/modules directory. In practice, a helper build script is available which can automatically do that extra work for you. That additional script is currently being re-vamped so I will describe its use later.

</rich_text>
      <rich_text weight="heavy">d</rich_text>
      <rich_text>. Note that any of, for example, 08firstrib_rootfs, 00modules, 01firmware and any other such addon you wish to add can OPTIONALLY be converted into squashed filesystems, which must be given a .sfs extension if used. In practice you can freely use a mixture of uncompressed 2-digit named directories and/or compressed .sfs files in your FirstRib distro frugal install.

</rich_text>
      <rich_text weight="heavy">e</rich_text>
      <rich_text>. An exemplar of a grub2 (grub.conf stanza) follows. You would of course use UUID values and installation directory names appropriate to your own system build:

menuentry "FR_AOT2" {
  insmod ext2
  search --no-floppy --fs-uuid --set 424d8f42-e835-4111-9053-dd086b3d38e8
  linux /FR_AOT2/vmlinuz w_bootfrom=UUID=424d8f42-e835-4111-9053-dd086b3d38e8=/FR_AOT2 w_changes=RAM2
  initrd /FR_AOT2/initrd.img
}

</rich_text>
      <rich_text scale="h2">More FirstRib-related documentation will be provided later</rich_text>
    </node>
  </node>
</cherrytree>
