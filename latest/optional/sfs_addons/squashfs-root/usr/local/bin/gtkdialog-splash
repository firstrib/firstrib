#!/bin/bash
#set -x
#splash window - compatible with yaf-splash
#built upon gtkdialog-splash, written by mave, june 2010.
#other contributors: BarryK, shinobar, 01micko
#gpl license see: /usr/share/doc/legal/gpl-2.0.txt
#Mar 2014, zigbert
#● MochiMoppel changes (see http://www.murga-linux.com/puppy/viewtopic.php?t=114446&start=15)
# fredx181 changes; find the longest line (if given multiple lines of text) and
# center the icon vertically
# fredx181, another small change to get the icon centered vertically
# 20181109, fredx181, many changes, e.g. size of dialog using 'bottom' 'bottom-right'...
# ... when given multiple lines should be correct now
# 20190614, fredx181, many changes, extra options, see -help
# 20190622, fredx181, default is yad GUI for to create splash code.

[ $(which "${0##*/}") ] && export PROG="${0##*/}" || export PROG=${0}
#echo $PROG

export TEXTDOMAIN=libstardust # even if not gettext!
export OUTPUT_CHARSET=UTF-8   # even if not gettext.

mksplash () {
echo "Default, without arguments, is GUI display for to create splash code"
echo "Type: $PROG -help , for the regular options"

if [ -z $(which xmessage) ]; then
MESSAGE=gxmessage
else
MESSAGE=xmessage
fi

if [ -z `which yad` ]; then
	msg="  Package 'yad' is not installed, please install it. "
	$MESSAGE  "`echo -e $msg`"
	rm -f /tmp/yrad 
exit 0
fi

VERSION=`yad --version | awk '{ print $1 }'`
verlte() {
    [  "$1" = "`echo -e "$1\n$2" | sort -V | head -n1`" ]
}

verlt() {
    [ "$1" = "$2" ] && return 1 || verlte $1 $2
}

if verlt $VERSION 0.38.0; then
yad --text=" The version of yad installed is too old for to run this program,  \n Please upgrade yad to a version higher than 0.37   " --button="gtk-close"
exit
fi

info () {
echo -e " *** Easy way to setup the code for a splash screen ***
Based on '/usr/lib/gtkdialog/box_splash' included in latest Puppy versions.
(formerly named 'gtkdialog-splash' and before that 'yaf-splash')
Continuation of the improvements made by murga-linux forum member MochiMoppel.
See here: http://murga-linux.com/puppy/viewtopic.php?t=114446

Many changes and more options added, e.g:
* -mksplash (this GUI, depends on yad version 0.38 or higher)
* -margin_width and -margin_height (to adjust the text width and height)
(depending on the font used, sometimes using a negative value may be required to get the desired result)
* -bg_gradient_height (background gradient height)
* -ontop
* -transparent (requires gtk2desklet installed)
* animated .gif images supported
Type: '$PROG -help' in terminal to show all options

Depends on gtkdialog, the 'fold' and/or 'fmt' utility, and yad for this GUI
(but yad is not required to run the generated splash-screen code)
 
" | yad --title="Info"  --text-info --fontname=11 --width 650 --height 450 --wrap --margins=5 --button="gtk-close:0"
}
export -f info

splash_save () {
SAVE="/tmp/splash__save.sh"
msg="`echo -e "'"$1"'"`"
DECO="'"${2}"'"
PLACE="'"${3}"'"
CLOSE="'"${4}"'"
ALIGN="'"${5}"'"
BGRHEIGHT="'"${6}"'"
ICON="'"${7}"'"
[ "${8}" -eq "0" ] && ICONWIDTH="'"-1"'" || ICONWIDTH="'"${8}"'"
FONT="$(echo "${9}" | awk '{$NF=""; print $0}')"
FONTSIZE="$(echo "${9}" | awk '{print $NF}')"
FONT="'"${FONT}"'"
FONTSIZE="'"${FONTSIZE}"'"
MARGINW="'"${10}"'"
MARGINH="'"${11}"'"
TIMEOUT="'"${12}"'"
FG="'"${13}"'"
BG="'"${14}"'"
border=$(echo "${15}" | tr '[:upper:]' '[:lower:]')
bgr=$(echo "${16}" | tr '[:upper:]' '[:lower:]')
wrap=$(echo "${17}" | tr '[:upper:]' '[:lower:]')
ontop=$(echo "${18}" | tr '[:upper:]' '[:lower:]')
styled=$(echo "${19}" | tr '[:upper:]' '[:lower:]')
trans=$(echo "${20}" | tr '[:upper:]' '[:lower:]')

echo '#!/bin/bash' > $SAVE
echo '' >> $SAVE
echo '# variable PROG for the program' >> $SAVE 
echo 'PROG="'"${PROG}"'"' >> $SAVE
echo '# text, support multiple lines' >> $SAVE
[ ! -z "$1" ] && echo 'msg="`echo -e "'"$1"'"`"' >> $SAVE || echo 'msg="`echo -e "'"$1 "'"`"' >> $SAVE
if [ "$2" = "" -o "$18" = "TRUE" ]; then
  if [ "$7" ]; then
echo "
"'"$PROG"'" -text "'"$msg"'" -placement $PLACE -close $CLOSE -align $ALIGN -bg_gradient_height $BGRHEIGHT -icon "$ICON" -icon_width $ICONWIDTH -font "$FONT" -fontsize $FONTSIZE -fg "$FG" -bg "$BG" -margin_width $MARGINW -margin_height $MARGINH -timeout $TIMEOUT -bg_gradient $bgr -border $border -wrap $wrap -ontop $ontop -styled $styled -transparent $trans
" >> $SAVE
  else
echo "
"'"$PROG"'" -text "'"$msg"'" -placement $PLACE -close $CLOSE -align $ALIGN -bg_gradient_height $BGRHEIGHT -font "$FONT" -fontsize $FONTSIZE -fg "$FG" -bg "$BG" -margin_width $MARGINW -margin_height $MARGINH -timeout $TIMEOUT -bg_gradient $bgr -border $border -wrap $wrap -ontop $ontop -styled $styled -transparent $trans
" >> $SAVE
  fi
else
  if [ "$7" ]; then
echo "
"'"$PROG"'" -text "'"$msg"'" -deco "$DECO" -placement $PLACE -close $CLOSE -align $ALIGN -bg_gradient_height $BGRHEIGHT -icon "$ICON" -icon_width $ICONWIDTH -font "$FONT" -fontsize $FONTSIZE -fg "$FG" -bg "$BG" -margin_width $MARGINW -margin_height $MARGINH -timeout $TIMEOUT -bg_gradient $bgr -border $border -wrap $wrap -ontop $ontop -styled $styled -transparent $trans
" >> $SAVE
  else
echo "
"'"$PROG"'" -text "'"$msg"'" -deco "$DECO" -placement $PLACE -close $CLOSE -align $ALIGN -bg_gradient_height $BGRHEIGHT -font "$FONT" -fontsize $FONTSIZE -fg "$FG" -bg "$BG" -margin_width $MARGINW -margin_height $MARGINH -timeout $TIMEOUT -bg_gradient $bgr -border $border -wrap $wrap -ontop $ontop -styled $styled -transparent $trans
" >> $SAVE
  fi
fi
cat $SAVE | yad --title="Splash Screen code" --text=" Copy the content below or: \n Move the $SAVE script to another location, rename and make executable" --text-info --width 600 --height 500 --wrap --margins=5 --button="gtk-close:0"
}
export -f splash_save 


splashprev () {
PREV="/tmp/__splash__prev__"

[ ! -z "$1" ] && echo 'msg="`echo -e "'"$1"'"`"' > $PREV || echo 'msg="`echo -e "'"$1 "'"`"' > $PREV
echo 'PROG="'"${PROG}"'"' >> $PREV
echo 'DECO="'"${2}"'"' >> $PREV
echo 'PLACE="'"${3}"'"' >> $PREV
echo 'CLOSE="'"${4}"'"' >> $PREV
echo 'ALIGN="'"${5}"'"' >> $PREV
echo 'BGRHEIGHT="'"${6}"'"' >> $PREV
echo 'ICON="'"${7}"'"' >> $PREV
[ "${8}" -eq "0" ] && echo 'ICONWIDTH="'"-1"'"' >> $PREV || echo 'ICONWIDTH="'"${8}"'"' >> $PREV
FONT="$(echo "${9}" | awk '{$NF=""; print $0}')"
FONTSIZE="$(echo "${9}" | awk '{print $NF}')"
echo 'FONT="'"${FONT}"'"' >> $PREV
echo 'FONTSIZE="'"${FONTSIZE}"'"' >> $PREV
echo 'MARGINW="'"${10}"'"' >> $PREV
echo 'MARGINH="'"${11}"'"' >> $PREV
echo 'TIMEOUT="'"10"'"' >> $PREV
echo 'FG="'"${13}"'"' >> $PREV
echo 'BG="'"${14}"'"' >> $PREV
echo 'BORDER="'"${15}"'"' | tr '[:upper:]' '[:lower:]' >> $PREV
echo 'BGR="'"${16}"'"' | tr '[:upper:]' '[:lower:]' >> $PREV
echo 'WRAP="'"${17}"'"' | tr '[:upper:]' '[:lower:]' >> $PREV
echo 'ONTOP="'"${18}"'"' | tr '[:upper:]' '[:lower:]' >> $PREV
echo 'STYLED="'"${19}"'"' | tr '[:upper:]' '[:lower:]' >> $PREV
echo 'TRANS="'"${20}"'"' | tr '[:upper:]' '[:lower:]' >> $PREV

if [ -z "$2" -o "$18" = "TRUE" ]; then
  if [ "$7" ]; then
echo '
"$PROG" -text "$msg" -placement $PLACE -close $CLOSE -align $ALIGN -bg_gradient_height $BGRHEIGHT -icon "$ICON" -icon_width $ICONWIDTH -font "$FONT" -fontsize $FONTSIZE -fg "$FG" -bg "$BG" -margin_width "$MARGINW" -margin_height "$MARGINH" -timeout $TIMEOUT -bg_gradient $bgr -border $border -wrap $wrap -ontop $ontop -styled $styled -transparent $trans
' >> $PREV
  else
echo '
"$PROG" -text "$msg" -placement $PLACE -close $CLOSE -align $ALIGN -bg_gradient_height $BGRHEIGHT -font "$FONT" -fontsize $FONTSIZE -fg "$FG" -bg "$BG" -margin_width "$MARGINW" -margin_height "$MARGINH" -timeout $TIMEOUT -bg_gradient $bgr -border $border -wrap $wrap -ontop $ontop -styled $styled -transparent $trans
' >> $PREV
  fi
else
  if [ "$7" ]; then
echo '
"$PROG" -text "$msg" -deco "$DECO" -placement $PLACE -close $CLOSE -align $ALIGN -bg_gradient_height $BGRHEIGHT -icon "$ICON" -icon_width $ICONWIDTH -font "$FONT" -fontsize $FONTSIZE -fg "$FG" -bg "$BG" -margin_width "$MARGINW" -margin_height "$MARGINH" -timeout $TIMEOUT -bg_gradient $bgr -border $border -wrap $wrap -ontop $ontop -styled $styled -transparent $trans
' >> $PREV
  else
echo '
"$PROG" -text "$msg" -deco "$DECO" -placement $PLACE -close $CLOSE -align $ALIGN -bg_gradient_height $BGRHEIGHT -font "$FONT" -fontsize $FONTSIZE -fg "$FG" -bg "$BG" -margin_width "$MARGINW" -margin_height "$MARGINH" -timeout $TIMEOUT -bg_gradient $bgr -border $border -wrap $wrap -ontop $ontop -styled $styled -transparent $trans
' >> $PREV
  fi
fi
bash $PREV


}
export -f splashprev 

yad --center --title="Create Splash Screen script" --width=600 --form --columns=2 --item-separator=":" \
--text="   Make your settings and click 'Preview' (has timeout 10 sec) or 'Generate' button" \
--field 'Text (can be multiple lines) for content of text-file, e.g. $(cat /path/list.txt):TXT' '' \
--field "Deco Title (empty for undecorated): : " '' \
--field "Placement: :CB" "center:mouse:top:bottom:top-right:top-left:bottom-right:bottom-left" \
--field "Close: :CB" "mouseover:never:box" \
--field "Alignment: :CB" "left:center:right" \
--field="Gradient height (if Gradient background): :NUM" "80:1..800:1" \
--field "Icon (also .gif supported, empty for no icon) :sfl" "gtk-ok" \
--field="Icon width (0=original icon width): :NUM" "0:0..400:1" \
--field "Select Font :FN" "DejaVu Sans Bold 10" \
--field="Margin width (negative value supported): :NUM" "10:-200..400:1" \
--field="Margin height (negative value supported): :NUM" "10:-200..400:1" \
--field="Timeout (sec):NUM" "0:0..600:1" \
--field "Foreground:clr" "#000000" \
--field "Background:clr" "#BEBEBE" \
--field "  Border :chk" "TRUE" \
--field "Gradient Background :chk" "TRUE" \
--field "Wrap text:chk" "TRUE" \
--field "On Top (will disable deco) :chk" "FALSE" \
--field "Styled border\n(depends gtk-engines-murrine):chk" "FALSE" \
--field "Transparent\n(depends gtk2desklet):chk" "FALSE" \
--field "Preview:fbtn" 'bash -c "splashprev %1 %2 %3 %4 %5 %6 %7 %8 %9 %10 %11 %12 %13 %14 %15 %16 %17 %18 %19 %20"' \
--field "Generate:fbtn" '@bash -c "splash_save %1 %2 %3 %4 %5 %6 %7 %8 %9 %10 %11 %12 %13 %14 %15 %16 %17 %18 %19 %20"' --buttons-layout=spread --button="gtk-info:bash -c info" --button="gtk-close:0"

}
export -f mksplash

[ ! -f /tmp/yaf-splash ] && ln -sfn "`which gtkdialog`" /tmp/yaf-splash

##### end yad GUI ####

##### start splash code ####

geometry=$(xwininfo -root | grep ' \-geometry ')
geometry=${geometry%%\+*}
geometry=${geometry##* }
ROOTX=${geometry%x*}
ROOTY=${geometry#*x}

#read ROOTDIMS ROOTX ROOTY << EOF
#`xwininfo -root | grep ' \-geometry ' | cut -f 1 -d '+' | tr 'x' ' '`
#EOF

MAX_CHAR_WIDTH=45 #Maximum number of chars in one line
MARGIN_DESKTOP=40 #WM bars could conflict if pushing splashes all into the corner

#fredx181, add bottom-right and top-right to the options
helptext="usage: $PROG [OPTIONS] [-timeout SEC] -text TEXT | -kill PID
  -mksplash run yad GUI , create splash-screen script (default)
  -text \"EXPRESSION\"
  -timeout COUNT (in seconds)
  -icon GTK-XXX (for example: gtk-info all gtk-stock-symbols, default: none)
    OR path/to/pixmap.png|gif|jpeg|svg
  -icon_width WIDTH in pixels
  -bg COLOR (background color red, blue, yellow..., default: grey)
  -bg_gradient true|false (default: true)
  -bg_gradient_height SIZE (default 80)
  -ontop true|false (default: false)
  -fg COLOR (font color, default: black)
  -placement center|mouse|top|bottom|top-right|top-left|bottom-right|bottom-left (default: center)
  -close never|mouseover|box (default is mouseover)
  -deco TITLE (shows windows decorations, with title)
  -font \"NAME\"
  -fontsize SIZE
  -align ALIGNMENT left, right or center
  -margin_width SIZE (default: 10)
  -margin_height SIZE (default: 10)
  -margin SIZE (width and height, will disable margin_width and margin_height setting)
  -border true|false (default: true)
  -styled true|false (default: true, required is gtk-engines-murrine)
  -transparent true|false (default: false, required is gtk-desklet)
  -wrap true|false (default: true)
  -kill PID (process ID called before:
    PID=0 auto-search for the last one,
    PID=xxxxx kill the last one read by PID=\$!"

BasePIDFile="/tmp/GTKDIALOG-SPLASH"

close='mouseover'
text=""
timeout=0
icon=""
font="DejaVu bold"
fontsize="10"
marginw=10
marginh=10
border=true
pid=""
placement='center'
deco='false'
title='box_splash'
#~ ICON_WIDTH=32	#~ ●
ICON_WIDTH=-1		#~ ● Shows icons in their original size, enables GIF animation
typehint=0		# fredx181, default is not on-top, '-ontop true' will set typehint=6
#typehint=6			#~ ● no decoration, no taskbar, window above
ALIGN=0 #left
WRAP=true
bg_gradient_height=80
ontop=false
styled=false
trans=false
#now, let's see if the default colors is set in config file
[ -s $HOME/.config/ptheme/gtkdialog_active ] && . $HOME/.config/ptheme/gtkdialog_active
if [ ! "$BOX_SPLASH_BG" -a ! "$BOX_SPLASH_BG_GRADIENT" -a ! "$BOX_SPLASH_FG" ] ; then
	BOX_SPLASH_BG=grey90
elif [ ! "$BOX_SPLASH_BG" ] ; then
	BOX_SPLASH_BG=grey
fi
[ ! "$BOX_SPLASH_BG_GRADIENT" ]	&& BOX_SPLASH_BG_GRADIENT=false
[ ! "$BOX_SPLASH_FG" ]	&& BOX_SPLASH_FG=black

bg=$BOX_SPLASH_BG
bg_gradient=$BOX_SPLASH_BG_GRADIENT
fg=$BOX_SPLASH_FG
[ $border = false ] && notebook_border='show-border="false"'

#parameters
#if [ $# = 0 ]; then echo "$helptext"; exit; fi #no parameters
if [ $# = 0 ]; then mksplash; exit; fi #no parameters
for arg in "$@" ; do
	case $1 in
		-h|-help|--help) echo "$helptext";	exit;;
		-mksplash) mksplash;	exit;;
	esac
	if [ "$2" ] ; then
		case $1 in
			-align)
				case $2 in
					right) ALIGN=1 ;;
					center) ALIGN=2 ;;
					*) ALIGN=0 ;; #left
				esac
				shift
				;;
			-bg)			bg="$2";			shift;;
			-bg_gradient) 	bg_gradient="$2";	shift;;
			-bg_gradient_height) 	bg_gradient_height="$2";	shift;;
			-border)
				border="$2"
				[ $border = false ] && notebook_border='show-border="false"'
				[ $border = true ] && notebook_border='show-border="true"'
				shift
				;;
			-close)			close="$2";			shift;;
#~ 			-deco)			title="$2";			shift;;
			-deco)	deco=true	typehint=0	title="$2";			shift;; #~ ●  deco variable set (this assignment was missing); typehint=0 results in window decorations
			-fg)			fg="$2";			shift;;
			-font)
				N=$(echo $2 | sed -e 's/^.[^0-9]*//'| cut -b1-2| tr -dc '0-9')
				[ "$N" ] && fontsize=''
				font="$2"
				shift
				;;
			-fontsize)
				fontsize="$2"
				[ $2 = small ] && fontsize="8"
				[ $2 = medium ] && fontsize="10"
				[ $2 = large ] && fontsize="12"
				[ $2 = x-large ] && fontsize="14"
				shift
				;;
			-icon)			ICON="$2";			shift;;
			-icon_width)	ICON_WIDTH="$2";	shift;;
			-kill)
				pid=$2
				if [ ${#pid} -eq -1 ]; then
					dlgPID=`busybox ps | grep -w 'GTKDIALOG_SPLASH'`
					kill $dlgPID 2>/dev/null
				else
					TmpFile=$BasePIDFile${pid}
					dlgPID="`cat $TmpFile`"
					kill $dlgPID 2>/dev/null
					rm -f $TmpFile
					exit
				fi
				shift
				;;
			-margin_width)		marginw="$2"; 		shift;;
			-margin_height)		marginh="$2"; 		shift;;
			-margin)		margin="$2"; 		shift;;
			-styled)		styled="$2"; 		shift;;
			-placement)		placement="$2";		shift;;
			-text)			text="$2";			shift;;
			-timeout)		timeout="$2";		shift;;
			-wrap)
				if [ "$2" = false ]; then
					MAX_CHAR_WIDTH=9999
					WRAP=false
				fi
				shift
				;;
			-ontop)
				ontop="$2"
				[ $ontop = false ] && typehint=0
				[ $ontop = true ] && typehint=6
				shift
				;;
			-transparent)
				trans="$2"
				[ $trans = true ] && notebook_border='show-border="false"'
				shift
				;;
		esac
		shift
	fi
done

# compatible with box_splash when margin is set
if [ $margin ]; then
marginh=$margin
marginw=$margin
fi

# convert background color to hex if name contains more than one word
  if [ $(echo -e "$bg" | wc -w) -ge 2 ]; then
while read -r line; do
BG="$(echo $line | grep -i "$bg" |awk '{$1="";print}')"
if [[ "${BG,,}" = " ${bg,,}" ]]; then
bg="$(echo $line |awk '{ print $1 }')"
fi
done <<< "$(grep -i "${bg}$" /usr/share/X11/rgb.txt | awk 'NF<7 {printf "#%02X%02X%02X\t%s %s %s\n",$1,$2,$3,$4,$5,$6}')"
echo $bg
  fi

#put it here in case command have -icon_width specified after -icon
# fredx181, set space-expand= to true to get the icon centered
if [ "$ICON" ]; then
	icon="<vbox>
<pixmap width-request=\"$ICON_WIDTH\" space-expand=\"true\" space-fill=\"false\">
	  <width>$ICON_WIDTH</width>
	  <input file stock=\"$ICON\"></input>
	</pixmap>
</vbox>"
	case $ICON in
	gtk*) ok=1 ;;
	*) icon="<vbox>
<pixmap width-request=\"$ICON_WIDTH\" space-expand=\"true\" space-fill=\"false\">
	  <width>$ICON_WIDTH</width>
	  <input file>\"$ICON\"</input>
	</pixmap>
</vbox>" ;;
	esac
# fredx181, in case only text, no icon. Moved from the main dialog below to here
#else
#	icon="<text space-expand=\"true\" space-fill=\"true\"><label>\"\"</label></text>"
fi

#define height and width of the window
WIDTH=$(($WIDTH+0))
HEIGHT=$(($HEIGHT+0))

# experimented with xftwidth and ghostscript to get the width in pixels, but isn't reliable so commented out
# instead added margin setting for width and height
#   if [ $(which xftwidth) ]; then

#NR_LINES=$(echo -e "$text" | wc -l) # count number of lines
#[ $NR_LINES -gt 8 ] && MAX_CHAR_WIDTH=75

#CHAR_WIDTH=$(echo $longest | wc -L)

#if [ -z $(which fmt) ]; then
#[ ${WRAP} = true ] && text=$(printf '%s\n' "$text" | fold -s -w $MAX_CHAR_WIDTH)
#else
#[ ${WRAP} = true ] && text=$(printf '%s\n' "$text" | fmt -s -w $MAX_CHAR_WIDTH | fold -s -w $(($MAX_CHAR_WIDTH+10)))
#fi
#NR_LINES=$(echo -e "$text" | wc -l) # count number of lines again after wrap

#longest=$(echo -e "$text" | awk 'length > m { m = length; a = $0 } END { print a }')
#WIDTH=$(gs -dQUIET -sDEVICE=nullpage 2>/dev/null -   <<<"$fontsize /$fnt   findfont exch scalefont setfont ($longest)  stringwidth pop ==" | cut -f1 -d".")

#WIDTH=$(xftwidth "$font:size=$fontsize" "$longest")

#if [ -n "$(echo "$font" | grep "Bold")" ]; then
#WIDTH=`awk -v VAR1=$WIDTH 'BEGIN{print int(VAR1*1.25)}'`
#else
#WIDTH=`awk -v VAR1=$WIDTH 'BEGIN{print int(VAR1*1.00)}'`
#fi

#   else       # if xftwidth not installed

NR_LINES=$(echo -e "$text" | wc -l) # count number of lines
[ $NR_LINES -gt 8 ] && MAX_CHAR_WIDTH=75

if [ -z $(which fmt) ]; then
[ ${WRAP} = true ] && text=$(printf '%s\n' "$text" | fold -s -w $MAX_CHAR_WIDTH)
else
[ ${WRAP} = true ] && text=$(printf '%s\n' "$text" | fmt -s -w $MAX_CHAR_WIDTH | fold -s -w $(($MAX_CHAR_WIDTH+10)))
fi
NR_CHARS="$(echo "$text"| wc -L )"

NR_LINES=$(echo -e "$text" | wc -l) # count number of lines

CHAR_WIDTH=$NR_CHARS

#TMP=`awk -v VAR1=$CHAR_WIDTH -v VAR2=$fontsize 'BEGIN{print int(VAR1*(VAR2*0.95))}'`
#TMP=$(( $CHAR_WIDTH * ($fontsize * 1) ))
#TMP=`awk -v VAR1=$TMP 'BEGIN{print int(VAR1*0.95)}'`

if [ -n "$(echo "$font" | grep "Bold")" ]; then
TMP=`awk -v VAR1=$CHAR_WIDTH -v VAR2=$fontsize 'BEGIN{print int(VAR1*(VAR2*0.95))}'`
else
TMP=`awk -v VAR1=$CHAR_WIDTH -v VAR2=$fontsize 'BEGIN{print int(VAR1*(VAR2*0.85))}'`
fi
#   fi

[ "$marginw" -lt 0 ] && WIDTH=$(($WIDTH+$TMP-${marginw##*-}-${marginw##*-})) || WIDTH=$(($WIDTH+$TMP+$marginw+$marginw))

TMP=`awk -v VAR1=$NR_LINES -v VAR2=$fontsize 'BEGIN{print int(VAR1*(VAR2*1.65))}'`
#TMP=$(( $NR_LINES * ($fontsize * 2) ))
#TMP=`awk -v VAR1=$TMP 'BEGIN{print int(VAR1*0.85)}'`
[ "$marginh" -lt 0 ] && HEIGHT=$(($HEIGHT+$TMP-${marginh##*-}-${marginh##*-})) || HEIGHT=$(($HEIGHT+$TMP+$marginh+$marginh))

#if [ $NR_LINES -gt 1 ] && [ $NR_CHARS -eq $MAX_CHAR_WIDTH ]; then
#HEIGHT=$(($HEIGHT+$fontsize)) #gtkdialog <text> widgets does not has such accurate wrapping as we calculate. It wraps words, not chars
#fi

# fredx181, define hight and width of icon, gives more accurate size of splash dialog
if [ "$ICON" ]; then
	if [ $CHAR_WIDTH -lt 1 ]; then
	WIDTH=0
	HEIGHT=0
	ICON_ONLY=yes
	fi
# get size of the icon
IM_WIDTH=$(file "$ICON" | awk -F " x " '{print $1}' | awk '{print $NF}' | sed 's/,//')
IM_HEIGHT=$(file "$ICON" | awk -F " x " '{print $2}' | awk '{print $1}' | sed 's/,//')
case $ICON in
gtk*)
IM_WIDTH=30
IM_HEIGHT=30
;;
esac
IM_WIDTH=$(($IM_WIDTH+5))
IM_HEIGHT=$(($IM_HEIGHT+5))

  if [ $ICON_WIDTH -gt 1 ]; then
# set new height according to icon width aspect ratio 
IM_HEIGHT=$(awk "BEGIN { v=($IM_HEIGHT/$IM_WIDTH)*$ICON_WIDTH; print v=int(v)}")
IM_HEIGHT=$(($IM_HEIGHT+5))
    if [ "$IM_HEIGHT" -ge "$HEIGHT" ]; then
#    HEIGHT=$(($IM_HEIGHT+$marginh+$marginh))
[ "$marginh" -lt 0 ] && HEIGHT=$(($IM_HEIGHT-${marginh##*-}-${marginh##*-})) || HEIGHT=$(($IM_HEIGHT+$marginh+$marginh))
    else
    DIFF=$(($HEIGHT-$IM_HEIGHT))
    HEIGHT=$(($HEIGHT))
    fi
  else
    if [ "$IM_HEIGHT" -ge "$HEIGHT" ]; then
#    HEIGHT=$(($IM_HEIGHT+$marginh+$marginh))
[ "$marginh" -lt 0 ] && HEIGHT=$(($IM_HEIGHT-${marginh##*-}-${marginh##*-})) || HEIGHT=$(($IM_HEIGHT+$marginh+$marginh))
    else
    DIFF=$(($HEIGHT-$IM_HEIGHT))
    HEIGHT=$(($HEIGHT))
    fi
  fi

if [ $ICON_WIDTH -gt 1 ]; then
#WIDTH=`awk -v VAR1=$WIDTH 'BEGIN{print int(VAR1*0.90)}'`
WIDTH=$(($WIDTH+$ICON_WIDTH))

else
#WIDTH=`awk -v VAR1=$WIDTH 'BEGIN{print int(VAR1*0.90)}'`
WIDTH=$(($WIDTH+$IM_WIDTH))

fi
	if [ $CHAR_WIDTH -lt 1 ]; then
#	WIDTH=$(($WIDTH+$marginw+$marginw))
[ "$marginw" -lt 0 ] && WIDTH=$(($WIDTH-${marginw##*-}-${marginw##*-})) || WIDTH=$(($WIDTH+$marginw+$marginw))
	HEIGHT=$(($HEIGHT))
#ALIGN=right
	fi	

#	[ $HEIGHT -lt 30 ] && HEIGHT=30
#	[ $WIDTH -lt 65 ] && WIDTH=65

else          # text only

HEIGHT=$(($HEIGHT))
#WIDTH=`awk -v VAR1=$WIDTH 'BEGIN{print int(VAR1*0.90)}'`
WIDTH=$(($WIDTH))

#HEIGHT=$(($HEIGHT-15))
#	[ $HEIGHT -lt 30 ] && HEIGHT=30
#	[ $WIDTH -lt 65 ] && WIDTH=65
fi

if [ "$border" = "true" ]; then
	WIDTH=$(($WIDTH+10))
	HEIGHT=$(($HEIGHT+10))
fi

if [ "$close" = "box" ]; then
	WIDTH=$(($WIDTH+25))
	HEIGHT=$(($HEIGHT+20))
fi

# Set CHAR_WIDTH to 0 if no wrap, otherwise the width of splash screen gets too large
[ ${WRAP} = false ] && CHAR_WIDTH=0

#process placement, default is center...
WINPLACE=''
WINTYPE=1

case $placement in
	mouse) GEOMPARAM=''; WINPLACE='window-position="2"'; WINTYPE=2;;
	top) X=$((($ROOTX/2)-($WIDTH/2))); Y=$MARGIN_DESKTOP; WINTYPE=10;;
	top-right) X=$(($ROOTX-$WIDTH-$MARGIN_DESKTOP)); Y=$MARGIN_DESKTOP; WINTYPE=10;;
	top-left) X=$MARGIN_DESKTOP; Y=$MARGIN_DESKTOP; WINTYPE=10;;
	bottom) X=$((($ROOTX/2)-($WIDTH/2))); Y=$(($ROOTY-$HEIGHT-$MARGIN_DESKTOP)); WINTYPE=10;;
	bottom-right) X=$(($ROOTX-$WIDTH-$MARGIN_DESKTOP)); Y=$(($ROOTY-$HEIGHT-$MARGIN_DESKTOP)); WINTYPE=10;;
	bottom-left) X=$MARGIN_DESKTOP; Y=$(($ROOTY-$HEIGHT-$MARGIN_DESKTOP)); WINTYPE=10;;
esac

width_svg=$(($WIDTH+40))
height_svg=$(($HEIGHT+40))
#set backgound
if [ $bg_gradient = true ]; then
	#build svg-background image
	echo '
	<svg version="1.1" width="'$width_svg'" height="'$height_svg'">
	  <defs>
	    <linearGradient id="LG_01" x1="8" y1="'$bg_gradient_height'" x2="0" y2="0" gradientUnits="userSpaceOnUse">
	      <stop style="stop-color:'$bg';stop-opacity:1" offset="0" />
	      <stop style="stop-color:'$bg';stop-opacity:0.3" offset="1" />
	    </linearGradient>
	  </defs>
	  <rect style="fill:url(#LG_01);fill-opacity:1" width="'$width_svg'" height="'$height_svg'"/>
	</svg>' > /tmp/gtkdialog_splash_bg.svg #full opacity
	echo '
	<svg version="1.1" width="'$width_svg'" height="'$height_svg'">
	  <defs>
	    <linearGradient id="LG_01" x1="8" y1="20" x2="0" y2="0" gradientUnits="userSpaceOnUse">
	      <stop style="stop-color:'$bg';stop-opacity:1" offset="0" />
	      <stop style="stop-color:'$bg';stop-opacity:0.1" offset="10" />
	    </linearGradient>
	  </defs>
	  <rect style="fill:url(#LG_01);fill-opacity:0.7;stroke-width:0" width="'$width_svg'" height="'$height_svg'"/>
	</svg>' > /tmp/gtkdialog_splash_border.svg #a bit transparent
	if [ $border = false ]; then
		gtk_border='bg_pixmap[NORMAL] = "gtkdialog_splash_bg.svg"'
	else
		gtk_border='bg_pixmap[NORMAL] = "gtkdialog_splash_border.svg"'
		gtk_bg='bg_pixmap[NORMAL] = "gtkdialog_splash_bg.svg"'
	fi
else
	gtk_bg="bg[NORMAL] = \"$bg\""
	gtk_border="bg[NORMAL] = \"$bg\""
fi

#gtk-theme
if [ $styled = true ]; then 
gtkrc='
pixmap_path "/tmp"
style "gtkdialog-splash"
{
	'"$gtk_border"'

}
class "GtkWindow" style "gtkdialog-splash"

style "notebook"
{
	'"$gtk_bg"'
	xthickness	= '1'
	ythickness	= '1'	
engine "murrine"{


contrast = '2.0'		
roundness           = '3' 
}
}
class "GtkNotebook" style "notebook"

style "font"
{
	font_name="'"$font"' '"$fontsize"'"
	text[NORMAL]="'$fg'"
	fg[NORMAL]="'$fg'"
}
class "GtkLabel" style "font"'
else
gtkrc='
pixmap_path "/tmp"
style "gtkdialog-splash"
{
	'"$gtk_border"'

}
class "GtkWindow" style "gtkdialog-splash"

style "notebook"
{
	'"$gtk_bg"'
	xthickness	= '1'
	ythickness	= '1'	

}
class "GtkNotebook" style "notebook"

style "font"
{
	font_name="'"$font"' '"$fontsize"'"
	text[NORMAL]="'$fg'"
	fg[NORMAL]="'$fg'"
}
class "GtkLabel" style "font"'
fi
echo $gtkrc > /tmp/gtkdialog-splash_gtkrc
export GTK2_RC_FILES=/tmp/gtkdialog-splash_gtkrc
chmod 666 /tmp/gtkdialog-splash_gtkrc

#process close of window, default is mouseover...
CLOSETAG1=''
#~ CLOSETAG2='<action signal="enter-notify-event" type="exit">Exit on mouse-over</action>' #~ ● That's bad. Does not display dialog when mouse cursor happens to be in same location. 
CLOSETAG2='
<action signal="leave-notify-event" condition="command_is_true(N=$(date +%s%2N);[ $((N-STARTTIME)) -gt 150 ] && echo true)"  type="exit" >Exit on mouse-leave</action>
<action signal="enter-notify-event" condition="command_is_true(N=$(date +%s%2N);[ $((N-STARTTIME)) -gt 150 ] && echo true)"  type="exit" >Exit on mouse-enter</action>
'

case $close in
	never)
		CLOSETAG2=''
		;;
	box)
		CLOSETAG2=''
		CLOSETAG1='<vbox><button>'"`/usr/bin/xml_button-icon cancel`"'<action type="exit">Exit on click close-box</action></button></vbox>'
		;;
esac

#gui
#~ export GTKDIALOG_SPLASH='
#~ <window title="'${title}'" focus-on-map="false" skip-taskbar-hint="false" icon-name="gtk-preferences" resizable="false" decorated="'${deco}'" '${WINPLACE}'>
#~ <vbox>
#~   <notebook show-tabs="false" '"$notebook_border"' space-expand="true" space-fill="true">
#~     <hbox>
#~       <vbox>
#~         '${icon}'
#~         <text space-expand="true" space-fill="true"><label>""</label></text>
#~       </vbox> 
#~       <text justify="'${ALIGN}'" width-chars="'${CHAR_WIDTH}'" wrap="'${WRAP}'" space-expand="true" space-fill="true">
#~         <label>"'${text}'"</label>
#~       </text>
#~       '${CLOSETAG1}'
#~     </hbox>
#~   </notebook>
#~ </vbox>
#~ '${CLOSETAG2}'
#~ </window>'

#~ ● Changed only <window... ; type-hint="6 forces dialog on top of any other window, even on top of fullscreen and "above" mode windows; 'title' and 'decorated' not functional since variable deco missing. Just another bug...
# fredx181, moved away the line after '${icon}' to section (above) "if [ "$ICON" ]; then" ...
# ... after the "else". This makes the icon display on the center vertically.
export GTKDIALOG_SPLASH='
<window title="'${title}'" type-hint="'$typehint'" focus-on-map="false" skip-taskbar-hint="true" icon-name="gtk-preferences" resizable="false" decorated="'${deco}'" '${WINPLACE}'>
<vbox>
  <notebook show-tabs="false" '"$notebook_border"' space-expand="true" space-fill="true">
    <hbox>
        '${icon}'
      <text justify="'${ALIGN}'" width-chars="0" wrap="false" space-expand="true" space-fill="true">
        <label>"'${text}'"</label>
      </text>
      '${CLOSETAG1}'
    </hbox>
  </notebook>
</vbox>
'${CLOSETAG2}'
</window>'

[ $trans = true ] && TRANS="--gtk-module=gtk2desklet"
export STARTTIME=$(date +%s%2N)	#~ ● Needed to prevent premature closing
case $WINTYPE in
	2) #mouse
		[ ${timeout} -eq 0 ] && exec /tmp/yaf-splash $TRANS --class="gtkdialog-splash" -p GTKDIALOG_SPLASH --geometry="$WIDTH"x"$HEIGHT"+"${X}"+"${Y}" #to keep same pid.
		/tmp/yaf-splash $TRANS --class="gtkdialog-splash" -p GTKDIALOG_SPLASH --geometry="$WIDTH"x"$HEIGHT"+"${X}"+"${Y}" &
		;;
	10)
		[ ${timeout} -eq 0 ] && exec /tmp/yaf-splash $TRANS --class="gtkdialog-splash" -p GTKDIALOG_SPLASH --geometry="$WIDTH"x"$HEIGHT"+"${X}"+"${Y}" #to keep same pid.
		/tmp/yaf-splash $TRANS --class="gtkdialog-splash" -p GTKDIALOG_SPLASH --geometry="$WIDTH"x"$HEIGHT"+"${X}"+"${Y}" &
		;;
	*) #center
		[ ${timeout} -eq 0 ] && exec /tmp/yaf-splash $TRANS --class="gtkdialog-splash" -p GTKDIALOG_SPLASH --center --geometry="$WIDTH"x"$HEIGHT"+"${X}"+"${Y}" #to keep same pid.
		/tmp/yaf-splash $TRANS --class="gtkdialog-splash" -p GTKDIALOG_SPLASH --center --geometry="$WIDTH"x"$HEIGHT"+"${X}"+"${Y}" &
		;;
esac
dlgPID=$!

while [ $timeout -ne 0 ];do #100604
	sleep 1
	timeout=$(($timeout-1))
	[ "`busybox ps | awk '{print $1}' | grep "^${dlgPID}$"`" == "" ] && exit #already killed.
done
kill $dlgPID
echo 'EXIT="Exit on timeout"'
