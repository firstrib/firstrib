#!/bin/sh
# Creation Date: 23May2019; Revision Date: 30Aug2021
# Copyright wiak 30 June 2019+; Licence MIT (aka X11 license). Revision 2.0.8 26Apr2020.
case "$1" in
	'') echo "initrd name must be specified";exit;;
	'-v'|'--version') echo "Modify initrd xz revision 2.0.8";exit;;
	'-h'|'--help'|'-?') echo "Run this script from location of xz-compressed initrd with command:"
						echo "./modify_initrd_xz.sh <filename>"
						echo "For example: ./modify_initrd_xz.sh initrd1.xz";exit;;
	"-*") echo "option $1 not available";exit;;
esac
if [ ! -f "$1" ];then echo "No such file exists";exit;fi  
initrd=${1%.*}
mkdir -p ${initrd}_decompressed
cd ${initrd}_decompressed
unxz -c ../${1} | cpio -idm
printf "File ${1} has been decompressed into directory ${initrd}_decompressed
and a cd command to that directory has been made.
At this stage, you can use this new shell or another shell terminal
or gui program to modify the initrd contents.
For example, you can edit the initrd/init shell script if you wish,
or add/remove contents from/to this ${initrd}_decompressed directory
via shell commands or your favourite filemanager.
Once you have completed your modifications, simply type: exit
A new date-stamped ${1} will then be created in same directory
as the original ${1} and this ${initrd}_decompressed directory
will be automatically deleted.
Alternatively, if you simply want ${initrd}_decompressed, close this terminal.
"
sh
echo "Result being compressed. Please wait patiently ..."
datestamp=`date +%Y_%m_%d_%H%M%S`
find . | cpio -oH newc 2>/dev/null | xz -f --extreme --check=crc32 > ../${initrd}_${datestamp}.xz 
cd ..
sync
rm -rf ${initrd}_decompressed
printf "Finished. 
File ${initrd}_${datestamp}.xz has been created.
"
