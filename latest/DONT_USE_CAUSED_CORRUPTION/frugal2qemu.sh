#!/bin/bash
# Creation Date: 16July2024
progname="frugal2qemu.sh"; version="001"; revision="-rc1" #Revision Date: 16July2024
# A FirstRib utility (but can be used with other distros)
# Copyright wiak (William McEwan) 16July2024+; Licence MIT (aka X11 license)
# Run this script from FR/KL distro frugal boot directory



# If not using a frugal2qemu.cfg file, the following are the defaults
# Also refer to line 60 or thereabouts. You can modify these to suit your needs:
# Alternatively create a frugal2qemu.cfg txt file with altered values
# prior to running this script
qemu_executable="qemu-system-x86_64"  # qemu executable filename
q_mem=2G					# RAM allocated to qemu Virtual Machine
q_vga="cirrus"				# or, for example, std or virtio
q_smp="2"					# 'q' stands for qemu
q_kernel="vmlinuz"			# filename of kernel used
q_initrd="initrd.gz"		# filename of initrd used
w_changes="RAM0"			# FirstRib save persistence mode to use
q_extras=""					# Empty by default, but can use to extend via frugal2qemu.cfg file


_find_partition (){  # code extracted from FirstRib wd_grubconfig.sh and firstribit.sh
	cd "$HERE"
	subdir="$bootdir"
	bootuuid=`df . | awk '/^\/dev/ {print $1}' | xargs blkid -s UUID | awk -F\" '{print $2}'`
	bootlabel=`df . | awk '/^\/dev/ {print $1}' | xargs blkid -s LABEL | awk -F\" '{print $2}'`

}

case "$1" in
  '--version') printf "$progname ${version}${revision}\n"; exit 0;;
  '-h'|'--help'|'-?') 
		printf "$progname ${version}${revision}\n\n"
		printf "This scripts boots a FR/KL frugal installed distro in qemu
by directly accessing the bootfrom media vmlinuz, initrd, and
upper_changes folder.

Simply execute this script with command:

./$progname optional_config_filename

default config filename is frugal2qemu.cfg whose contents
you can edit to change qemu arguments\n";	exit 0;;
	'q') exit;;
esac

# Create working directories
HERE="`pwd`"
bootdir=`basename "$HERE"` # for use with grub config
_find_partition

# If fr_qemu_cfg01 file exists its values will overwrite the defaults:
fr_qemu_cfg01="frugal2qemu.cfg"	# filename of frugal2qemu configuration file
[ "$1" ] && fr_qemu_cfg01="$1"		# optional first parameter specifies alternative fr_qemu_cfg01 filename	



# Defaults are for firstrib-only (FR/KL) distros
# For non-FR distro type boot arguments need changed in plugin:
distro=firstrib
w_bootfrom="UUID=${bootuuid}=/$subdir"
bootmnt="`findfs ${w_bootfrom%=*} 2>/dev/null`"
q_drive="file=${bootmnt::-1}"  # warning: only works for max of 9 partitions I think
q_append="w_bootfrom=$w_bootfrom w_changes=$w_changes"

if [ -s ./"${fr_qemu_cfg01}" ]; then  # otherwise, use script's firstrib-only qemu defaults
  printf "\nNOTE that a ${fr_qemu_cfg01} plugin file has been detected.
This is simply a text file containing qemu arguments you want to use.
You can optionally modify its contents right now if you wish, then:
Press Enter key to use its commands,
or q to quit without using the plugin: "
  read choice
  if [ "$choice" = "q" ]; then
    exit 0
  else
    . ./"${fr_qemu_cfg01}"  # sources the fr_qemu_cfg01 config file 
	# Extra stanzas could be added for other distros or alternatively simply put appropriate code in plugin:
    case $distro in
      'firstrib'):
		# We redo this because w_changes mode may be altered in plugin
        q_append="w_bootfrom=$w_bootfrom w_changes=$w_changes"
      ;;
      'puppy'):
        # using rockedge "Boot parameters" reference: https://forum.puppylinux.com/viewtopic.php?p=52875#p52875

#menuentry \"${subdir}\" {
#  insmod ext2
#  search --no-floppy --fs-uuid --set $bootuuid
#  linux /$subdir/vmlinuz pmedia=ata pfix=nocopy psubdir=/$subdir/ psave=${bootuuid}:/$subdir/
#  initrd /$subdir/initrd.gz
#}



echo here1111111111111
#        q_append="pmedia=usbflash pfix=nocopy psubdir=/$subdir/ psave=${bootuuid}:/$subdir/"  # pdrv=${bootuuid}
        q_append="pmedia=usbflash pfix=nocopy psubdir=/$subdir/ pdir=${bootuuid}"  # pdrv=${bootuuid} 
        bootmnt="`findfs UUID=${bootuuid} 2>/dev/null`"
		q_drive="file=${bootmnt::-1}"  # warning: only works for max of 9 partitions I think
      ;;
    esac
  fi
fi



# You can alternatively manually modify the following
# ${qemu_executable} -enable-kvm -m $q_mem -vga $q_vga -smp $q_smp -kernel $q_kernel -append "$q_append" -initrd $q_initrd -drive ${q_drive}
${qemu_executable} -enable-kvm -m $q_mem -vga $q_vga -smp $q_smp -kernel $q_kernel -append "$q_append" -initrd $q_initrd -drive ${q_drive} $q_extras 

exit 0

