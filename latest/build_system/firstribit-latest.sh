#!/bin/bash
# Creation Date: 14feb2022 (greatly modified in 30Jun2024 release)
progname="firstribit.sh"; version="022"; revision="-rc1" #Revision Date: 07July2024
# Copyright wiak (William McEwan) 14feb2022+; Licence MIT (aka X11 license)
# Run this script from directory you want to frugal boot from.

# List follows of URLs for a few distros you might want auto-downloaded,
# particularly those whose download site is difficult to find,
# but overall best to download iso manually
# To expand this you'd also need to add to case statement near line 270:
# 	case "$distro" in  # expand list with suitable distro/URL fetches

# TinyCorePure64-current can be downloaded via: https://ftp.nluug.nl/os/Linux/distr/tinycorelinux/
# The one you need as subdir of above is release.x/x86_64/release/TinyCorePure64-current.iso. Example provided currently:
URL18="https://ftp.nluug.nl/os/Linux/distr/tinycorelinux/15.x/x86_64/release/TinyCorePure64-current.iso"
# Fedora can cloud image isos can be downloaded via https://fedoraproject.org/cloud/ and click tab "Get Fedora"
# I then used 'Spins' LXQt but plenty of alternatives are available there
URL33="https://download.fedoraproject.org/pub/fedora/linux/releases/40/Spins/x86_64/iso/Fedora-LXQt-Live-x86_64-40-1.14.iso"

firstribitplugin="f_it00.plug"			# contains extra commandlines to execute at very end of build
										# For example, using wget to download more addons, or could perhaps:
										# Uncompress main root filesystem (sometimes) and chroot into it and
										# run package manager commands and/or config scripts and so on...
[ "$1" ] && firstribitplugin="$1"		# optional first parameter specifies alternative fxxxit... plugin name										

_firstribit_plug (){
  [ -s ./"${firstribitplugin}" ] && . ./"${firstribitplugin}"
}

_get_FR_initrd (){
  wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/initrd-latest.gz -O initrd.gz  # FR skeleton initrd
  wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/w_init-latest -O w_init  # FR w_init
  # Some useful FirstRib utilities in case you want to modify the initrd or the 07firstrib_rootfs
  # All these utilities have --help option
  wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/wd_grubconfig && chmod +x wd_grubconfig  # When run finds correct grub menu stanza for your system
  wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/modify_initrd_gz.sh && chmod +x modify_initrd_gz.sh  # For 'experts' to modify initrd.gz
  wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/mount_chroot.sh && chmod +x mount_chroot.sh  # To enter rootfs in a chroot
  wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/umount_chroot.sh && chmod +x umount_chroot.sh  # to 'clean up mounts used by above mount_chroot.sh'
  mv initrd.gz initrdSKEL.gz
  mkdir -p initrd_decompressed
  cd initrd_decompressed
  sync;sync
  zcat ../initrdSKEL.gz | cpio -idm
  cp -a /tmp/firstribit/fsroot/usr/lib/modules/* usr/lib/modules/		
  echo "Result being compressed. Please wait patiently ..."
  sync;sync
  find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
  cd ..
}
_get_FR_initrd_kmod (){ # kmod needed just now for zstd compressed modules
  wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/initrd_kmod-latest.gz -O initrd.gz # FR skeleton initrd_kmod
  wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/w_init-latest -O w_init  # FR w_init
  wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/wd_grubconfig && chmod +x wd_grubconfig  # When run finds correct grub menu stanza for your system
  wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/modify_initrd_gz.sh && chmod +x modify_initrd_gz.sh  # For 'experts' to modify initrd.gz
  wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/mount_chroot.sh && chmod +x mount_chroot.sh  # To enter rootfs in a chroot
  wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/umount_chroot.sh && chmod +x umount_chroot.sh  # to 'clean up mounts used by above mount_chroot.sh'
  mv initrd.gz initrdSKEL.gz
  mkdir -p initrd_decompressed
  cd initrd_decompressed
  sync;sync
  zcat ../initrdSKEL.gz | cpio -idm
  cp -a /tmp/firstribit/fsroot/usr/lib/modules/* usr/lib/modules/		
  echo "Result being compressed. Please wait patiently ..."
  sync;sync
  find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
  cd ..
}

_get_layer_addons01 (){
  wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/12KL_gtkdialogGTK3filemnt64.tar
  tar xvf 12KL_gtkdialogGTK3filemnt64.tar
  sleep 1
  rm 12KL_gtkdialogGTK3filemnt64.tar
  mv 12* 12w_mods01
}

_set_passwords (){ # for root user and normal user firstrib
  # So here is how to make a temporary overlay of ro X.sfs and rw directory 12w_mods01/
  [ ! -d 12w_mods01 ] && mkdir -p 12w_mods01
  mkdir -p work
  sudo mount -t overlay -o \
lowerdir=/tmp/firstribit/fsroot,\
upperdir=12w_mods01,\
workdir=work \
overlay_result /tmp/firstribit/merged
  # simple chroot but not mounting /proc /dev and so on here - might need in more complex additions followed by umount -l. Following may need 'tweaked' but seems ok
  cat << INSIDE_CHROOT | LC_ALL=C chroot /tmp/firstribit/merged sh
export PATH=/sbin:/usr/sbin:/bin:/usr/bin
printf "root\nroot" | passwd root >/dev/null 2>&1 # Quietly set default root passwd to "root"
useradd -m -s /bin/bash firstrib # create user firstrib with home dir and bash sh
printf "firstrib\nfirstrib" | passwd firstrib >/dev/null 2>&1 # Quietly set default firstrib passwd to "firstrib"
usermod -G sudo firstrib # adds firstrib to group sudo if it exists
usermod -G wheel firstrib # adds firstrib to group wheel if it exists
exit
INSIDE_CHROOT
}

_set_passwords2 (){
  cat << INSIDE_CHROOT2 | LC_ALL=C chroot "$1" sh
export PATH=/sbin:/usr/sbin:/bin:/usr/bin
printf "root\nroot" | passwd root >/dev/null 2>&1 # Quietly set default root passwd to "root"
useradd -m -s /bin/bash firstrib # create user firstrib with home dir and bash sh
printf "firstrib\nfirstrib" | passwd firstrib >/dev/null 2>&1 # Quietly set default firstrib passwd to "firstrib"
usermod -G sudo firstrib # adds firstrib to group sudo if it exists
usermod -G wheel firstrib # adds firstrib to group wheel if it exists
exit
INSIDE_CHROOT2
}

_grub_config (){
	cd "$HERE"
	subdir="$bootdir"
	bootuuid=`df . | awk '/^\/dev/ {print $1}' | xargs blkid -s UUID | awk -F\" '{print $2}'`
	bootlabel=`df . | awk '/^\/dev/ {print $1}' | xargs blkid -s LABEL | awk -F\" '{print $2}'`
	printf "
You should now create appropriate grub.conf or menu.lst stanza
If different, substitute in your own uuid and bootdir_name details
Check the actual names for files vmlinuz and initrd.gz in bootdir_name

#####menu.lst (note the LABEL or UUID options below):
title $subdir
  find --set-root --ignore-floppies /${subdir}/grub_config.txt
  kernel /$subdir/vmlinuz w_bootfrom=LABEL=${bootlabel}=/$subdir w_changes=RAM2
  initrd /$subdir/initrd.gz
#############################OR uuid method:
title $subdir
  find --set-root uuid () $bootuuid
  kernel /$subdir/vmlinuz w_bootfrom=UUID=${bootuuid}=/$subdir w_changes=RAM2
  initrd /$subdir/initrd.gz

#####grub.cfg (note the UUID or LABEL options below):
menuentry \"${subdir}\" {
  insmod ext2
  search --no-floppy --label $bootlabel --set
  linux /$subdir/vmlinuz w_bootfrom=LABEL=${bootlabel}=/$subdir w_changes=RAM2
  initrd /$subdir/initrd.gz
}
#############################OR uuid method:
menuentry \"${subdir}\" {
  insmod ext2
  search --no-floppy --fs-uuid --set $bootuuid
  linux /$subdir/vmlinuz w_bootfrom=UUID=${bootuuid}=/$subdir w_changes=RAM2
  initrd /$subdir/initrd.gz
}

Once grub is configured you should be able to boot the new install

FOR LOGIN user:password check distro websites or try 
firstrib:firstrib or root:root; very occasionally password is blank
Rarely, you need to edit upper_changes/etc/passwd to remove root
password (which is the x in for example: root:x:0:0:root:/root:/bin/bash)

Refer to $HERE/grub_config.txt for
copy of this information plus blkid info\n" | tee grub_config.txt
	blkid -s UUID >> grub_config.txt
}

# Following under test - only using for EndeavourOS and Pop!_OS at the moment
_extractiso () { # 14Apr2022 provided by Puppy forum member Keef, thanks!
    echo "Extracting large files from iso. Please wait patiently ..."
    mount $isoname /tmp/firstribit/miso
    cp -a $(find /tmp/firstribit/miso -name "vmlinuz*" | head -n 1) vmlinuz
    sqfs=$(find /tmp/firstribit/miso -type f -exec file {} \; | grep "Squashfs" | cut -d":" -f1)
    cp -a "$sqfs" 08filesystem.sfs
    sync;sync
    [ -s 08filesystem.sfs ] && mount 08filesystem.sfs /tmp/firstribit/fsroot  # for most common case
}

_extractiso_many () {
    echo "Extracting large files from iso. Please wait patiently ..."
    mount $isoname /tmp/firstribit/miso
    cp -a $(find /tmp/firstribit/miso -name "vmlinuz*" | head -n 1) vmlinuz
    find /tmp/firstribit/miso -type f -exec file {} \; | grep "Squashfs" | cut -d":" -f1 | xargs -I {} cp -a {} .
    sync;sync
    [ -s 08filesystem.sfs ] && mount 08filesystem.sfs /tmp/firstribit/fsroot  # for most common case
}

case "$1" in
  '--version') printf "$progname ${version}${revision}\n"; exit 0;;
  ''|'-h'|'--help'|'-?') 
		printf "$progname ${version}${revision}\n\n"
		printf "For optimum find iso reliability it is best to manually download
the iso to be FirstRibbed into the build directory and when firsribit
is executed, choose that same distro variant from menu of choices\n\n"
		printf "To make a FirstRibbed frugal install using FR wiak skeleton initrd

Simply execute this script with command:

./$progname optional_f_it_plugname\n"
		printf "
Note (I have to check this): for vanilladpup and some other newer pups 
you may need extra grub (kernel) linux argument: fwmod=usrlib

Once grub is configured you should be able to boot the new install

FOR LOGIN user:password check distro websites or try 
firstrib:firstrib or root:root; very occasionally password is blank
Rarely, you need to edit upper_changes/etc/passwd to remove root
password (which is the x in for example: root:x:0:0:root:/root:/bin/bash)\n";	exit 0;;
	'q') exit;;
esac

# Distros currently able to be converted
distro01="FirstRib anypup (default BookwormPup64_10.0.6.iso; rough shutdown)"
distro02="PopOS64"
distro03="EndeavourOS"
distro04="Manjaro"
distro07="MakuluLinux-Shift-x64-Ubuntu"
distro08="MX Linux X64"
distro13="Zorin"
distro14="NeonKDE"
distro15="Garuda"
distro16="Bodhi"
distro17="MassOS"
distro18="TinyCoreLinux64"
distro20="Intel Clear Linux"
distro21="Ubuntu_live"
distro23="linux-lite"
distro24="bunsen-lithium"
distro25="LXLE-Focal"
distro26="tails-amd64"
distro27="Debian_live"
distro28="Regata_OS"
distro29="Kodachi"
distro30="SysLinuxOS"
distro31="4MLinux" # extremely experimental FR conversion!
distro33="Fedora"
distro34="Xubuntu"
distro35="Lubuntu"
distro36="Mabox"
distro37="Sparky"
distro38="Spiral"
distro44="Linux Mint"
distro98="Generic Arch Linux based"
distro99="Generic Debian/Ubuntu based"

printf "
Make a FirstRibbed frugal install using FR skeleton wiak initrd
Choose a distro to firstribit

1 $distro01 firstribed using FR wiak initrd
2 $distro02
3 $distro03
4 $distro04
7 $distro07
8 $distro08
13 $distro13
14 $distro14
15 $distro15 (warning: Cancel out of disk partitioning install)
16 $distro16
17 $distro17
18 $distro18
20 $distro20
21 $distro21
23 $distro23
24 $distro24
25 $distro25
26 $distro26
27 $distro27
28 $distro28
29 $distro29
30 $distro30
31 $distro31
33 $distro33
34 $distro34
35 $distro35
36 $distro36
37 $distro37
38 $distro38
44 $distro44
98 $distro98
99 $distro99
q quit (more choices may come in later release)
Enter distro choice (i.e. 1, 2, 3 ...etc) or press q to quit: " 
# note: users are invited to add additional distros above!
read distro

# Create working directories
HERE="`pwd`"
bootdir=`basename "$HERE"` # for use with grub config
mkdir -p /tmp/firstribit/miso /tmp/firstribit/miso2 /tmp/firstribit/fsroot /tmp/firstribit/merged

if [ "$distro" = "999" ];then  # [ "$distro" = "999" -o $distro = "1000" ]
  :
else
while [ ! -s *.iso ]; do
	printf "\nYou need to download the relevant iso from
its website prior to continuing. 
Make sure to only have one iso in the install directory.
Once you have done the above, press Enter to continue, or q to quit\n"
	read getiso
	[ "$getiso" = "q" ] && exit 0
	case "$distro" in  # expand list with suitable distro/URL fetches
	  18) echo Downloading iso. Please wait patiently...
	  wget -c $URL18  # TinyCorePure64-current
	  ;;
	  33) echo Downloading iso. Please wait patiently...
	  wget -c $URL33  # Fedora LXQt Spin latest stable
	  ;;
	esac
done
isoname=*.iso
fi

case "$distro" in
  'q') exit;;
  1)	#distro="FR(KL)_Bookwormpup (can modify for ANY pup)"
		_extractiso_many
		pupversion=`ls puppy* | grep -o '[^puppy].*sfs'`
		echo pupversion is $pupversion
		mv puppy${pupversion} 08puppy${pupversion}		
		mv zdrv${pupversion} 00zdrv${pupversion}
		mv fdrv${pupversion} 01fdrv${pupversion}
		mv bdrv${pupversion} 02bdrv${pupversion}
		mv adrv${pupversion} 14adrv${pupversion}
		mv ydrv${pupversion} 15ydrv${pupversion}		
		kernelversion=`ls kbuild* | grep -o '[^kbuild-].*[^.sfs]'`		
		printf "if available, might also want:
16kbuild-${kernelversion}.sfs, 17devx_${pupversion}
18docx_${pupversion}, 19nslx_${pupversion}
20kernel_sources-${kernelversion}-kernel-kit.sfs\n"
		wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/initrd-latest.gz -O initrd.gz  # FR skeleton initrd
		# Some useful FirstRib utilities in case you want to modify the initrd or the 07firstrib_rootfs
		# All these utilities have --help option
		wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/wd_grubconfig && chmod +x wd_grubconfig  # When run finds correct grub menu stanza for your system
		wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/modify_initrd_gz.sh && chmod +x modify_initrd_gz.sh  # For 'experts' to modify initrd.gz
		wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/mount_chroot.sh && chmod +x mount_chroot.sh  # To enter rootfs in a chroot
		wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/umount_chroot.sh && chmod +x umount_chroot.sh  # to 'clean up mounts used by above mount_chroot.sh'
		wget -c --no-check-certificate https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/25KL_forkpup_type1.tar
		tar xvf 25KL_forkpup_type1.tar
		[ "$mvpoweroff" = "true" ] && mv 25KL_forkpup_type1/usr/sbin/ 25KL_forkpup_type1/
#		_get_layer_addons02
		sleep 1; rm 25KL_forkpup_type1.tar
		;;
  2)	#distro="pop-os"
		_extractiso
		_get_FR_initrd
		_get_layer_addons01
		_set_passwords
		;;
  3)	# distro="EndeavourOS"
		_extractiso
		_get_FR_initrd_kmod
		_get_layer_addons01
		_set_passwords
		;;
  4)	#distro="Manjaro"
		_extractiso_many
		mv rootfs.sfs 08filesystem.sfs
		mv desktopfs.sfs 09desktopfs.sfs
		mv mhwdfs.sfs 10mhwdfs.sfs
		mv livefs.sfs 11livefs.sfs
		mount 08filesystem.sfs /tmp/firstribit/fsroot
		_get_FR_initrd_kmod
		_get_layer_addons01
		_set_passwords
		;;
  7)	#distro="MakuluLinux-Shift-x64-U"
		_extractiso
		_get_FR_initrd
		_get_layer_addons01
		_set_passwords
		;;
  8)	# distro="MX"
		_extractiso
		_get_FR_initrd
		_get_layer_addons01
		_set_passwords
		;;
  13)	#distro="Zorin-OS"
		_extractiso
		_get_FR_initrd
		_get_layer_addons01
		_set_passwords
		;;
  14)	distro="neon-user"
		_extractiso
		_get_FR_initrd
		_get_layer_addons01
		_set_passwords
		;;
  15)	#distro="garuda"
		_extractiso_many
		mv rootfs.sfs 08filesystem.sfs
		mv desktopfs.sfs 09desktopfs.sfs
		mv mhwdfs.sfs 10mhwdfs.sfs
		mv livefs.sfs 11livefs.sfs
		mount 08filesystem.sfs /tmp/firstribit/fsroot
		_get_FR_initrd_kmod
		_get_layer_addons01
		_set_passwords
		;;
  16)	#distro="bodhi"
		_extractiso
		_get_FR_initrd
		_get_layer_addons01
		_set_passwords
		;;
  17)	#distro="massos" (discontinued upstream?)
		_extractiso
		_get_FR_initrd #wiak remove: can't remember if needs kmod version - distro is discontinued though
		_get_layer_addons01
		_set_passwords
		;;
  18)	# Best to fetch TinyCorePure64-current via: https://ftp.nluug.nl/os/Linux/distr/tinycorelinux/
		# The one you need as subdir of above is release_number.x/x86_64/release/TinyCorePure64-current.iso
		# For example: https://ftp.nluug.nl/os/Linux/distr/tinycorelinux/15.x/x86_64/release/TinyCorePure64-current.iso
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		# Previously used tcz repos via: MIRROR="${MIRROR%/}/$(getMajorVer).x/$BUILD/tcz"
		# where MIRROR=repo.tinycorelinux.net and BUILD=x86_64
		# e.g. https://ftp.nluug.nl/os/Linux/distr/tinycorelinux/15.x/x86_64/tcz/rsync.tcz
		# and checked dependencies in file file.tcz.dep

		mount $isoname /tmp/firstribit/miso
		cp -a /tmp/firstribit/miso/cde/optional/* .
		rm -f *.md5.txt
		c=16
		for f in *.tcz; do  # rename required tcz apps to numbered sfs form
		  mv "$f" "${c}${f}.sfs"
		  c=$((c+1))
		done
		# need rsync and its deps libattr, libacl, and bash plus deps for snapmergepuppy so fetching here assuming tinycore 15.x version:
		for g in rsync attr acl bash ncursesw readline findutils coreutils gmp; do  # rename required tcz apps to numbered sfs form
		  f=${g}.tcz
		  wget -c https://ftp.nluug.nl/os/Linux/distr/tinycorelinux/15.x/x86_64/tcz/${f}
		  mv "$f" "${c}${f}.sfs"
		  c=$((c+1))
		done
#		OR can use individual wget commands:
#		wget -c https://ftp.nluug.nl/os/Linux/distr/tinycorelinux/15.x/x86_64/tcz/rsync.tcz
#		mv rsync.tcz "${c}rsync.sfs"; c=$((c+1))
#		wget -c https://ftp.nluug.nl/os/Linux/distr/tinycorelinux/15.x/x86_64/tcz/attr.tcz
#		mv rsync.tcz "${c}rsync.sfs"; c=$((c+1))
#		wget -c https://ftp.nluug.nl/os/Linux/distr/tinycorelinux/15.x/x86_64/tcz/acl.tcz
#		mv rsync.tcz "${c}rsync.sfs"; c=$((c+1))
		
		mkdir -p 08core # for core TCL root filesystem (uncompressed)
		cp -a /tmp/firstribit/miso/boot/corepure64.gz 08core/
		cd 08core && zcat corepure64.gz | cpio -idm
		sync;sync
		# sed -i.bak '/USER="tc"/iNOZSWAP=1' etc/init.d/tc-config
		# now done by /usr/local/tc_installed.sh as is echo Xfbdev > etc/sysconfig/Xserver
		rm -f corepure64.gz
		cd ..
		wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/initrd-latest.gz -O initrd.gz  # FR skeleton initrd
		# Some useful FirstRib utilities in case you want to modify the initrd or the 07firstrib_rootfs
		# All these utilities have --help option
		wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/wd_grubconfig && chmod +x wd_grubconfig  # When run finds correct grub menu stanza for your system
		wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/modify_initrd_gz.sh && chmod +x modify_initrd_gz.sh  # For 'experts' to modify initrd.gz
		wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/mount_chroot.sh && chmod +x mount_chroot.sh  # To enter rootfs in a chroot
		wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/umount_chroot.sh && chmod +x umount_chroot.sh  # to 'clean up mounts used by above mount_chroot.sh'
	#wiak remove: will change name and download from firstrib gitlab on later release
		wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/WDLfork_TCL.tar
		tar xvf WDLfork_TCL.tar
		sync;sync
		# fetch KLV-Airedale kernel components (TCL kernel has no overlayfs)
		echo "Fetching huge kernel/modules/firmware. Please wait patiently ..."
		wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/huge_kernels/kernel_usrmerge01/vmlinuz  # kernel
		wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/huge_kernels/kernel_usrmerge01/00modules_usrmerge1.sfs # modules
		wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/huge_kernels/kernel_usrmerge01/01firmware_usrmerge1.sfs  # firmware
		rm -f WDLfork_vdpup.tar
		sync;sync
		_get_layer_addons01
		sed -i 's@PATH="@PATH="/usr/local/bin:@' 12w_mods01/usr/bin/snapmergepuppy
		ln -s /usr/local/bin/bash 08core/bin/bash
#		_set_passwords
		;;
  20)	#distro="clearlinux"
		mount $isoname /tmp/firstribit/miso
		# horrible hack, but works to mount raw img "rootfs.img" as 08filesystem.sfs later:
		cp -a /tmp/firstribit/miso/images/rootfs.img 08filesystem.sfs
		sync;sync
		mount 08filesystem.sfs /tmp/firstribit/fsroot
		cp -a /tmp/firstribit/miso/kernel/kernel.xz vmlinuz
		_get_FR_initrd_kmod  # seems to be needed cos of zstd
		_get_layer_addons01
		_set_passwords
		;;
  21)	#distro="Ubuntu"
		_extractiso
		_get_FR_initrd
		_get_layer_addons01
		_set_passwords
		;;
  23)	#distro="linux-lite"
		_extractiso
		_get_FR_initrd
		_get_layer_addons01
		_set_passwords
		;;
  24)	#distro="bunsen-lithium"
		_extractiso
		_get_FR_initrd
		_get_layer_addons01
		_set_passwords
		;;
  25)	#distro="LXLE-Focal" (discontinued upstream?)
		_extractiso
		_get_FR_initrd
		_get_layer_addons01
		_set_passwords
		;;
  26)	#distro="tails-amd64"
		_extractiso
		_get_FR_initrd
		_get_layer_addons01
		_set_passwords
		;;
  27)	#distro="Debian"
		_extractiso
		_get_FR_initrd
		_get_layer_addons01
		_set_passwords
		;;
  28)	#distro="Regata_OS"
		# Needing extra img decompress so cannot use this here: _extractiso
		echo "Extracting large files from iso. Please wait patiently ..."
		mount $isoname /tmp/firstribit/miso
		sleep 1
		mount /tmp/firstribit/miso/LiveOS/squashfs.img /tmp/firstribit/miso2
		sleep 1
		mount /tmp/firstribit/miso2/LiveOS/rootfs.img /tmp/firstribit/fsroot		
		sleep 1
		cp -a $(find /tmp/firstribit/fsroot -type f -name "vmlinuz*" | head -n 1) vmlinuz
		mksquashfs /tmp/firstribit/fsroot/ 08filesystem.sfs
		wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/initrd_kmod-latest.gz -O initrd.gz # FR skeleton initrd_kmod
		wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/w_init-latest -O w_init  # FR w_init
		wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/wd_grubconfig && chmod +x wd_grubconfig  # When run finds correct grub menu stanza for your system
		wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/modify_initrd_gz.sh && chmod +x modify_initrd_gz.sh  # For 'experts' to modify initrd.gz
		wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/mount_chroot.sh && chmod +x mount_chroot.sh  # To enter rootfs in a chroot
		wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/umount_chroot.sh && chmod +x umount_chroot.sh  # to 'clean up mounts used by above mount_chroot.sh'
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_get_layer_addons01
		_set_passwords # (password is blank, can: sudo password root to user:pw root:root)
		;;
  29)	#distro="Kodachi"
		_extractiso
		_get_FR_initrd
		_get_layer_addons01
		_set_passwords
		;;
  30)	#distro="SysLinuxOS"
		_extractiso
		_get_FR_initrd
		_get_layer_addons01
		_set_passwords
		;;
  31)	#distro="4MLinux"
		# Needing extra img decompress so cannot use this here: _extractiso
		echo "Extracting large files from iso. Please wait patiently ..."
		mount $isoname /tmp/firstribit/miso
		sleep 1
		mkdir -p 08initrd_decomp
		cd 08initrd_decomp
		zcat /tmp/firstribit/miso/boot/initrd.gz | cpio -idm
		tar xJvf /tmp/firstribit/miso/drivers/addon_modules-all*
		tar xJvf /tmp/firstribit/miso/drivers/addon_firmware*
		k=`ls lib/modules`
		depmod -a $k -b .
		cd ..
		mkdir -p initrd2_decomp
		cd initrd2_decomp
		zcat /tmp/firstribit/miso/boot/initrd2.gz | cpio -idm
		cd ..
		cp -a /tmp/firstribit/miso/boot/bzImage vmlinuz		
		_get_FR_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a ../08initrd_decomp/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_get_layer_addons01
		_set_passwords # maybe not needed else password is blank or maybe austrumi, can: sudo password root to user:pw root:root
		;;
  33)	#distro="Fedora"
		# Fedora can cloud image isos can be downloaded via https://fedoraproject.org/cloud/ and click tab "Get Fedora"
		# I then used 'Spins' LxQt but plenty of alternatives are available there
		# Needing extra img decompress so cannot use this here: _extractiso
		echo "Extracting large files from iso. Please wait patiently ..."
		mount $isoname /tmp/firstribit/miso
		sleep 1
		mount /tmp/firstribit/miso/LiveOS/squashfs.img /tmp/firstribit/miso2
		sleep 1
		mount /tmp/firstribit/miso2/LiveOS/rootfs.img /tmp/firstribit/fsroot		
		sleep 1
		cp -a /tmp/firstribit/fsroot/etc/dnf/dnf.conf .
		mksquashfs /tmp/firstribit/fsroot/ 08filesystem.sfs
		wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/initrd_kmod-latest.gz -O initrd.gz # FR skeleton initrd_kmod
		wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/w_init-latest -O w_init  # FR w_init
		wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/wd_grubconfig && chmod +x wd_grubconfig  # When run finds correct grub menu stanza for your system
		wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/modify_initrd_gz.sh && chmod +x modify_initrd_gz.sh  # For 'experts' to modify initrd.gz
		wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/mount_chroot.sh && chmod +x mount_chroot.sh  # To enter rootfs in a chroot
		wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/umount_chroot.sh && chmod +x umount_chroot.sh  # to 'clean up mounts used by above mount_chroot.sh'
		echo "Fetching huge kernel/modules/firmware. Please wait patiently ..."
		wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/huge_kernels/kernel_usrmerge01/vmlinuz  # kernel
		wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/huge_kernels/kernel_usrmerge01/00modules_usrmerge1.sfs # modules
		wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/huge_kernels/kernel_usrmerge01/01firmware_usrmerge1.sfs  # firmware
		sync;sync
		_get_layer_addons01
		#Avoid updating/fetching new kernels/modules later
		mkdir -p 12w_mods01/etc/dnf/
		cp -a dnf.conf 12w_mods01/etc/dnf/dnf.conf
		echo exclude=kernel* redhat-release* >> 12w_mods01/etc/dnf/dnf.conf
		_get_layer_addons01
		_set_passwords
		;;
  34)	#distro="xubuntu"
		_extractiso
		_get_FR_initrd
		_get_layer_addons01
		_set_passwords
		;;
  35)	#distro="lubuntu"
		_extractiso
		_get_FR_initrd
		_get_layer_addons01
		_set_passwords
		;;
  36)	#distro="mabox" # thanks TerryH
		_extractiso_many
		mv rootfs.sfs 08filesystem.sfs
		mv desktopfs.sfs 09desktopfs.sfs
		mv mhwdfs.sfs 10mhwdfs.sfs
		mv livefs.sfs 11livefs.sfs
		mount 08filesystem.sfs /tmp/firstribit/fsroot
		_get_FR_initrd_kmod
		_get_layer_addons01
		_set_passwords
		;;
  37)	#distro="Sparky" # Thanks esos
		_extractiso
		_get_FR_initrd
		_get_layer_addons01
		_set_passwords
		;;
  38)	#distro="Spiral"
		_extractiso
		_get_FR_initrd
		_get_layer_addons01
		_set_passwords
		;;
  44)	#distro="Linux Mint"
		_extractiso
		_get_FR_initrd
		_get_layer_addons01
		_set_passwords
		;;
  98)	#distro="generic Arch Linux based"
		_extractiso_many
		mv rootfs.sfs 08filesystem.sfs
		mv desktopfs.sfs 09desktopfs.sfs
		mv mhwdfs.sfs 10mhwdfs.sfs
		mv livefs.sfs 11livefs.sfs
		mount 08filesystem.sfs /tmp/firstribit/fsroot
		_get_FR_initrd_kmod
		_get_layer_addons01
		_set_passwords  # not sure this works with Arch Linux based distros, but shouldn't cause harm(?)
		;;
  99)	#distro="generic Debian/Ubuntu based"
		_extractiso
		_get_FR_initrd
		_get_layer_addons01
		_set_passwords
		;;
	# note: users are invited to add additional distros above!
	*)	printf "$progname ${version}${revision}\n"; exit 0;;
esac

# clean up
sync;sync
umount -l /tmp/firstribit/merged
umount -l /tmp/firstribit/fsroot
umount -l /tmp/firstribit/miso
umount -l /tmp/firstribit/miso2
rm -rf /tmp/firstribit/
rm -rf initrd_decompressed
_grub_config
printf "\nNOTE that a ${firstribitplugin} plugin file has been detected.
This is simply a text file containing extra commands.
You can optionally modify its contents right now if you wish, then:
Press Enter key to use its commands,
or q to quit without using the plugin: "
read choice
if [ "$choice" = "q" ]; then
  exit 0
else
  _firstribit_plug  # source f_it plug file containing any extra commands you want to do.
					# For example use wget to download an addon or, say,
					# uncompress and modify an existing sfs file,
					# make an iso of frugal result, or whatever...
fi
exit 0
