#!/bin/bash
# FRU_mini_jammy_wayland00.sh
# wiak Created: 15Aug2023 Revised: 21Aug2023 Licence: MIT
# version 1.00 -rc1

# General Build Instructions:
# Create an empty directory at root of partition you want to bootfrom
# For example: /KLU_minijam
# In a terminal opened at that bootfrom directory simply run this single script!!! ;-)

# Fetch the build_firstrib_rootfs build parts:
wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/build_firstrib_rootfs.sh && chmod +x build_firstrib_rootfs.sh

# wiak minimal Ubuntu Jammy build plugin used during the build (you can add to this plugin for whatever extras you want in your build)
wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/noX_f_plugs/f_00_Ubuntu_jammy_amd64_Wayland00_wiak-rc1.plug

# Download the boot components:
wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/initrd-latest.gz -O initrd.gz  # FR skeleton initrd
wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/huge_kernels/kernel_usrmerge01/vmlinuz  # kernel
wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/huge_kernels/kernel_usrmerge01/00modules_usrmerge1.sfs # modules
wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/huge_kernels/kernel_usrmerge01/01firmware_usrmerge1.sfs  # firmware

# Some useful FirstRib utilities in case you want to modify the initrd or the 07firstrib_rootfs
# All these utilities have a --help option
wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/wd_grubconfig && chmod +x wd_grubconfig  # When run finds correct grub menu stanza for your system
wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/modify_initrd_gz.sh && chmod +x modify_initrd_gz.sh  # For 'experts' to modify initrd.gz
wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/mount_chroot.sh && chmod +x mount_chroot.sh  # To enter rootfs in a chroot
wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/umount_chroot.sh && chmod +x umount_chroot.sh  # to 'clean up mounts used by above mount_chroot.sh'

# Optional addon layers

# Main KL addon containing the likes of gtkdialog, filemnt, UExtract, gxmessage, save2flash and more
# save2flash works with command-line-only distros too
wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/12KL_gtkdialogGTK3filemnt64.sfs

#### Remove below comment if you just want the downloaded parts...
#exit

# Build the Void Linux root filesystem to firstrib_rootfs directory
# NOTE WELL: If you have an alternative f_plugin in your bootfrom directory (name must start with f_),
# simply alter below command to use it
./build_firstrib_rootfs.sh ubuntu jammy amd64 f_00_Ubuntu_jammy_amd64_Wayland00_wiak-rc1.plug

# Squash up the rootfs and number the layer ready for booting
mksquashfs firstrib_rootfs/ 07firstrib_rootfs.sfs

# Alternatively you can comment out the above line and uncomment the following:
#mv firstrib_rootfs 07firstrib_rootfs

# or alternatively, you can make a pseudo-full-install, that will allow direct updates:
#mkdir -p 07dummy
#rm -rf upper_changes  # BE CAREFUL. Assuming new install so upper_changes should not exist anyway
#mv firstrib_rootfs upper_changes

# The only thing now to do is find correct grub stanza for your system
printf "\nPress any key to run utility wd_grubconfig
which will output suitable exact grub stanzas
Use one of these with your pre-installed grub
Press enter to finish\n"
read choice
./wd_grubconfig
exit 0

