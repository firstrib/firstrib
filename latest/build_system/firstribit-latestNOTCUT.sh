#!/bin/bash
# Creation Date: 14feb2022 (greatly modified in 26Jun2024 release)
progname="firstribit.sh"; version="021"; revision="-rc2" #Revision Date: 26Jun2024
# Copyright wiak (William McEwan) 14feb2022+; Licence MIT (aka X11 license)
# Run this script from directory you want to frugal boot from.

_get_FR_initrd (){
  wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/initrd-latest.gz -O initrd.gz  # FR skeleton initrd
  # Some useful FirstRib utilities in case you want to modify the initrd or the 07firstrib_rootfs
  # All these utilities have --help option
  wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/wd_grubconfig && chmod +x wd_grubconfig  # When run finds correct grub menu stanza for your system
  wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/modify_initrd_gz.sh && chmod +x modify_initrd_gz.sh  # For 'experts' to modify initrd.gz
  wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/mount_chroot.sh && chmod +x mount_chroot.sh  # To enter rootfs in a chroot
  wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/umount_chroot.sh && chmod +x umount_chroot.sh  # to 'clean up mounts used by above mount_chroot.sh'
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
}
_get_FR_initrd_kmod (){ # kmod needed just now for zstd compressed modules
  wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/initrd_kmod-latest.gz -O initrd.gz # FR skeleton initrd_kmod
  wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/wd_grubconfig && chmod +x wd_grubconfig  # When run finds correct grub menu stanza for your system
  wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/modify_initrd_gz.sh && chmod +x modify_initrd_gz.sh  # For 'experts' to modify initrd.gz
  wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/mount_chroot.sh && chmod +x mount_chroot.sh  # To enter rootfs in a chroot
  wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/umount_chroot.sh && chmod +x umount_chroot.sh  # to 'clean up mounts used by above mount_chroot.sh'
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
}

_get_layer_addons01 (){
  wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/12KL_gtkdialogGTK3filemnt64.sfs
}

_set_passwords (){ # for root user and normal user firstrib
  # So here is how to make a temporary overlay of ro X.sfs and rw upper_changes
  sudo mount -t overlay -o \
lowerdir=/tmp/firstribit/fsroot,\
upperdir=upper_changes,\
workdir=work \
overlay_result /tmp/firstribit/merged
  # simple chroot but not mounting /proc /dev and so on here - might need in more complex additions followed by umount -l. Following may need 'tweaked' but seems ok
  cat << INSIDE_CHROOT | LC_ALL=C chroot /tmp/firstribit/merged sh
export PATH=/sbin:/usr/sbin:/bin:/usr/bin
printf "root\nroot" | passwd root >/dev/null 2>&1 # Quietly set default root passwd to "root"
useradd -m -s /bin/bash firstrib # create user firstrib with home dir and bash sh
printf "firstrib\nfirstrib" | passwd firstrib >/dev/null 2>&1 # Quietly set default firstrib passwd to "firstrib"
usermod -G sudo firstrib # adds firstrib to group sudo if it exists
usermod -G wheel firstrib # adds firstrib to group wheel if it exists
exit
INSIDE_CHROOT
}

_set_passwords2 (){
  cat << INSIDE_CHROOT2 | LC_ALL=C chroot "$1" sh
export PATH=/sbin:/usr/sbin:/bin:/usr/bin
printf "root\nroot" | passwd root >/dev/null 2>&1 # Quietly set default root passwd to "root"
useradd -m -s /bin/bash firstrib # create user firstrib with home dir and bash sh
printf "firstrib\nfirstrib" | passwd firstrib >/dev/null 2>&1 # Quietly set default firstrib passwd to "firstrib"
usermod -G sudo firstrib # adds firstrib to group sudo if it exists
usermod -G wheel firstrib # adds firstrib to group wheel if it exists
exit
INSIDE_CHROOT2
}

_grub_config (){
	cd "$HERE"
	subdir="$bootdir"
	bootuuid=`df . | awk '/^\/dev/ {print $1}' | xargs blkid -s UUID | awk -F\" '{print $2}'`
	bootlabel=`df . | awk '/^\/dev/ {print $1}' | xargs blkid -s LABEL | awk -F\" '{print $2}'`
	printf "
You should now create appropriate grub.conf or menu.lst stanza
If different, substitute in your own uuid and bootdir_name details
Check the actual names for files vmlinuz and initrd.gz in bootdir_name

#####menu.lst (note the LABEL or UUID options below):
title $subdir
  find --set-root --ignore-floppies /${subdir}/grub_config.txt
  kernel /$subdir/vmlinuz w_bootfrom=LABEL=${bootlabel}=/$subdir w_changes=RAM2
  initrd /$subdir/initrd.gz
#############################OR uuid method:
title $subdir
  find --set-root uuid () $bootuuid
  kernel /$subdir/vmlinuz w_bootfrom=UUID=${bootuuid}=/$subdir w_changes=RAM2
  initrd /$subdir/initrd.gz

#####grub.cfg (note the UUID or LABEL options below):
menuentry \"${subdir}\" {
  insmod ext2
  search --no-floppy --label $bootlabel --set
  linux /$subdir/vmlinuz w_bootfrom=LABEL=${bootlabel}=/$subdir w_changes=RAM2
  initrd /$subdir/initrd.gz
}
#############################OR uuid method:
menuentry \"${subdir}\" {
  insmod ext2
  search --no-floppy --fs-uuid --set $bootuuid
  linux /$subdir/vmlinuz w_bootfrom=UUID=${bootuuid}=/$subdir w_changes=RAM2
  initrd /$subdir/initrd.gz
}

Once grub is configured you should be able to boot the new install

FOR LOGIN user:password check distro websites or try 
firstrib:firstrib or root:root; very occasionally password is blank
Rarely, you need to edit upper_changes/etc/passwd to remove root
password (which is the x in for example: root:x:0:0:root:/root:/bin/bash)

Refer to $HERE/grub_config.txt for
copy of this information plus blkid info\n" | tee grub_config.txt
	blkid -s UUID >> grub_config.txt
}

# Following under test - only using for EndeavourOS and Pop!_OS at the moment
_extractiso () { # 14Apr2022 provided by Puppy forum member Keef, thanks!
    echo "Extracting large files from iso. Please wait patiently ..."
    mount $isoname /tmp/firstribit/miso
    cp -a $(find /tmp/firstribit/miso -name "vmlinuz*" | head -n 1) vmlinuz
    sqfs=$(find /tmp/firstribit/miso -type f -exec file {} \; | grep "Squashfs" | cut -d":" -f1)
    cp -a "$sqfs" 08filesystem.sfs
    sync;sync
    mount 08filesystem.sfs /tmp/firstribit/fsroot
}

_extractiso_many () {
    echo "Extracting large files from iso. Please wait patiently ..."
    mount $isoname /tmp/firstribit/miso
    cp -a $(find /tmp/firstribit/miso -name "vmlinuz*" | head -n 1) vmlinuz
    find /tmp/firstribit/miso -type f -exec file {} \; | grep "Squashfs" | cut -d":" -f1 | xargs -I {} cp -a {} .
    sync;sync
}

#
distro01="FirstRib anypup (default BookwormPup64_10.0.6.iso; rough shutdown)"
distro02="PopOS64"
distro03="EndeavourOS XFCE"
distro04="manjaro-xfce"
distro05="manjaro-kde"
distro06="manjaro-gnome"
distro07="MakuluLinux-Shift-x64-Ubuntu"
distro08="MX-21_X64 XFCE"
distro09="MX-21_X64 XFCE ahs" # advanced hardware support - newer PCs
distro10="MX-21_X64 KDE" # with ahs for newer PCs 
distro11="MX-21_X64 fluxbox" # 64bit variant
distro12="NOT WORKING YET MX-21_X32 fluxbox" # 32bit variant
distro13="Zorin Lite XFCE"
distro14="NeonKDE"
distro15="GarudaSway"
distro16="Bodhi Std"
distro17="MassOS"
distro18="TinyCoreLinux64 flwm"
distro19="to be decided"
distro20="Intel Clear Linux"
distro21="Ubuntu_live"
distro22="Mint Cinnamon live"
distro23="linux-lite"
distro24="bunsen-lithium"
distro25="LXLE-Focal"
distro26="tails-amd64"
distro27="Debian_live"
distro28="Regata_OS"
distro29="Kodachi"
distro30="SysLinuxOS"
distro31="4MLinux" # extremely experimental FR conversion!
distro32="austrumi"
distro33="Fedora Rawhide cmdline"
distro34="Xubuntu"
distro35="Lubuntu"
distro36="Mabox"
distro37="Sparky mingui"
distro38="Sparky lxqt"
distro39="Sparky mate"
distro40="Sparky xfce"
distro41="Sparky kde"
distro42="Sparky gameover"
distro43="Sparky multimedia"
distro44="Mint XFCE live"

if [ $1 ];then
  distro="$1"
else
  printf "
Make a FirstRibbed frugal install using FR skeleton wiak initrd
Choose a distro to firstribit

1 $distro01 firstribed using FR wiak initrd
2 $distro02
3 $distro03
4 $distro04
5 $distro05
6 $distro06
7 $distro07
8 $distro08
9 $distro09
10 $distro10
11 $distro11
12 $distro12
13 $distro13
14 $distro14
15 $distro15 (warning: Cancel out of disk partitioning install)
16 $distro16
17 $distro17
18 $distro18
19 $distro19
20 $distro20
21 $distro21
22 $distro22
23 $distro23
24 $distro24
25 $distro25
26 $distro26
27 $distro27
28 $distro28
29 $distro29
30 $distro30
31 $distro31
32 $distro32
33 $distro33
34 $distro34
35 $distro35
36 $distro36
37 $distro37
38 $distro38
39 $distro39
40 $distro40
41 $distro41
42 $distro42
43 $distro43
44 $distro44
q quit (more choices may come in later release)
Enter distro choice (i.e. 1, 2, 3 ...etc) to use script's URL for the
iso OR press q to quit, and optionally update URL and re-run: " 
# note: users are invited to add additional distros above!
  read distro
fi

case "$distro" in
  '--version') printf "$progname ${version}${revision}\n"; exit 0;;
  ''|'-h'|'--help'|'-?') 
		printf "$progname ${version}${revision}\n\n"
		printf "For optimum find iso reliability it is best to manually download
the iso to be FirstRibbed into the build directory and when firsribit
is executed, choose that same distro variant from menu of choices\n\n"
		printf "To make a FirstRibbed frugal install using FR wiak skeleton initrd

Simply execute this script with command:

./$progname\n"
		printf "
Note (I have to check this): for vanilladpup and some other newer pups 
you may need extra grub (kernel) linux argument: fwmod=usrlib

Once grub is configured you should be able to boot the new install

FOR LOGIN user:password check distro websites or try 
firstrib:firstrib or root:root; very occasionally password is blank
Rarely, you need to edit upper_changes/etc/passwd to remove root
password (which is the x in for example: root:x:0:0:root:/root:/bin/bash)\n";	exit 0;;
	'q') exit;;
esac

# Create working directories
HERE="`pwd`"
bootdir=`basename "$HERE"` # for use with grub config
mkdir -p /tmp/firstribit/miso /tmp/firstribit/miso2 /tmp/firstribit/fsroot /tmp/firstribit/merged upper_changes work

while [ ! -s *.iso ]; do
	printf "\nYou need to download the relevant iso from
its website prior to continuing. 
Make sure to only have one iso in the install directory.
Once you have done the above, press Enter to continue, or q to quit\n"
	read getiso
	[ "$getiso" = "q" ] && exit 0
done
isoname=*.iso

case "$distro" in
  1)	#distro="KL_anypup"
		_extractiso_many  # extracts sfs files and vmlinuz from iso
		pupversion=`ls puppy* | grep -o '[^puppy].*sfs'`
		echo pupversion is $pupversion
		mv puppy${pupversion} 08puppy${pupversion}		
		mv zdrv${pupversion} 00zdrv${pupversion}
		mv fdrv${pupversion} 01fdrv${pupversion}
		mv bdrv${pupversion} 02bdrv${pupversion}
		mv adrv${pupversion} 20adrv${pupversion}
		mv ydrv${pupversion} 21ydrv${pupversion}		
		kernelversion=`ls kbuild* | grep -o '[^kbuild-].*[^.sfs]'`		
		printf "if available, might also want:
21kbuild-${kernelversion}.sfs, 22devx_${pupversion}
23docx_${pupversion}, 24nslx_${pupversion}
24kernel_sources-${kernelversion}-kernel-kit.sfs\n"
		_get_FR_initrd
		wget -c --no-check-certificate https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/09KL_forkpup_type1.tar
		tar xvf 09KL_forkpup_type1.tar
		[ $mvpoweroff = "true" ] && mv 09KL_forkpup_type1/usr/sbin/ 09KL_forkpup_type1/
		;;
  2)	#distro="pop-os"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd
#		_set_passwords
		_get_layer_addons01
		;;
  3)	# distro="EndeavourOS"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd_kmod
#		_set_passwords
		_get_layer_addons01
		;;
  4)	#distro="manjaro-xfce"
		_extractiso_many
		mv rootfs.sfs 04rootfs.sfs
		mv desktopfs.sfs 05desktopfs.sfs
		mv mhwdfs.sfs 06mhwdfs.sfs
		mv livefs.sfs 07livefs.sfs
		_get_FR_initrd
#		_set_passwords
		_get_layer_addons01
		;;
  5)	#distro="manjaro-kde"
		_extractiso_many
		mv rootfs.sfs 04rootfs.sfs
		mv desktopfs.sfs 05desktopfs.sfs
		mv mhwdfs.sfs 06mhwdfs.sfs
		mv livefs.sfs 07livefs.sfs
		_get_FR_initrd
#		_set_passwords
		_get_layer_addons01
		;;
  6)	#distro="manjaro-gnome"
		_extractiso_many
		mv rootfs.sfs 04rootfs.sfs
		mv desktopfs.sfs 05desktopfs.sfs
		mv mhwdfs.sfs 06mhwdfs.sfs
		mv livefs.sfs 07livefs.sfs
		_get_FR_initrd
#		_set_passwords
		_get_layer_addons01
		;;
  7)	#distro="MakuluLinux-Shift-x64-U"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd_kmod
		_set_passwords
		_get_layer_addons01
		;;
  8)	# distro="MX" # XFCE stable for PC a few years old
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd_kmod
		_set_passwords
		_get_layer_addons01
		;;
  9)	#distro="MX" # XFCE advanced hardware support (ahs) for newer machines
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd_kmod
		_set_passwords
		_get_layer_addons01
		;;
  10)	#distro="MX" # KDE with advanced hardware support (ahs) for newer machines
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd_kmod
		_set_passwords
		_get_layer_addons01
		;;
  11)	#distro="MX" # fluxbox with advanced hardware support (ahs) for newer machines
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd_kmod
		_set_passwords
		_get_layer_addons01
		;;
  12)	#distro="MX" # fluxbox 32bit
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd_kmod
		_set_passwords
		_get_layer_addons01
		;;
  13)	#distro="Zorin-OS"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd_kmod
		_set_passwords
		_get_layer_addons01
		;;
  14)	distro="neon-user"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd_kmod
		_set_passwords
		_get_layer_addons01
		;;
  15)	distro="garuda"
		subdir="$subdirectory15"
	if [ -s *.iso ];then
		isoname=*.iso
	else
		if [ "$URL15" = "" ];then
			:
		fi
		wget -c --no-check-certificate "${URL15}"
		echo "Extracting large files from iso. Please wait patiently ..."
		sync;sync
		isoname="${URL15##*/}"
	fi
		mount $isoname /tmp/firstribit/miso
		cp -a /tmp/firstribit/miso/${distro}/x86_64/rootfs.sfs 04rootfs.sfs
		cp -a /tmp/firstribit/miso/${distro}/x86_64/desktopfs.sfs 05desktopfs.sfs
		cp -a /tmp/firstribit/miso/${distro}/x86_64/mhwdfs.sfs 06mhwdfs.sfs
		cp -a /tmp/firstribit/miso/${distro}/x86_64/livefs.sfs 07livefs.sfs
		sync;sync
		mount 04rootfs.sfs /tmp/firstribit/fsroot
		cp -a /tmp/firstribit/fsroot/boot/vmlinuz* vmlinuz
		_get_FR_initrd_kmod
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
#		_set_passwords
		_get_layer_addons01
		;;
  16)	#distro="bodhi"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd_kmod
		_set_passwords
		_get_layer_addons01
		;;
  17)	#distro="massos"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd_kmod
#		_set_passwords
		_get_layer_addons01
		;;
  18)	#distro="TinyCorePure64-current"LXLE-Focal 
		subdir="$subdirectory18"
		# tcz repos via: MIRROR="${MIRROR%/}/$(getMajorVer).x/$BUILD/tcz"
		# where MIRROR=repo.tinycorelinux.net and BUILD=x86_64
		# e.g. https://repo.tinycorelinux.net/13.x/x86_64/tcz/file.tcz
		# and check dependencies in file file.tcz.dep
		# or fetch via: https://ftp.nluug.nl/os/Linux/distr/tinycorelinux/
		
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
	if [ -s *.iso ];then
		isoname=*.iso
	else
		if [ "$URL18" = "" ];then
			:
		fi
		wget -c --no-check-certificate "${URL18}"
		echo "Extracting large files from iso. Please wait patiently ..."
		sync;sync
		isoname="${URL18##*/}"
	fi
		mount $isoname /tmp/firstribit/miso
		cp -a /tmp/firstribit/miso/cde/optional/* .
		rm -f *.md5.txt
		c=11
		for f in *.tcz; do  # rename required tcz apps to numbered sfs form
		  mv "$f" "${c}${f}.sfs"
		  c=$((c+1))
		done
		mkdir -p 08core # for core TCL root filesystem (uncompressed)
		cp -a /tmp/firstribit/miso/boot/corepure64.gz 08core/
		cd 08core && zcat corepure64.gz | cpio -idm
		sync;sync
		# sed -i.bak '/USER="tc"/iNOZSWAP=1' etc/init.d/tc-config
		# now done by /usr/local/tc_installed.sh as is echo Xfbdev > etc/sysconfig/Xserver
		rm -f corepure64.gz
		cd ..
		wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/initrd-latest.gz -O initrd.gz  # FR skeleton initrd
		# Some useful FirstRib utilities in case you want to modify the initrd or the 07firstrib_rootfs
		# All these utilities have --help option
		wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/wd_grubconfig && chmod +x wd_grubconfig  # When run finds correct grub menu stanza for your system
		wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/modify_initrd_gz.sh && chmod +x modify_initrd_gz.sh  # For 'experts' to modify initrd.gz
		wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/mount_chroot.sh && chmod +x mount_chroot.sh  # To enter rootfs in a chroot
		wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/umount_chroot.sh && chmod +x umount_chroot.sh  # to 'clean up mounts used by above mount_chroot.sh'
		wget -c --no-check-certificate https://owncloud.rockedge.org/index.php/s/gK3wm2Z7QdoqHe7/download -O WDLfork_TCL.tar
		tar xvf WDLfork_TCL.tar
		sync;sync
		# fetch KLV-Airedale kernel components (TCL kernel has no overlayfs)
		echo "Fetching huge kernel/modules/firmware. Please wait patiently ..."
		mkdir -p kernelmodules
		cd kernelmodules
	#wiak remove: will change this to newer kernel
		wget -c --no-check-certificate https://rockedge.org/kernels/data/Kernels/64bit/5.14.1_untested/huge-5.14.1-bionicpup64.tar.bz2
		tar xjvf huge-5.14.1-bionicpup64.tar.bz2
		mv vmlinuz-5.14.1-bionicpup64 ../vmlinuz
		mv kernel-modules-5.14.1-bionicpup64.sfs ../00kernel-modules-5.14.1-bionicpup64.sfs
		mv fdrv-5.14.1-bionicpup64.sfs ../01fdrv-5.14.1-bionicpup64.sfs
		rm -f WDLfork_vdpup.tar
		sync;sync
		cd ..
		;;
  19)	#distro="to be decided"
		:
		;;
  20)	#distro="clearlinux"
		mount $isoname /tmp/firstribit/miso
		# horrible hack, but works to mount raw img "rootfs.img" as 08filesystem.sfs later:
		cp -a /tmp/firstribit/miso/images/rootfs.img 08filesystem.sfs
		sync;sync
		mount 08filesystem.sfs /tmp/firstribit/fsroot
		cp -a /tmp/firstribit/miso/kernel/kernel.xz vmlinuz
		_get_FR_initrd
#		_set_passwords
		_get_layer_addons01
		;;
  21)	#distro="Ubuntu"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd_kmod
		_set_passwords
		_get_layer_addons01
		;;
  22)	#distro="MintCinnamon"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd_kmod
		_set_passwords
		_get_layer_addons01
		;;
  23)	#distro="linux-lite"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd_kmod
		_set_passwords
		_get_layer_addons01
		;;
  24)	#distro="bunsen-lithium"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd_kmod
		_set_passwords
		_get_layer_addons01
		;;
  25)	#distro="LXLE-Focal"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd_kmod
		_set_passwords
		_get_layer_addons01
		;;
  26)	#distro="tails-amd64"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd_kmod
		_set_passwords
		_get_layer_addons01
		;;
  27)	#distro="Debian"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd_kmod
		_set_passwords
		_get_layer_addons01
		;;
  28)	#distro="Regata_OS"
		# Needing extra img decompress so cannot use this here: _extractiso
		echo "Extracting large files from iso. Please wait patiently ..."
		mount $isoname /tmp/firstribit/miso
		sleep 1
		mount /tmp/firstribit/miso/LiveOS/squashfs.img /tmp/firstribit/miso2
		sleep 1
		mount /tmp/firstribit/miso2/LiveOS/rootfs.img /tmp/firstribit/fsroot		
		sleep 1
		cp -a $(find /tmp/firstribit/fsroot -name "vmlinuz*default") vmlinuz
		mksquashfs /tmp/firstribit/fsroot/ 08filesystem.sfs
		_get_FR_initrd_kmod
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
#		_set_passwords (password is blank, can: sudo password root to user:pw root:root)
		_get_layer_addons01
		;;
  29)	#distro="Kodachi"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd_kmod
		_set_passwords
		_get_layer_addons01
		;;
  30)	#distro="SysLinuxOS"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd_kmod
		_set_passwords
		_get_layer_addons01
		;;
  31)	#distro="4MLinux"
		# Needing extra img decompress so cannot use this here: _extractiso
		echo "Extracting large files from iso. Please wait patiently ..."
		mount $isoname /tmp/firstribit/miso
		sleep 1
		mkdir -p 08initrd_decomp
		cd 08initrd_decomp
		zcat /tmp/firstribit/miso/boot/initrd.gz | cpio -idm
		tar xJvf /tmp/firstribit/miso/drivers/addon_modules-all*
		tar xJvf /tmp/firstribit/miso/drivers/addon_firmware*
		k=`ls lib/modules`
		depmod -a $k -b .
		cd ..
		mkdir -p initrd2_decomp
		cd initrd2_decomp
		zcat /tmp/firstribit/miso/boot/initrd2.gz | cpio -idm
		cd ..
		cp -a /tmp/firstribit/miso/boot/bzImage vmlinuz		
		_get_FR_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a ../08initrd_decomp/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		# _set_passwords (password is blank or maybe austrumi, can: sudo password root to user:pw root:root)
		_get_layer_addons01
		;;
  32)	#distro="austrumi"
		mount $isoname /tmp/firstribit/miso
		sleep 1
		for i in austrumi.fs bzImage intel-ucode.cpio amd-ucode.cpio austrumi.tgz message.msg; do
			cp -a /tmp/firstribit/miso/austrumi/${i} .
		done
		sync;sync
		unsquashfs -d 08filesystem austrumi.fs
		mv bzImage vmlinuz
		tar xzvf austrumi.tgz -C 08filesystem
		cd 08filesystem/boot
		cat > FR_aust_rc.sysinit << "CODE_FOR_ROOTFS_RC_SYSINIT"
#!/bin/sh
# FR_austrumi_rc.sysinit (12 Aug 2022) is slight modification of following build_wiak_initrd script:
# rc.sysinit: Copyright William McEwan (wiak) 16 July 2019; Licence MIT (aka X11 license). Revision 2.0.9 28 Dec 2020

# Add to PATH in case not done. /usr/local/firstrib/bin last in preference here so overwritten by same-named core apps
export PATH=/usr/local/bin:/usr/local/sbin:/sbin:/usr/sbin:/bin:/usr/bin
# The first part of the following is modified/skeleton extract from
# Void Linux /etc/runit/core-services/00-pseudofs.sh so ready for runit mod if wanted
mkdir -p -m0755 /proc /sys/kernel/security /run /dev /tmp
#msg "Mounting pseudo-filesystems..."
mountpoint -q /proc || mount -o nosuid,noexec,nodev -t proc proc /proc
mountpoint -q /sys || mount -o nosuid,noexec,nodev -t sysfs sys /sys
mountpoint -q /run || mount -o mode=0755,nosuid,nodev,size=$((`free | grep 'Mem: ' | tr -s ' ' | cut -f 4 -d ' '`/4))k -t tmpfs run /run
mountpoint -q /dev || mount -o mode=0755,nosuid -t devtmpfs dev /dev
mkdir -p -m0755 /run/runit /run/lvm /run/user /run/lock /run/log /dev/pts /dev/shm
mountpoint -q /dev/pts || mount -o mode=0620,gid=5,nosuid,noexec -n -t devpts devpts /dev/pts
mountpoint -q /dev/shm || mount -o mode=1777,nosuid,nodev,size=$((`free | grep 'Mem: ' | tr -s ' ' | cut -f 4 -d ' '`/4))k -n -t tmpfs shm /dev/shm
mountpoint -q /tmp || mount -t tmpfs -o mode=1777,nosuid,nodev,size=$((`free | grep 'Mem: ' | tr -s ' ' | cut -f 4 -d ' '`/4))k tmpfs /tmp
mountpoint -q /sys/kernel/security || mount -n -t securityfs securityfs /sys/kernel/security
[ -x /etc/rc.local ] && /etc/rc.local	# If /etc/rc.local script exists and is executable, run it
										# User can add custom commands into that script
echo "Starting udev and waiting for devices to settle..." >/dev/console
udevd --daemon
udevadm trigger --action=add --type=subsystems
udevadm trigger --action=add --type=devices
udevadm settle
exec /sbin/init 4 </dev/console >/dev/console 2>&1
exit
CODE_FOR_ROOTFS_RC_SYSINIT
		chmod +x FR_aust_rc.sysinit
		cd ..
		cp -af etc/skel/. root
		sed -i "s@proc/cmdline)@proc/cmdline | grep -oE 'lang_.*[^ ]')@" etc/rc.d/rc.M
		cd ..
		_get_FR_initrd
		sed -i 's@/sbin/init@/boot/FR_aust_rc.sysinit@' w_init
#		_set_passwords2 "08filesystem"
		echo "Result being compressed. Please wait patiently ..."
		# doesn't mount it when sfs!!! so no good: mksquashfs 08filesystem/ 08filesystem.sfs
		sync;sync
		# doesn't mount it when sfs!!! so no good: rm -rf 08filesystem/ austrumi.fs austrumi.tgz 
		rm -rf austrumi.fs austrumi.tgz
		_get_layer_addons01
		;;
  33)	#distro="Fedora Rawhide"
		# Currently auto-downloading and using fdstrap () to create the base fedora rootfs build.
		# It simply downloads Fedora provided tar.gz of fedora rawhide filesystem. https://gitlab.com/tearch-linux/applications-and-tools
		wget -c https://gitlab.com/tearch-linux/applications-and-tools/fdstrap/-/raw/main/fdstrap.sh && chmod +x fdstrap.sh
		sed -i 's@fedora-release@fedora-release kernel linux-firmware NetworkManager NetworkManager-tui NetworkManager-wifi@' fdstrap.sh
		./fdstrap.sh 08firstrib_rootfs
		_get_FR_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a ../08firstrib_rootfs/usr/lib/modules/* usr/lib/modules/
		cp -a ../08firstrib_rootfs/boot/vmlinuz* ../vmlinuz
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords2 "08firstrib_rootfs"
		_get_layer_addons01
		;;
  34)	#distro="xubuntu"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd_kmod
		_set_passwords
		_get_layer_addons01
		;;
  35)	#distro="lubuntu"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd_kmod
		_set_passwords
		_get_layer_addons01
		;;
  36)	#distro="mabox" # thanks TerryH
		_extractiso_many
		mv rootfs.sfs 04rootfs.sfs
		mv desktopfs.sfs 05desktopfs.sfs
		mv mhwdfs.sfs 06mhwdfs.sfs
		mv livefs.sfs 07livefs.sfs
		_get_FR_initrd
#		_set_passwords
		_get_layer_addons01
		;;
  37)	#distro="sparky minimalgui" # Thanks esos
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd_kmod
		_set_passwords
		_get_layer_addons01
		;;
  38)	#distro="sparky lxqt"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd_kmod
		_set_passwords
		_get_layer_addons01
		;;
  39)	#distro="sparky mate"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd_kmod
		_set_passwords
		_get_layer_addons01
		;;
  40)	#distro="sparky xfce"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd_kmod
		_set_passwords
		_get_layer_addons01
		;;
  41)	#distro="sparky kde"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd_kmod
		_set_passwords
		_get_layer_addons01
		;;
  42)	#distro="sparky gameover"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd_kmod
		_set_passwords
		_get_layer_addons01
		;;
  43)	#distro="sparky multimedia"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd_kmod
		_set_passwords
		_get_layer_addons01
		;;
  44)	#distro="Mintxfce"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd_kmod
		_set_passwords
		_get_layer_addons01
		;;
	# note: users are invited to add additional distros above!
	*)	printf "$progname ${version}${revision}\n"; exit 0;;
esac

# clean up
sync;sync
umount -l /tmp/firstribit/merged
umount -l /tmp/firstribit/fsroot
umount -l /tmp/firstribit/miso
umount -l /tmp/firstribit/miso2
rm -rf /tmp/firstribit/
rm -rf initrd_decompressed

_grub_config
exit 0
