# f_00_Void_xorg_minimal_JWM_no-kernel.plug
# wiak+FRteam Creation date 12Sep2023; Revision date: 12Sep2023
# Copyright FirstRib team 31Aug2023+; Licence MIT (aka X11 license)
# version="0.0.1"; revision="rc1"
# Can boot result using suitable huge-kernel/00modules/01firmware and FR skeleton initrd

## NOTES:
# In the below, 'Mod' means 'Windows key on keyboard'
# REMEMBER to set your own timezone at line 69 below
# For sway usage: refer, for example, to https://i3wm.org/docs/userguide.html
# For differences to i3 usage read: https://github.com/swaywm/sway/wiki/i3-Migration-Guide
# For configured key bindings: cat ~/.config/sway/config
# Mod+Enter to open simple terminal. Mod+Shift+b to open firefox; Mod+d for dmenu in top bar
#   also view file for extra config details: /etc/sway/config.d/100-wiak_global
# You'll likely want to also install pipewire for Audio (not done)
# You can add as many valid commandlines as you want in here. Currently is relatively simplistic...
# Build includes NetworkManager so best use nmtui 'Activate Connection', but also includes simple wiakwifi
# Mod+Shift+e to exit sway back to commandline
# For available Void Linux packages refer to: https://voidlinux.org/packages/
# JUST READ THE FOLLOWING TO UNDERSTAND HOW TO CUSTOMISE THE BUILD!

xbps-install -Syu base-minimal bash sudo rsync
xbps-install -y eudev xdg-utils light chrony  # or maybe use ntpd?
xbps-install -y NetworkManager # includes wpa_supplicant as dependency
xbps-install -Sy dbus-elogind elogind polkit mesa-dri sway dmenu font-bh-ttf
# you optionally may want liberation-fonts-ttf or whatever...

# Choose your apps...
xbps-install -y grim slurp imv lxtask foot sakura geany pcmanfm
# You may want something like: xbps-install -Syu mtpaint firefox

# set up passwd system
pwconv
grpconv
printf "root\nroot\n" | passwd >/dev/null 2>&1 # Quietly set default root passwd to "root"
# set root to use /bin/bash
usermod --shell /bin/bash root

# Set locale to en_US.UTF-8 
sed -i 's/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/default/libc-locales
xbps-reconfigure -f glibc-locales

cp -R /etc/sv/agetty-tty1 /etc/sv/agetty-autologin-tty1
# In following change 'root' for 'spot' if you want autologin as spot
sed -i 's/GETTY_ARGS.*/GETTY_ARGS="--autologin root --noclear"/' /etc/sv/agetty-autologin-tty1/conf

# Use agetty-autologin-tty1 instead of agetty-tty1 
rm -f /etc/runit/runsvdir/default/agetty-tty1
ln -s /etc/sv/agetty-autologin-tty1 /etc/runit/runsvdir/default/agetty-autologin-tty1
touch /etc/sv/agetty-tty1/down

# Remove this section if not wanting boot straight into wayland sway
touch ~/.bash_profile
cat <<'AUTOLOGIN' > /etc/profile.d/autologin.sh
# autologin on tty1
if [ -z "${WAYLAND_DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
  sway   # use 'exec sway' here if you don't want back to tty1 on exit
fi
AUTOLOGIN

# enable some key services to start at boot
ln -s /etc/sv/dbus /etc/runit/runsvdir/default/dbus
ln -s /etc/sv/NetworkManager /etc/runit/runsvdir/default/NetworkManager
ln -s /etc/sv/ntpd /etc/runit/runsvdir/default/ntpd  # /etc/sv/nptd is symlink to chronyd

# Some default configs for root and users
mkdir -p ~/.config/sway
cp -a /etc/sway/config ~/.config/sway
# Set your timezone. Example:
ln -s /usr/share/zoneinfo/Europe/London /etc/localtime
# Some extra sway (global) configs
mkdir -p /etc/sway/config.d
cat <<'SWAYEXTRACONF' > /etc/sway/config.d/100-wiak_global
set $browser  'firefox-wayland'
bindsym $mod+Shift+b exec $browser
SWAYEXTRACONF

cp -af /root/. /etc/skel
mkdir -p /etc/skel/.config /etc/skel/.cache /etc/skel/.local/share

## create user spot and put in wheel group
# Give wheel group nopasswd sudo rights and create spot as wheel group member
echo '%wheel ALL=(ALL) NOPASSWD: ALL' | (VISUAL="tee -a" visudo) # wheel group added to sudo no password required
useradd -m -G audio,video,wheel -s /bin/bash spot
printf "spot\nspot" | passwd spot >/dev/null 2>&1 # Quietly set default spot passwd to "spot"

