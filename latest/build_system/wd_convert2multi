#!/bin/bash
## wd_convert2multi
# Frugal install many instances of Kennels frugal_install(s)
# Revision Date: 23 Sept 2023
# Copyright wiak (William McEwan) 20 Sept 2023+; Licence: MIT

# Cut (or copy) Kennels frugal install or several
# such frugal installs into empty installation sub-directory
# on root (/) of partition (e.g. /KL_frugals) and then
# rename each with extension .MKL (for MultiKL)
# PUt a copy of this script (made executable) into same subdir

progname="wd_convert2multi"; version="1.0.0"; revision="-rc4"

case "$1" in
	'--version') printf "$progname ${version}${revision}\n"; exit 0;;
	'-h'|'--help'|'-?') printf "This script makes cloned frugal installs with
hardly any increase in storage space required. Instead, 
all per distro core FR components are shared via symlinks.

Usage: DISCLAIMER USE AT OWN RISK ENTIRELY
Cut (or copy) Kennels frugal install or several
such frugal installs into empty installation sub-directory
on root (/) of partition (e.g. /KL_frugals)  and then
rename each with extension .MKL (for MultiKL) and finally
run this script from same directory with command:

./wd_convert2multi <n> <levels>

where n is the number of frugal install instances of each
distro frugal install you want (default is n=1)
\n";exit 0;;
	"-*") printf "option $1 not available\n";exit 0;;
esac

_grub_config (){
  level1="";level2="";level3=""  # initialise
  HERE2=`pwd`
  case $subdirs in  # subdirs is global variable...
  1) level2=""
  level1=`basename "$HERE2"`
  ;;
  2) level2=`basename "$HERE2"`
  path_level1=`dirname "$HERE2"`
  level1=`basename "$path_level1"`/
  ;;
  3) level3=`basename "$HERE2"`
  path_level2=`dirname "$HERE2"`
  level2=`basename "$path_level2"`/
  path_level1=`dirname "$path_level2"`
  level1=`basename "$path_level1"`/
  ;;
  esac
  bootdir="${level1}""${level2}""${level3}"
  for bootiso in *.iso; do :;done
  subdir="$bootdir"
  bootuuid=`df . | awk '/^\/dev/ {print $1}' | xargs blkid -s UUID | awk -F\" '{print $2}'`
  bootlabel=`df . | awk '/^\/dev/ {print $1}' | xargs blkid -s LABEL | awk -F\" '{print $2}'`

if [ -e ${HERE}/"wd_multi_grubplug" ];then
  . ${HERE}/"wd_multi_grubplug"  # use grub PLUGIN for non-FR distros
else
if [ "$bootiso" != '*.iso' ];then
isotext="For direct iso boot grub2 configuration:

#####grub.cfg (BOOT from ISO, note the LABEL or UUID options below):
menuentry \"${subdir} ISO LABEL ${bootlabel}\" {
  echo \"Searching partitions for iso. Please wait patiently...\"
  set isopath=/${subdir}/${bootiso}
  search --no-floppy --file --set=root \$isopath
  loopback loop \$isopath
  set root=(loop)
  linux (loop)/vmlinuz w_bootfrom=\$isopath w_changes=LABEL=${bootlabel}=/${subdir} w_changes1=RAM2
  initrd (loop)/initrd.gz
}
#############################OR uuid method:
menuentry \"${subdir} ISO UUID\" {
  echo \"Searching partitions for iso. Please wait patiently...\"
  set isopath=/${subdir}/${bootiso}
  search --no-floppy --file --set=root \$isopath
  loopback loop \$isopath
  set root=(loop)
  linux (loop)/vmlinuz w_bootfrom=\$isopath w_changes=${bootuuid}=/${subdir} w_changes1=RAM2
  initrd (loop)/initrd.gz
}

"
fi
printf "${isotext}For frugal install grub configuration:

Assuming names: kernel is vmlinuz and initrd is initrd.gz and booting is
from this build directory and needed modules and firmware are present:

#####menu.lst (note the LABEL or UUID options below):
title $subdir
  find --set-root --ignore-floppies /${subdir}/grub_config.txt
  kernel /$subdir/vmlinuz w_bootfrom=LABEL=${bootlabel}=/$subdir w_changes=RAM2
  initrd /$subdir/initrd.gz
#############################OR uuid method:
title $subdir
  find --set-root uuid () $bootuuid
  kernel /$subdir/vmlinuz w_bootfrom=UUID=${bootuuid}=/$subdir w_changes=RAM2
  initrd /$subdir/initrd.gz

#####grub.cfg (note the UUID or LABEL options below):
menuentry \"${subdir}\" {
  insmod ext2
  search --no-floppy --label $bootlabel --set
  linux /$subdir/vmlinuz w_bootfrom=LABEL=${bootlabel}=/$subdir w_changes=RAM2
  initrd /$subdir/initrd.gz
}
#############################OR uuid method:
menuentry \"${subdir}\" {
  insmod ext2
  search --no-floppy --fs-uuid --set $bootuuid
  linux /$subdir/vmlinuz w_bootfrom=UUID=${bootuuid}=/$subdir w_changes=RAM2
  initrd /$subdir/initrd.gz
}

Note that you can remove w_changes=RAM2 if you don't want
save session on demand mode.\n" >> grub_config.txt
fi  # of internal grub for FR-based distros
}

_make_instances () { # $1 is number of iso instances wanted
  local c=1
  mkdir -p instance0  # instance0 holds actual build components
  cd instance0
  # extract build components from isofilename into instance0
  echo "Extracting large files from frugal. Please wait patiently ..."
  mv ../* .
  # unsquashfs 07*  # wiak unsquashed main rootfs
  # rm -f 07*  # wiak remove previous 07 sfs
  # mv squashfs-root 07  # wiak unsquashed main rootfs (will use uncompressed)
  [ -e grub_config.txt ] && rm -f grub_config.txt  # or symlink issue later
  sync;sync
  while [ $c -lt $1 ]; do  # does not do this if instance n only 1
    cd ..  # back to isoname (without .MKL) directory
    mkdir -p instance${c}
    cd instance${c}
    # symlink boot components
    for tolink in ../instance0/*; do
      ln -s ${tolink} ${tolink##.*/}
    done
    [ -L upper_changes ] && mv upper_changes 60instance0_uc
    _grub_config
    c=$((c+1))
  done
}

# Number of distro instances wanted
[ "$1" = "" ] && n=1 || n=$1

HERE="`pwd`" # wd_multi start build directory
[ "$2" = "" ] && subdirs=3 || subdirs=$2  # $2 = subdir levels from partition /

# Build n frugal install instances of each provided FR-based frugal
for i in *.MKL; do
  [ "$i" = '*.MKL' ] && break
  isoname=${i%.MKL}
  mv $i $isoname  # rename $isoname.MKL to $isoname
  cd ${isoname}
  _make_instances $n
  cd "$HERE"
  cd ${isoname}/instance0
  _grub_config
  cd "$HERE"
done
[ -e grub_config.txt ] && rm -f grub_config.txt
# will include direct grub2 config for last iso (modify for other iso)
subdirs=1 && _grub_config

# clean up
sync;sync

exit 0

