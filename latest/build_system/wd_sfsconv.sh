#!/bin/sh
# wd_sfsconv: Change squashed filesystem (sfs file) compression
# Default is to zstd -Xcompression-level 19 -b 1024K
# Copyright William McEwan (wiak) 10Apr2023+ Licence MIT 
# Created 10Apr2023; Revised 22Oct2023
progname="wd_sfsconv"; version=0.0.1; revision=-rc2
FRsystem="KL" # change this to "FR" for standard FirstRib distro

# Alternative compressions; alter these to suit your wishes...
conv_zstd="-comp zstd -Xcompression-level 19 -b 1024K"
conv_xz="-b 1024k -comp xz -Xbcj x86"
conv_gz=""

case "$1" in
  '-v'|'--version') printf "${progname}-${version}${revision}\n"; exit 0;;
  '-h'|'--help'|'-?') printf "Convert sfs file to alternative compression format
Execute: wd_sfsconv [default|filename.sfs] [zstd|xz|gz]
If no arguments supplied, default is ??${FRsystem}*.sfs converted to zstd
zstd compression is $conv_zstd
xz compression is $conv_xz
gz compression is ${conv_gz:-default}\n";exit 0;;
esac

if [ "$1" = "" -o "$1" = "default" ]; then
  sfs_in="`printf ??${FRsystem}*`"
else
  sfs_in="$1"
fi
case "$2" in
  ""|"zstd") conv="$conv_zstd";;
  "xz") conv="$conv_xz";;
  "gz") conv="$conv_gz";;
  *)  printf "Compression type $2 not available\n";exit 0;;
esac
echo sfs_in is $sfs_in which is "$sfs_in" and conv is "$conv"

# NOTE: this is a rough and simple script with but few error checks... ;-)
if [ ! -d squashfs-root ]; then
  unsquashfs "$sfs_in"
else
  printf "squashfs-root already exists sorry, exiting...\n"
  exit 1
fi
sync;sync

if ls -A1q ./squashfs-root/ | grep -q .; then
  rm "$sfs_in"
  sleep 2
  mksquashfs squashfs-root/ "$sfs_in" $conv
  sync;sync
  rm -rf squashfs-root/
  echo finished
else
  printf "squashfs-root was empty sorry, exiting...\n"
  exit 2
fi
exit 0
