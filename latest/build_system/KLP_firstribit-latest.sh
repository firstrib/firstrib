#!/bin/sh
# Creation Date: 27Aug2023 (based on old weedogit so some code here not needed)
progname="KLP_firstribit.sh"; version="021"; revision="-rc3" #Revision Date: 27Aug2023
# Copyright wiak (William McEwan) 14feb2022+; Licence MIT (aka X11 license)
# Run this script from directory you want to frugal boot from.
# You can pre-download the iso if you wish into same directory, or
# let firstribit.sh fetch it for you.
# NOTE WELL: If pre-downloading iso it must be stored with exactly same filename as download URLnn filename below

# The following are needed for "KL_anypup (e.g. KL_fossapup)" experimental firstribed builds
# Change Puppy iso URL01 and subdirectory01 name to suit Puppy being firstribed
# For example, could use the following two lines for building FR fossapup:
## URL01="https://distro.ibiblio.org/puppylinux/puppy-fossa/fossapup64-9.5.iso"
## distro01="FirstRib'd fossapup (experimental)"; subdirectory01="KL_fossapup64"; mvpoweroff=true  # since fossa stores poweroff in /sbin not in /usr/sbin

# KLP are all currently fossapup-based; future work needs done on provided simple shutdown/reboot mechanism
URL01="https://rockedge.org/kernels/data/ISO/F96-CE/F96-CE_4.iso"
URL02="http://distro.ibiblio.org/puppylinux/puppy-fossa/fossapup64-9.5.iso"
URL03="https://archive.org/download/puppy_linux_-fossapup64/fossapup64-9.5-mid-2.iso"


# Suggested default frugal install subdirectories
# In practice the build folder name is used by default automatically now
# For example, distro04="ManjaroXFCE"; subdirectory04="ManjaX"
distro01="F96-CE_4.iso"; subdirectory01="firstribit"; mvpoweroff=true
distro02="fossapup64-9.5.iso"; subdirectory01="firstribit"; mvpoweroff=true
distro03="fossapup64-9.5-mid-2.iso"; subdirectory01="firstribit"; mvpoweroff=true

_get_FR_initrd (){
  wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/initrd-latest.gz -O initrd.gz  # FR skeleton initrd
  # Some useful FirstRib utilities in case you want to modify the initrd or the 07firstrib_rootfs
  # All these utilities have a --help option
  wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/wd_grubconfig && chmod +x wd_grubconfig  # When run finds correct grub menu stanza for your system
  wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/modify_initrd_gz.sh && chmod +x modify_initrd_gz.sh  # For 'experts' to modify initrd.gz
  wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/mount_chroot.sh && chmod +x mount_chroot.sh  # To enter rootfs in a chroot
  wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/umount_chroot.sh && chmod +x umount_chroot.sh  # to 'clean up mounts used by above mount_chroot.sh'
}
# Will update the kmod version later...
#_get_FR_initrd_kmod (){ # kmod needed just now for zstd compressed modules
#  wget -c --no-check-certificate https://owncloud.rockedge.org/index.php/s/HRZhsnouSm3Gpf3/download -O modify_initrd_gz.sh && chmod +x modify_initrd_gz.sh
#  wget -c --no-check-certificate https://owncloud.rockedge.org/index.php/s/yIHevOcDCZf1xzX/download -O initrd.gz
#  wget -c --no-check-certificate https://owncloud.rockedge.org/index.php/s/8aFjxy5QrjfyI8o/download -O w_init
#}
_get_FR_initrd_kmod (){
  printf "\nNot working at the moment... come back later... exiting..."
  exit
}

_grub_config (){
	cd "$HERE"
	subdir="$bootdir"
	bootuuid=`df . | awk '/^\/dev/ {print $1}' | xargs blkid -s UUID | awk -F\" '{print $2}'`
	bootlabel=`df . | awk '/^\/dev/ {print $1}' | xargs blkid -s LABEL | awk -F\" '{print $2}'`
	printf "
You should now create appropriate grub.conf or menu.lst stanza
If different, substitute in your own uuid and bootdir_name details
Check the actual names for files vmlinuz and initrd.gz in bootdir_name

#####menu.lst (note the LABEL or UUID options below):
title $subdir
  find --set-root --ignore-floppies /${subdir}/grub_config.txt
  kernel /$subdir/vmlinuz w_bootfrom=LABEL=${bootlabel}=/$subdir w_changes=RAM2 w_00modules=lib
  initrd /$subdir/initrd.gz
#############################OR uuid method:
title $subdir
  find --set-root uuid () $bootuuid
  kernel /$subdir/vmlinuz w_bootfrom=UUID=${bootuuid}=/$subdir w_changes=RAM2 w_00modules=lib
  initrd /$subdir/initrd.gz

#####grub.cfg (note the UUID or LABEL options below):
menuentry \"${subdir}\" {
  insmod ext2
  search --no-floppy --label $bootlabel --set
  linux /$subdir/vmlinuz w_bootfrom=LABEL=${bootlabel}=/$subdir w_changes=RAM2 w_00modules=lib
  initrd /$subdir/initrd.gz
}
#############################OR uuid method:
menuentry \"${subdir}\" {
  insmod ext2
  search --no-floppy --fs-uuid --set $bootuuid
  linux /$subdir/vmlinuz w_bootfrom=UUID=${bootuuid}=/$subdir w_changes=RAM2 w_00modules=lib
  initrd /$subdir/initrd.gz
}
   
Note: for vanilladpup and some other newer pups you need extra 
grub (kernel) linux argument: fwmod=usrlib

Once grub is configured you should be able to boot the new install

FOR LOGIN user:password check distro websites or try 
firstrib:firstrib or root:root; very occasionally password is blank
Rarely, you need to edit upper_changes/etc/passwd to remove root
password (which is the x in for example: root:x:0:0:root:/root:/bin/bash)

Refer to $HERE/grub_config.txt for
copy of this information plus blkid info\n" | tee grub_config.txt
	blkid -s UUID >> grub_config.txt
}

# Following under test - only using for EndeavourOS and Pop!_OS at the moment
_extractiso () { # 14Apr2022 provided by Puppy forum member Keef, thanks!
    echo "Extracting large files from iso. Please wait patiently ..."
    mount "$isoname" /tmp/firstribit/miso
    cp -a $(find /tmp/firstribit/miso -name "vmlinuz*" | head -n 1) vmlinuz
    sqfs=$(find /tmp/firstribit/miso -type f -exec file {} \; | grep "Squashfs" | cut -d":" -f1)
    cp -a "$sqfs" 08filesystem.sfs
    sync;sync
    mount 08filesystem.sfs /tmp/firstribit/fsroot
}

_extractiso_many () {
    echo "Extracting large files from iso. Please wait patiently ..."
    mount "$isoname" /tmp/firstribit/miso
    cp -a $(find /tmp/firstribit/miso -name "vmlinuz*" | head -n 1) vmlinuz
    find /tmp/firstribit/miso -type f -exec file {} \; | grep "Squashfs" | cut -d":" -f1 | xargs -I {} cp -a {} .
    sync;sync
}

if [ $1 ];then
  distro="$1"
else
  printf "
Make a FirstRibed frugal install using FR skeleton wiak initrd
Choose a distro to firstribit

1 $distro01 F96-CE fossapup
2 $distro02 Original FossaPup64
3 $distro03 Fossa64-Mid

q quit (more choices may come in later release)
Enter distro choice (i.e. 1, 2, 3 ...etc) to use script's URL for the
iso OR press q to quit, and optionally update URL and re-run: " 
# note: users are invited to add additional distros above!
  read distro
fi

case "$distro" in
  '--version') printf "$progname ${version}${revision}\n"; exit 0;;
  ''|'-h'|'--help'|'-?') 
		printf "$progname ${version}${revision}\n\n"
		printf "To make a FirstRibed frugal install using FR wiak skeleton initrd

Simply execute this script with command:

./$progname\n"
		printf "
Note: for vanilladpup and some other newer pups you need extra 
grub (kernel) linux argument: fwmod=usrlib

Once grub is configured you should be able to boot the new install

FOR LOGIN user:password check distro websites or try 
firstrib:firstrib or root:root; very occasionally password is blank
Rarely, you need to edit upper_changes/etc/passwd to remove root
password (which is the x in for example: root:x:0:0:root:/root:/bin/bash)\n";	exit 0;;
	'q') exit;;
esac

# Create working directories
HERE="`pwd`"
bootdir=`basename "$HERE"` # for use with grub config
mkdir -p /tmp/firstribit/miso /tmp/firstribit/miso2 /tmp/firstribit/fsroot /tmp/firstribit/merged upper_changes work

case "$distro" in
  1)	distro="KLP_F96-CE"
		subdir="$subdirectory01"
		default_release="Press Enter to accept default or check and change"
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		wget -c --no-check-certificate "${URL01}"
		isoname="${URL01##*/}"
		_extractiso_many  # extracts sfs files and vmlinuz from iso
		_get_FR_initrd
		wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/FR_fork_vdpup.tar -O FR_fork_vdpup.tar
		tar xvf FR_fork_vdpup.tar
		[ $mvpoweroff = "true" ] && mv 09FR_fork_vdpup/usr/sbin/ 09FR_fork_vdpup/
		pupversion=`ls puppy* | grep -o '[^puppy].*sfs'`
		echo pupversion is $pupversion
#		mv puppy${pupversion} 02puppy.sfs
        unsquashfs puppy${pupversion}; mv squashfs-root 02puppy
        rm -rf 02puppy/tmp; mkdir -p 02puppy/tmp	
		mv zdrv${pupversion} 00zdrv_modules.sfs
		mv fdrv${pupversion} 01fdrv_firmware.sfs
		mv bdrv${pupversion} 04bdrv.sfs
		mv adrv${pupversion} 18adrv.sfs
		mv ydrv${pupversion} 19ydrv.sfs
        wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/20KLsave2flash.sfs
		kernelversion=`ls kbuild* | grep -o '[^kbuild-].*[^.sfs]'`		
		printf "if available, might also want:
21kbuild-${kernelversion}.sfs, 22devx_${pupversion}
23docx_${pupversion}, 24nslx_${pupversion}
24kernel_sources-${kernelversion}-kernel-kit.sfs\n"
		;;
  2)	distro="KLP_FossaPup64_orig"
		subdir="$subdirectory02"
		default_release="Press Enter to accept default or check and change"
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		wget -c --no-check-certificate "${URL02}"
		isoname="${URL02##*/}"
		_extractiso_many  # extracts sfs files and vmlinuz from iso
		_get_FR_initrd
		wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/FR_fork_vdpup.tar -O FR_fork_vdpup.tar
		tar xvf FR_fork_vdpup.tar
		[ $mvpoweroff = "true" ] && mv 09FR_fork_vdpup/usr/sbin/ 09FR_fork_vdpup/
		pupversion=`ls puppy* | grep -o '[^puppy].*sfs'`
		echo pupversion is $pupversion
		mv puppy${pupversion} 02puppy.sfs
		mv zdrv${pupversion} 00zdrv_modules.sfs
		mv fdrv${pupversion} 01fdrv_firmware.sfs
		mv bdrv${pupversion} 04bdrv.sfs
		mv adrv${pupversion} 18adrv.sfs
		mv ydrv${pupversion} 19ydrv.sfs
        wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/20KLsave2flash.sfs
		kernelversion=`ls kbuild* | grep -o '[^kbuild-].*[^.sfs]'`		
		printf "if available, might also want:
21kbuild-${kernelversion}.sfs, 22devx_${pupversion}
23docx_${pupversion}, 24nslx_${pupversion}
24kernel_sources-${kernelversion}-kernel-kit.sfs\n"
		;;
  3)	distro="KLP_Fossa64-Mid"
		subdir="$subdirectory03"
		default_release="Press Enter to accept default or check and change"
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		wget -c --no-check-certificate "${URL03}"
		isoname="${URL03##*/}"
		_extractiso_many  # extracts sfs files and vmlinuz from iso
		_get_FR_initrd
		wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/FR_fork_vdpup.tar -O FR_fork_vdpup.tar
		tar xvf FR_fork_vdpup.tar
		[ $mvpoweroff = "true" ] && mv 09FR_fork_vdpup/usr/sbin/ 09FR_fork_vdpup/
		pupversion=`ls puppy* | grep -o '[^puppy].*sfs'`
		echo pupversion is $pupversion
		mv puppy${pupversion} 02puppy.sfs
		mv zdrv${pupversion} 00zdrv_modules.sfs
		mv fdrv${pupversion} 01fdrv_firmware.sfs
		mv bdrv${pupversion} 04bdrv.sfs
		mv adrv${pupversion} 18adrv.sfs
		mv ydrv${pupversion} 19ydrv.sfs
        wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/20KLsave2flash.sfs
		kernelversion=`ls kbuild* | grep -o '[^kbuild-].*[^.sfs]'`		
		printf "if available, might also want:
21kbuild-${kernelversion}.sfs, 22devx_${pupversion}
23docx_${pupversion}, 24nslx_${pupversion}
24kernel_sources-${kernelversion}-kernel-kit.sfs\n"
		;;
	*)	printf "$progname ${version}${revision}\n"; exit 0;;
esac

# clean up
sync;sync
umount -l /tmp/firstribit/merged
umount -l /tmp/firstribit/fsroot
umount -l /tmp/firstribit/miso
umount -l /tmp/firstribit/miso2
rm -rf /tmp/firstribit/
rm -rf initrd_decompressed

_grub_config
exit 0
