#wiak
copy_including_deps()
{
   # if source doesn't exist or target exists, do nothing
   if [ ! -e "$1" -o -e "$INITRAMFS"/"$1" ]; then
      return
   fi

   cp -R --parents "$1" "$INITRAMFS"
   if [ -L "$1" ]; then
      DIR="$(dirname "$1")"
      LNK="$(readlink "$1")"
      copy_including_deps "$(cd "$DIR"; realpath -s "$LNK")"
   fi

   ldd "$1" 2>/dev/null | sed -r "s/.*=>|[(].*//g" | sed -r "s/^\\s+|\\s+\$//" \
     | while read LIB; do
        copy_including_deps "$LIB"
     done

   for MOD in $(find "$1" -type f | grep .ko); do
      for DEP in $(cat /$LMK/modules.dep | fgrep /$(basename $MOD):); do
         copy_including_deps "/$LMK/$DEP"
      done
   done

   shift
   if [ "$1" != "" ]; then
       copy_including_deps "$@"
   fi
}

rm -Rf ${INITRAMFS}/lib/modules/* 

mknod $INITRAMFS/dev/console c 5 1
mknod $INITRAMFS/dev/null c 1 3
mknod $INITRAMFS/dev/ram0 b 1 0
mknod $INITRAMFS/dev/tty1 c 4 1
mknod $INITRAMFS/dev/tty2 c 4 2
mknod $INITRAMFS/dev/tty3 c 4 3
mknod $INITRAMFS/dev/tty4 c 4 4

#copy_including_deps /usr/bin/strace
#copy_including_deps /usr/bin/lsof

copy_including_deps /$LMK/kernel/fs/aufs
copy_including_deps /$LMK/kernel/fs/overlayfs
copy_including_deps /$LMK/kernel/fs/ext2
copy_including_deps /$LMK/kernel/fs/ext3
copy_including_deps /$LMK/kernel/fs/ext4
copy_including_deps /$LMK/kernel/fs/fat
copy_including_deps /$LMK/kernel/fs/nls
copy_including_deps /$LMK/kernel/fs/fuse
copy_including_deps /$LMK/kernel/fs/isofs
copy_including_deps /$LMK/kernel/fs/ntfs
copy_including_deps /$LMK/kernel/fs/ntfs3
copy_including_deps /$LMK/kernel/fs/reiserfs
copy_including_deps /$LMK/kernel/fs/squashfs
# copy_including_deps /$LMK/kernel/crypto # fredx181 of Puppy Linux forum
copy_including_deps /$LMK/kernel/fs/exfat # fredx181
copy_including_deps /$LMK/kernel/fs/btrfs # fredx181

# crc32c is needed for ext4, but I don't know which one, add them all, they are small
find /$LMK/kernel/ | grep crc32c | while read LINE; do
   copy_including_deps $LINE
done

copy_including_deps /$LMK/kernel/drivers/staging/zsmalloc # needed by zram
copy_including_deps /$LMK/kernel/drivers/block/zram
copy_including_deps /$LMK/kernel/drivers/block/loop.*

# usb drivers
copy_including_deps /$LMK/kernel/drivers/usb/storage/usb-storage.*
copy_including_deps /$LMK/kernel/drivers/usb/storage/uas.* # fredx181
copy_including_deps /$LMK/kernel/drivers/usb/host
copy_including_deps /$LMK/kernel/drivers/usb/common
copy_including_deps /$LMK/kernel/drivers/usb/core
copy_including_deps /$LMK/kernel/drivers/hid/usbhid
copy_including_deps /$LMK/kernel/drivers/hid/hid.*
copy_including_deps /$LMK/kernel/drivers/hid/uhid.*
copy_including_deps /$LMK/kernel/drivers/hid/hid-generic.*

# disk and cdrom drivers
copy_including_deps /$LMK/kernel/drivers/cdrom
copy_including_deps /$LMK/kernel/drivers/scsi/sr_mod.*
copy_including_deps /$LMK/kernel/drivers/scsi/sd_mod.*
copy_including_deps /$LMK/kernel/drivers/scsi/scsi_mod.*
copy_including_deps /$LMK/kernel/drivers/scsi/sg.*
copy_including_deps /$LMK/kernel/drivers/ata
copy_including_deps /$LMK/kernel/drivers/nvme
copy_including_deps /$LMK/kernel/drivers/mmc
copy_including_deps /$LMK/kernel/drivers/md # fredx181

# network support drivers
if [ "$NETWORK" = "true" ]; then
   # add all known ethernet drivers
   copy_including_deps /$LMK/kernel/drivers/net/phy/realtek.* # fredx181
   copy_including_deps /$LMK/kernel/drivers/net/ethernet
   copy_including_deps /$LMK/kernel/fs/nfs # fredx181
fi

# copy all custom-built modules
copy_including_deps /$LMK/updates

copy_including_deps /$LMK/modules.*
