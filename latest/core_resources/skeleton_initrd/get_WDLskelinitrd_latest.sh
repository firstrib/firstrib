#!/bin/sh
# Creation Date: 20Jun2020; Revision Date: 22Oct2023
# Copyright wiak (William McEwan) 20Jun2020+; Licence MIT (aka X11 license)
progname="get_WDLskelinitrd.sh"; version="504"; revision="-rc5"
resource=${progname#get_}; resource=${resource%.sh}

case "$1" in
	'--help'|'-h'|'-?') printf "Simply execute this script with command:
./${progname}
to download $resource
to the current directory\n"; exit 0;;
	'--version') printf "$progname ${version}${revision}\n"; exit 0;;
esac

printf "\nPress Enter to download $resource\nor press Ctrl-C to abort download\n" 
read choice # Stop here until the Enter key is pressed or Ctrl-C to abort
wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/modify_initrd_gz.sh && chmod +x modify_initrd_gz.sh  # For 'experts' to modify initrd.gz
wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/initrd-latest.gz # FR skeleton initrd-latest
wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/build_system/w_init-latest  # same as inside FR initrd-latest

exit 0
