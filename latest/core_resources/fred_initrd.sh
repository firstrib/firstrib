#fred
copy_including_deps()
{
   # if source doesn't exist or target exists, do nothing
   if [ ! -e "$1" -o -e "$INITRAMFS"/"$1" ]; then
      return
   fi

   cp -R --parents "$1" "$INITRAMFS"
   if [ -L "$1" ]; then
      DIR="$(dirname "$1")"
      LNK="$(readlink "$1")"
      copy_including_deps "$(cd "$DIR"; realpath -s "$LNK")"
   fi

   ldd "$1" 2>/dev/null | sed -r "s/.*=>|[(].*//g" | sed -r "s/^\\s+|\\s+\$//" \
     | while read LIB; do
        copy_including_deps "$LIB"
     done

   for MOD in $(find "$1" -type f | grep .ko); do
      for DEP in $(cat /$LMK/modules.dep | fgrep /$(basename $MOD):); do
         copy_including_deps "/$LMK/$DEP"
      done
   done

   shift
   if [ "$1" != "" ]; then
       copy_including_deps "$@"
   fi
}
export -f copy_including_deps

copy_including_deps /$LMK/kernel/fs/aufs
copy_including_deps /$LMK/kernel/fs/overlayfs
copy_including_deps /$LMK/kernel/fs/exfat
copy_including_deps /$LMK/kernel/fs/ext2
copy_including_deps /$LMK/kernel/fs/ext3
copy_including_deps /$LMK/kernel/fs/ext4
copy_including_deps /$LMK/kernel/fs/fat
copy_including_deps /$LMK/kernel/fs/nls
copy_including_deps /$LMK/kernel/fs/fuse
copy_including_deps /$LMK/kernel/fs/isofs
copy_including_deps /$LMK/kernel/fs/ntfs
copy_including_deps /$LMK/kernel/fs/reiserfs
copy_including_deps /$LMK/kernel/fs/squashfs
copy_including_deps /$LMK/kernel/crypto
copy_including_deps /$LMK/kernel/fs/btrfs

# crc32c is needed for ext4, but I don't know which one, add them all, they are small
find /$LMK/kernel/ | grep crc32c | while read LINE; do
   copy_including_deps $LINE
done

copy_including_deps /$LMK/kernel/drivers/staging/zsmalloc # needed by zram
copy_including_deps /$LMK/kernel/drivers/block/zram
copy_including_deps /$LMK/kernel/drivers/block/loop.*

# usb drivers
copy_including_deps /$LMK/kernel/drivers/usb/storage/usb-storage.*
copy_including_deps /$LMK/kernel/drivers/usb/storage/uas.*
copy_including_deps /$LMK/kernel/drivers/usb/host
copy_including_deps /$LMK/kernel/drivers/usb/common
copy_including_deps /$LMK/kernel/drivers/usb/core
copy_including_deps /$LMK/kernel/drivers/hid/usbhid
copy_including_deps /$LMK/kernel/drivers/hid/hid.*
copy_including_deps /$LMK/kernel/drivers/hid/uhid.*
copy_including_deps /$LMK/kernel/drivers/hid/hid-generic.*

# disk and cdrom drivers
copy_including_deps /$LMK/kernel/drivers/cdrom
copy_including_deps /$LMK/kernel/drivers/scsi/sr_mod.*
copy_including_deps /$LMK/kernel/drivers/scsi/sd_mod.*
copy_including_deps /$LMK/kernel/drivers/scsi/scsi_mod.*
copy_including_deps /$LMK/kernel/drivers/scsi/sg.*
copy_including_deps /$LMK/kernel/drivers/ata
copy_including_deps /$LMK/kernel/drivers/nvme
copy_including_deps /$LMK/kernel/drivers/mmc
copy_including_deps /$LMK/kernel/drivers/md

# network support drivers
#if [ "$NETWORK" = "true" ]; then
   # add all known ethernet drivers
   copy_including_deps /$LMK/kernel/drivers/net/phy/realtek.*
   copy_including_deps /$LMK/kernel/drivers/net/ethernet
   copy_including_deps /$LMK/kernel/fs/nfs
#fi

# copy all custom-built modules
copy_including_deps /$LMK/updates

copy_including_deps /$LMK/modules.*
