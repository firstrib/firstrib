#!/bin/sh
# Creation Date: 14feb2022
progname="firstribit.sh"; version="021"; revision="-rc2" #Revision Date: 06Oct2022
# Copyright wiak (William McEwan) 14feb2022+; Licence MIT (aka X11 license)
# Run this script from directory you want to frugal boot from.
# You can pre-download the iso if you wish into same directory, or
# let firstribit.sh fetch it for you.
# NOTE WELL: If pre-downloading iso it must be stored with exactly same filename as download URLnn filename below

# Added: linux-lite, bunsen-lithium LXLE-Focal,
# tails-amd64 (part working only - ctrl-alt f2 to commandline login root:root; reboot -f to restart),
# Debian-live, Regata_OS (password is blank, can: sudo password root to user:pw root:root),
# Kodachi, SysLinuxOS, 4Mlinux, austrumi
# Fedora Rawhide, xubuntu, lubuntu
# FOR LOGINS user:passwords: check distro websites or try firstrib:firstrib or root:root

# NOTE WELL: Where possible the upstream distro URL is now being
# scraped automatically below, but some distros still need their URLnn
# addresses manually updated when upstream site updates its distro builds
# If auto-URL fails you can still manually add it (in case statement) instead.
# Currently the below following need manual URL updates: Pop-os, Zorin, Garuda Sway,
# tinycorelinux, Ubuntu, Linux Mint, Linux Lite, Kodachi, 4MLinux, Xubuntu, Lubuntu

# The following are needed for "KL_anypup (e.g. KL_fossapup)" experimental firstribed builds
# Change Puppy iso URL01 and subdirectory01 name to suit Puppy being firstribed
# For example, could use the following two lines for building FR fossapup:
## URL01="https://distro.ibiblio.org/puppylinux/puppy-fossa/fossapup64-9.5.iso"
## distro01="FirstRib'd fossapup (experimental)"; subdirectory01="KL_fossapup64"; mvpoweroff=true  # since fossa stores poweroff in /sbin not in /usr/sbin
# But default Pup is Vdpup64, with auto URL detect (in case statement 01)

# The following distros still require their URLs manually updated
URL02="https://iso.pop-os.org/22.04/amd64/intel/12/pop-os_22.04_amd64_intel_12.iso"
URL13="https://mirror.freedif.org/zorin/16/Zorin-OS-16.1-Lite-64-bit.iso"
URL15="https://iso.builds.garudalinux.org/iso/latest/garuda/sway/latest.iso"
URL18="http://repo.tinycorelinux.net/13.x/x86_64/release/TinyCorePure64-current.iso"
URL21="https://mirror.fsmg.org.nz/ubuntu-releases/22.04.1/ubuntu-22.04.1-desktop-amd64.iso"
URL22="https://mirrors.layeronline.com/linuxmint/stable/21/linuxmint-21-cinnamon-64bit.iso"
URL23="https://osdn.net/dl/linuxlite/linux-lite-6.0-64bit.iso"
URL29="https://sourceforge.net/projects/linuxkodachi/files/kodachi-8.24-64.iso"
URL31="https://sourceforge.net/projects/linux4m/files/40.0/livecd/4MLinux-40.0-64bit.iso"
URL34="https://cdimages.ubuntu.com/xubuntu/releases/22.04.1/release/xubuntu-22.04.1-desktop-amd64.iso"
URL35="https://cdimage.ubuntu.com/lubuntu/releases/22.04.1/release/lubuntu-22.04.1-desktop-amd64.iso"
URL36="https://sourceforge.net/projects/mabox-linux/files/22.08/mabox-linux-22.08-Herbolth-220820-linux515.iso"

# Suggested default frugal install subdirectories
# In practice the build folder name is used by default automatically now
# For example, distro04="ManjaroXFCE"; subdirectory04="ManjaX"
distro01="FirstRib anypup (default vdpup64)"; subdirectory01="KL_vdpup64"
distro02="PopOS64";                         subdirectory02="FR_pop-os" 
distro03="EndeavourOS XFCE";                subdirectory03="FR_endeavXFCE"
distro04="manjaro-xfce";                    subdirectory04="FR_manjaXFCE"
distro05="manjaro-kde";                     subdirectory05="FR_manjaKDE"
distro06="manjaro-gnome";                   subdirectory06="FR_manjaGNOME"
distro07="MakuluLinux-Shift-x64-Ubuntu";    subdirectory07="FR_makuluU"
distro08="MX-21_X64 XFCE";                  subdirectory08="FR_MXxfce"
distro09="MX-21_X64 XFCE ahs";              subdirectory09="FR_MXxfce_ahs" # advanced hardware support - newer PCs
distro10="MX-21_X64 KDE";                   subdirectory10="FR_MXkde"      # with ahs for newer PCs 
distro11="MX-21_X64 fluxbox";               subdirectory11="FR_MxFlux"
distro12="MX-21_386 fluxbox";               subdirectory12="FR_MxFlux32"   # 32bit variant
distro13="Zorin Lite XFCE";                 subdirectory13="FR_zorinlXFCE"
distro14="NeonKDE";                         subdirectory14="FR_neonKDE"
distro15="GarudaSway";                      subdirectory15="FR_garuSWAY"
distro16="Bodhi Std";                       subdirectory16="FR_bod"
distro17="MassOS";                          subdirectory17="FR_masso"
distro18="TinyCoreLinux64 flwm";            subdirectory18="KL_TCL64"
distro19="KLV-Airedale64";                  subdirectory19="KLV-Airedale64" # frugal install from official iso
distro20="Intel Clear Linux";               subdirectory20="FR_intclear"
distro21="Ubuntu_live";                     subdirectory21="FR_bunt"
distro22="Mint Cinnamon live";              subdirectory22="FR_mintCin"
# --- NEW ---
distro23="linux-lite";                      subdirectory23="FR_linlite"
distro24="bunsen-lithium";                  subdirectory24="FR_bunsenlithium"
distro25="LXLE-Focal";                      subdirectory25="FR_LXLE-Focal"
distro26="tails-amd64";                     subdirectory26="FR_tails-amd64"
distro27="Debian_live";                     subdirectory27="FR_debi"
distro28="Regata_OS";                       subdirectory28="FR_regata"
distro29="Kodachi";                         subdirectory29="FR_kodachiXFCE"
distro30="SysLinuxOS";                      subdirectory30="FR_syslinuxMATE"
distro31="4MLinux";                         subdirectory31="FR_4ML"       # extremely experimental FR conversion!
distro32="austrumi";                        subdirectory32="FR_aust"
distro33="Fedora Rawhide cmdline";          subdirectory33="FR_fedraw"
distro34="Xubuntu";                         subdirectory34="FR_xbunt"
distro35="Lubuntu";                         subdirectory35="FR_lbunt"
distro36="Mabox";                           subdirectory36="FR_mabo"
distro37="Sparky mingui";                   subdirectory37="FR_sparkmingui"
distro38="Sparky lxqt";                     subdirectory38="FR_sparklxqt"
distro39="Sparky mate";                     subdirectory39="FR_sparkmate"
distro40="Sparky xfce";                     subdirectory40="FR_sparkxfce"
distro41="Sparky kde";                      subdirectory41="FR_sparkkde"
distro42="Sparky gameover";                 subdirectory42="FR_sparkgameO"
distro43="Sparky multimedia";               subdirectory43="FR_sparkmultimed"

_get_FR_initrd (){
  wget -c --no-check-certificate https://owncloud.rockedge.org/index.php/s/HRZhsnouSm3Gpf3/download -O modify_initrd_gz.sh && chmod +x modify_initrd_gz.sh
  wget -c --no-check-certificate https://owncloud.rockedge.org/index.php/s/CXixDo8Dd2rtxSQ/download -O initrd.gz
  wget -c --no-check-certificate https://owncloud.rockedge.org/index.php/s/8aFjxy5QrjfyI8o/download -O w_init
}
_get_FR_initrd_kmod (){ # kmod needed just now for zstd compressed modules
  wget -c --no-check-certificate https://owncloud.rockedge.org/index.php/s/HRZhsnouSm3Gpf3/download -O modify_initrd_gz.sh && chmod +x modify_initrd_gz.sh
  wget -c --no-check-certificate https://owncloud.rockedge.org/index.php/s/yIHevOcDCZf1xzX/download -O initrd.gz
  wget -c --no-check-certificate https://owncloud.rockedge.org/index.php/s/8aFjxy5QrjfyI8o/download -O w_init
}

_set_passwords (){ # for root user and normal user firstrib
  # So here is how to make a temporary overlay of ro X.sfs and rw upper_changes
  sudo mount -t overlay -o \
lowerdir=/tmp/firstribit/fsroot,\
upperdir=upper_changes,\
workdir=work \
overlay_result /tmp/firstribit/merged
  # simple chroot but not mounting /proc /dev and so on here - might need in more complex additions followed by umount -l. Following may need 'tweaked' but seems ok
  cat << INSIDE_CHROOT | LC_ALL=C chroot /tmp/firstribit/merged sh
export PATH=/sbin:/usr/sbin:/bin:/usr/bin
printf "root\nroot" | passwd root >/dev/null 2>&1 # Quietly set default root passwd to "root"
useradd -m -s /bin/bash firstrib # create user firstrib with home dir and bash sh
printf "firstrib\nfirstrib" | passwd firstrib >/dev/null 2>&1 # Quietly set default firstrib passwd to "firstrib"
usermod -G sudo firstrib # adds firstrib to group sudo if it exists
usermod -G wheel firstrib # adds firstrib to group wheel if it exists
exit
INSIDE_CHROOT
}

_set_passwords2 (){
  cat << INSIDE_CHROOT2 | LC_ALL=C chroot "$1" sh
export PATH=/sbin:/usr/sbin:/bin:/usr/bin
printf "root\nroot" | passwd root >/dev/null 2>&1 # Quietly set default root passwd to "root"
useradd -m -s /bin/bash firstrib # create user firstrib with home dir and bash sh
printf "firstrib\nfirstrib" | passwd firstrib >/dev/null 2>&1 # Quietly set default firstrib passwd to "firstrib"
usermod -G sudo firstrib # adds firstrib to group sudo if it exists
usermod -G wheel firstrib # adds firstrib to group wheel if it exists
exit
INSIDE_CHROOT2
}

_grub_config (){
	cd "$PWD"
	subdir="$bootdir"  # or comment out for suggested bootdir names
	bootuuid=`df . | awk '/^\/dev/ {print $1}' | xargs blkid -s UUID | awk -F\" '{print $2}'`
	bootlabel=`df . | awk '/^\/dev/ {print $1}' | xargs blkid -s LABEL | awk -F\" '{print $2}'`
	printf "
You should now create appropriate grub.conf or menu.lst stanza
If different, substitute in your own uuid and bootdir_name details
Check the actual names for files vmlinuz and initrd.gz in bootdir_name

#####menu.lst (note the LABEL or UUID options below):
title $subdir
  find --set-root --ignore-floppies /${subdir}/grub_config.txt
  kernel /$subdir/vmlinuz w_bootfrom=LABEL=${bootlabel}=/$subdir
  initrd /$subdir/initrd.gz
#############################OR uuid method:
title $subdir
  find --set-root uuid () $bootuuid
  kernel /$subdir/vmlinuz w_bootfrom=UUID=${bootuuid}=/$subdir
  initrd /$subdir/initrd.gz

#####grub.cfg (note the UUID or LABEL options below):
menuentry \"${subdir}\" {
  insmod ext2
  search --no-floppy --label $bootlabel --set
  linux /$subdir/vmlinuz w_bootfrom=LABEL=${bootlabel}=/$subdir
  initrd /$subdir/initrd.gz
}
#############################OR uuid method:
menuentry \"${subdir}\" {
  insmod ext2
  search --no-floppy --fs-uuid --set $bootuuid
  linux /$subdir/vmlinuz w_bootfrom=UUID=${bootuuid}=/$subdir
  initrd /$subdir/initrd.gz
}
   
Note (I have to check this): for vanilladpup and some other newer pups you may need extra 
grub (kernel) linux argument: fwmod=usrlib

Once grub is configured you should be able to boot the new install

FOR LOGIN user:password check distro websites or try 
firstrib:firstrib or root:root; very occasionally password is blank
Rarely, you need to edit upper_changes/etc/passwd to remove root
password (which is the x in for example: root:x:0:0:root:/root:/bin/bash)

Refer to $PWD/grub_config.txt for
copy of this information plus blkid info\n" | tee grub_config.txt
	blkid -s UUID >> grub_config.txt
}

# Following under test - only using for EndeavourOS and Pop!_OS at the moment
_extractiso () { # 14Apr2022 provided by Puppy forum member Keef, thanks!
    echo "Extracting large files from iso. Please wait patiently ..."
    mount "$isoname" /tmp/firstribit/miso
    cp -a $(find /tmp/firstribit/miso -name "vmlinuz*" | head -n 1) vmlinuz
    sqfs=$(find /tmp/firstribit/miso -type f -exec file {} \; | grep "Squashfs" | cut -d":" -f1)
    cp -a "$sqfs" 08filesystem.sfs
    sync;sync
    mount 08filesystem.sfs /tmp/firstribit/fsroot
}

_extractiso_many () {
    echo "Extracting large files from iso. Please wait patiently ..."
    mount "$isoname" /tmp/firstribit/miso
    cp -a $(find /tmp/firstribit/miso -name "vmlinuz*" | head -n 1) vmlinuz
    find /tmp/firstribit/miso -type f -exec file {} \; | grep "Squashfs" | cut -d":" -f1 | xargs -I {} cp -a {} .
    sync;sync
}

if [ $1 ];then
  distro="$1"
else
  printf "
Make a FirstRibed frugal install using FR skeleton wiak initrd
Choose a distro to firstribit

1 $distro01 firstribed using FR wiak initrd, not frugalify
2 $distro02
3 $distro03
4 $distro04
5 $distro05
6 $distro06
7 $distro07
8 $distro08
9 $distro09
10 $distro10
11 $distro11
12 $distro12
13 $distro13
14 $distro14
15 $distro15 (warning: Cancel out of disk partitioning install)
16 $distro16
17 $distro17
18 $distro18
19 $distro19
20 $distro20
21 $distro21
22 $distro22
23 $distro23
24 $distro24
25 $distro25
26 $distro26
27 $distro27
28 $distro28
29 $distro29
30 $distro30
31 $distro31
32 $distro32
33 $distro33
34 $distro34
35 $distro35
36 $distro36
37 $distro37
38 $distro38
39 $distro39
40 $distro40
41 $distro41
42 $distro42
43 $distro43
q quit (more choices may come in later release)
Enter distro choice (i.e. 1, 2, 3 ...etc) to use script's URL for the
iso OR press q to quit, and optionally update URL and re-run: " 
# note: users are invited to add additional distros above!
  read distro
fi

case "$distro" in
  '--version') printf "$progname ${version}${revision}\n"; exit 0;;
  ''|'-h'|'--help'|'-?') 
		printf "$progname ${version}${revision}\n\n"
		printf "To make a FirstRibed frugal install using FR wiak skeleton initrd

Simply execute this script with command:

./$progname\n"
		printf "
Note: for vanilladpup and some other newer pups you need extra 
grub (kernel) linux argument: fwmod=usrlib

Once grub is configured you should be able to boot the new install

FOR LOGIN user:password check distro websites or try 
firstrib:firstrib or root:root; very occasionally password is blank
Rarely, you need to edit upper_changes/etc/passwd to remove root
password (which is the x in for example: root:x:0:0:root:/root:/bin/bash)\n";	exit 0;;
	'q') exit;;
esac

# Create working directories
PWD="`pwd`"
bootdir=`basename "$PWD"` # for use with grub config
mkdir -p /tmp/firstribit/miso /tmp/firstribit/miso2 /tmp/firstribit/fsroot /tmp/firstribit/merged upper_changes work

case "$distro" in
  1)	distro="KL_anypup"
		subdir="$subdirectory01"
		default_release="Press Enter to accept default or check and change"
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		if [ "$URL01" = "" ];then
			URL01=`wget -q -O - https://github.com/vanilla-dpup/releases/releases?q=vanilladpup-x86-9.2 | grep -oE 'vanilladpup.*iso' | head -n 1`
			URL01="https://github.com/vanilla-dpup/releases/releases/download/${URL01}"
			#URL01="or comment out above and manually insert url here"
		fi
		wget -c --no-check-certificate "${URL01}"
		isoname="${URL01##*/}"
		_extractiso_many  # extracts sfs files and vmlinuz from iso
		_get_FR_initrd
		wget -c --no-check-certificate https://owncloud.rockedge.org/index.php/s/SxsBbQy2cYeGGfq/download -O WDLfork_vdpup.tar
		tar xvf WDLfork_vdpup.tar
		[ $mvpoweroff = "true" ] && mv 09WDLfork_vdpup/usr/sbin/ 09WDLfork_vdpup/
		pupversion=`ls puppy* | grep -o '[^puppy].*sfs'`
		echo pupversion is $pupversion
		mv puppy${pupversion} 08puppy${pupversion}		
		mv zdrv${pupversion} 00zdrv${pupversion}
		mv fdrv${pupversion} 01fdrv${pupversion}
		mv bdrv${pupversion} 02bdrv${pupversion}
		mv adrv${pupversion} 20adrv${pupversion}
		kernelversion=`ls kbuild* | grep -o '[^kbuild-].*[^.sfs]'`		
		printf "if available, might also want:
21kbuild-${kernelversion}.sfs, 22devx_${pupversion}
23docx_${pupversion}, 24nslx_${pupversion}
24kernel_sources-${kernelversion}-kernel-kit.sfs\n"
		;;
  2)	distro="pop-os"
		subdir="$subdirectory02"
		if [ "$URL02" = "" ];then
			:
		fi
		wget -c --no-check-certificate "${URL02}" 
		sync;sync
		isoname="${URL02##*/}"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
#		_set_passwords
		;;
  3)	distro="EndeavourOS_Atlantis_neo"
		subdir="$subdirectory03"
		if [ "$URL03" = "" ];then
			URL03=`wget -q -O - https://endeavouros.com/latest-release/ | grep -oE "EndeavourOS.*iso" | head -n 1`
			URL03="https://github.com/endeavouros-team/ISO/releases/download/1-EndeavourOS-ISO-releases-archive/${URL03}"
		fi
		wget -c --no-check-certificate "${URL03}"
#		echo "Extracting large files from iso. Please wait patiently ..."
		sync;sync
		isoname="${URL03##*/}"
		_extractiso  # extracts sfs and vmlinuz from iso
#		mount "${URL03##*/}" /tmp/firstribit/miso
#		cp -a /tmp/firstribit/miso/arch/x86_64/airootfs.sfs 08airootfs.sfs
#		sync;sync
#		mount 08airootfs.sfs /tmp/firstribit/fsroot
#		cp -a /tmp/firstribit/fsroot/usr/lib/modules/5.17.1-arch1-1/vmlinuz .
		_get_FR_initrd_kmod
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
#		_set_passwords
		;;
  4)	distro="manjaro-xfce"
		subdir="$subdirectory04"
		if [ "$URL04" = "" ];then
			URL04=`wget -q -O - https://manjaro.org/download/ | grep -oE 'https://download.manjaro.org/xfce/.*iso"'`
			URL04="${URL04%\"}"
		fi
		wget -c --no-check-certificate "${URL04}"
		echo "Extracting large files from iso. Please wait patiently ..."
		sync;sync
		mount "${URL04##*/}" /tmp/firstribit/miso
		cp -a /tmp/firstribit/miso/manjaro/x86_64/rootfs.sfs 04rootfs.sfs
		cp -a /tmp/firstribit/miso/manjaro/x86_64/desktopfs.sfs 05desktopfs.sfs
		cp -a /tmp/firstribit/miso/manjaro/x86_64/mhwdfs.sfs 06mhwdfs.sfs
		cp -a /tmp/firstribit/miso/manjaro/x86_64/livefs.sfs 07livefs.sfs
		sync;sync
		mount 04rootfs.sfs /tmp/firstribit/fsroot
		cp -a /tmp/firstribit/fsroot/boot/vmlinuz* vmlinuz
		_get_FR_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
#		_set_passwords
		;;
  5)	distro="manjaro-kde"
		subdir="$subdirectory05"
		if [ "$URL05" = "" ];then
			URL05=`wget -q -O - https://manjaro.org/download/ | grep -oE 'https://download.manjaro.org/kde/.*iso"'`
			URL05="${URL05%\"}"
		fi
		wget -c --no-check-certificate "${URL05}"
		echo "Extracting large files from iso. Please wait patiently ..."
		sync;sync
		mount "${URL05##*/}" /tmp/firstribit/miso
		cp -a /tmp/firstribit/miso/manjaro/x86_64/rootfs.sfs 04rootfs.sfs
		cp -a /tmp/firstribit/miso/manjaro/x86_64/desktopfs.sfs 05desktopfs.sfs
		cp -a /tmp/firstribit/miso/manjaro/x86_64/mhwdfs.sfs 06mhwdfs.sfs
		cp -a /tmp/firstribit/miso/manjaro/x86_64/livefs.sfs 07livefs.sfs
		sync;sync
		mount 04rootfs.sfs /tmp/firstribit/fsroot
		cp -a /tmp/firstribit/fsroot/boot/vmlinuz* vmlinuz
		_get_FR_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
#		_set_passwords
		;;
  6)	distro="manjaro-gnome"
		subdir="$subdirectory06"
		if [ "$URL06" = "" ];then
			URL06=`wget -q -O - https://manjaro.org/download/ | grep -oE 'https://download.manjaro.org/gnome/.*iso"'`
			URL06="${URL06%\"}"
		fi
		wget -c --no-check-certificate "${URL06}"
		echo "Extracting large files from iso. Please wait patiently ..."
		sync;sync
		mount "${URL06##*/}" /tmp/firstribit/miso
		cp -a /tmp/firstribit/miso/manjaro/x86_64/rootfs.sfs 04rootfs.sfs
		cp -a /tmp/firstribit/miso/manjaro/x86_64/desktopfs.sfs 05desktopfs.sfs
		cp -a /tmp/firstribit/miso/manjaro/x86_64/mhwdfs.sfs 06mhwdfs.sfs
		cp -a /tmp/firstribit/miso/manjaro/x86_64/livefs.sfs 07livefs.sfs
		sync;sync
		mount 04rootfs.sfs /tmp/firstribit/fsroot
		cp -a /tmp/firstribit/fsroot/boot/vmlinuz* vmlinuz
		_get_FR_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
#		_set_passwords
		;;
  7)	distro="MakuluLinux-Shift-x64-U"
		subdir="$subdirectory07"
		if [ "$URL07" = "" ];then
			URL07=`wget -q -O - https://www.makululinux.com/wp/download/ | grep -oE 'https://sourceforge.*iso' | cut -d'>' -f2`
		fi
		wget -c --no-check-certificate "${URL07}"
		echo "Extracting large files from iso. Please wait patiently ..."
		sync;sync
		mount "${URL07##*/}" /tmp/firstribit/miso
		cp -a /tmp/firstribit/miso/live/filesystem.squashfs 08filesystem.sfs
		sync;sync
		mount 08filesystem.sfs /tmp/firstribit/fsroot
		cp -a /tmp/firstribit/fsroot/boot/vmlinuz* vmlinuz
		_get_FR_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  8)	distro="MX" # XFCE stable for PC a few years old
		subdir="$subdirectory08"
		if [ "$URL08" = "" ];then
			URL08=`wget -q -O - https://mxlinux.org/download-links/ | grep -oE 'https://sourceforge.net/projects/mx-linux/files/Final/Xfce/.*iso' | cut -dd -f1`
			URL08="${URL08%/}"
		fi
		wget -c --no-check-certificate "${URL08}"
		echo "Extracting large files from iso. Please wait patiently ..."
		sync;sync
		mount "${URL08##*/}" /tmp/firstribit/miso
		cp -a /tmp/firstribit/miso/antiX/linuxfs 08linuxfs.sfs
		sync;sync
		mount 08linuxfs.sfs /tmp/firstribit/fsroot
		cp -a /tmp/firstribit/fsroot/boot/vmlinuz* vmlinuz
		_get_FR_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  9)	distro="MX" # XFCE advanced hardware support (ahs) for newer machines
		subdir="$subdirectory09"
		if [ "$URL09" = "" ];then
			URL09=`wget -q -O - https://mxlinux.org/download-links/ | grep -oE 'https://sourceforge.net/projects/mx-linux/files/Final/Xfce/.*iso' | cut -d'"' -f5`
		fi
		wget -c --no-check-certificate "${URL09}"
		echo "Extracting large files from iso. Please wait patiently ..."
		sync;sync
		mount "${URL09##*/}" /tmp/firstribit/miso
		cp -a /tmp/firstribit/miso/antiX/linuxfs 08linuxfs.sfs
		sync;sync
		mount 08linuxfs.sfs /tmp/firstribit/fsroot
		cp -a /tmp/firstribit/fsroot/boot/vmlinuz* vmlinuz
		_get_FR_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  10)	distro="MX" # KDE with advanced hardware support (ahs) for newer machines
		subdir="$subdirectory10"
		if [ "$URL10" = "" ];then
			URL10=`wget -q -O - https://mxlinux.org/download-links/ | grep -oE 'https://sourceforge.net/projects/mx-linux/files/Final/KDE/.*iso' | cut -dd -f1`
		fi
		wget -c --no-check-certificate "${URL10}"
		echo "Extracting large files from iso. Please wait patiently ..."
		sync;sync
		mount "${URL10##*/}" /tmp/firstribit/miso
		cp -a /tmp/firstribit/miso/antiX/linuxfs 08linuxfs.sfs
		sync;sync
		mount 08linuxfs.sfs /tmp/firstribit/fsroot
		cp -a /tmp/firstribit/fsroot/boot/vmlinuz* vmlinuz
		_get_FR_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  11)	distro="MX" # fluxbox with advanced hardware support (ahs) for newer machines
		subdir="$subdirectory11"
		if [ "$URL11" = "" ];then
			URL11=`wget -q -O - https://mxlinux.org/download-links/ | grep -oE 'https://sourceforge.net/projects/mx-linux/files/Final/Fluxbox/.*x64.iso' | cut -dd -f1`
		fi
		wget -c --no-check-certificate "${URL11}"
		echo "Extracting large files from iso. Please wait patiently ..."
		sync;sync
		mount "${URL11##*/}" /tmp/firstribit/miso
		cp -a /tmp/firstribit/miso/antiX/linuxfs 08linuxfs.sfs
		sync;sync
		mount 08linuxfs.sfs /tmp/firstribit/fsroot
		cp -a /tmp/firstribit/fsroot/boot/vmlinuz* vmlinuz
		_get_FR_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  12)	distro="MX" # fluxbox with advanced hardware support (ahs) for newer machines
		subdir="$subdirectory12"
		if [ "$URL12" = "" ];then
			URL12=`wget -q -O - https://mxlinux.org/download-links/ | grep -oE 'https://sourceforge.net/projects/mx-linux/files/Final/Fluxbox/.*386.iso' | cut -d'"' -f3`
		fi
		wget -c --no-check-certificate "${URL12}"
		echo "Extracting large files from iso. Please wait patiently ..."
		sync;sync
		mount "${URL12##*/}" /tmp/firstribit/miso
		cp -a /tmp/firstribit/miso/antiX/linuxfs 08linuxfs.sfs
		sync;sync
		mount 08linuxfs.sfs /tmp/firstribit/fsroot
		cp -a /tmp/firstribit/fsroot/boot/vmlinuz* vmlinuz
		_get_FR_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  13)	distro="Zorin-OS"
		subdir="$subdirectory13"
		if [ "$URL13" = "" ];then
			:
		fi
		wget -c --no-check-certificate "${URL13}"
		echo "Extracting large files from iso. Please wait patiently ..."
		sync;sync
		mount "${URL13##*/}" /tmp/firstribit/miso
		cp -a /tmp/firstribit/miso/casper/filesystem.squashfs 08filesystem.sfs
		sync;sync
		mount 08filesystem.sfs /tmp/firstribit/fsroot
		cp -a /tmp/firstribit/fsroot/boot/vmlinuz-* vmlinuz
		_get_FR_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords "/tmp/firstribit/fsroot"
		;;
  14)	distro="neon-user"
		subdir="$subdirectory14"
		if [ "$URL14" = "" ];then
			URL14=`wget -q -O - https://neon.kde.org/download | grep -oE 'https://files.kde.org/neon/images.*iso' | head -n 1`
		fi
		wget -c --no-check-certificate "${URL14}"
		echo "Extracting large files from iso. Please wait patiently ..."
		sync;sync
		mount "${URL14##*/}" /tmp/firstribit/miso
		cp -a /tmp/firstribit/miso/casper/filesystem.squashfs 08filesystem.sfs
		sync;sync
		mount 08filesystem.sfs /tmp/firstribit/fsroot
		cp -a /tmp/firstribit/miso/casper/vmlinuz vmlinuz
		_get_FR_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  15)	distro="garuda"
		subdir="$subdirectory15"
		if [ "$URL15" = "" ];then
			:
		fi
		wget -c --no-check-certificate "${URL15}"
		echo "Extracting large files from iso. Please wait patiently ..."
		sync;sync
		mount "${URL15##*/}" /tmp/firstribit/miso
		cp -a /tmp/firstribit/miso/${distro}/x86_64/rootfs.sfs 04rootfs.sfs
		cp -a /tmp/firstribit/miso/${distro}/x86_64/desktopfs.sfs 05desktopfs.sfs
		cp -a /tmp/firstribit/miso/${distro}/x86_64/mhwdfs.sfs 06mhwdfs.sfs
		cp -a /tmp/firstribit/miso/${distro}/x86_64/livefs.sfs 07livefs.sfs
		sync;sync
		mount 04rootfs.sfs /tmp/firstribit/fsroot
		cp -a /tmp/firstribit/fsroot/boot/vmlinuz* vmlinuz
		_get_FR_initrd_kmod
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
#		_set_passwords
		;;
  16)	distro="bodhi"
		subdir="$subdirectory16"
		if [ "$URL16" = "" ];then
			URL16=`wget -q -O - https://www.bodhilinux.com/download/ | grep -oE 'https://sourceforge.net/projects/bodhilinux/files.*64.iso' | sed 's@/download@\n@' | head -n 1`
		fi
		wget -c --no-check-certificate "${URL16}"
		echo "Extracting large files from iso. Please wait patiently ..."
		sync;sync
		mount "${URL16##*/}" /tmp/firstribit/miso
		cp -a /tmp/firstribit/miso/casper/filesystem.squashfs 08filesystem.sfs
		sync;sync
		mount 08filesystem.sfs /tmp/firstribit/fsroot
		cp -a /tmp/firstribit/miso/casper/vmlinuz vmlinuz
		_get_FR_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  17)	distro="massos"
		subdir="$subdirectory17"
		if [ "$URL17" = "" ];then
			URL17=`wget -q -O - https://github.com/MassOS-Linux/MassOS/releases | grep -oE 'https://github.com/MassOS-Linux/MassOS/releases.*xfce.iso'`
		fi
		wget -c --no-check-certificate "${URL17}"
		echo "Extracting large files from iso. Please wait patiently ..."
		sync;sync
		mount "${URL17##*/}" /tmp/firstribit/miso
		# horrible hack, but works to mount squashfs.img as 08filesystem.sfs later:
		cp -a /tmp/firstribit/miso/LiveOS/squashfs.img 08filesystem.sfs
		sync;sync
		mount 08filesystem.sfs /tmp/firstribit/fsroot
		cp -a /tmp/firstribit/miso/casper/vmlinuz vmlinuz
		_get_FR_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
#		_set_passwords
		;;
  18)	distro="TinyCorePure64-current"LXLE-Focal 
		subdir="$subdirectory18"
		# tcz repos via: MIRROR="${MIRROR%/}/$(getMajorVer).x/$BUILD/tcz"
		# where MIRROR=repo.tinycorelinux.net and BUILD=x86_64
		# e.g. https://repo.tinycorelinux.net/13.x/x86_64/tcz/file.tcz
		# and check dependencies in file file.tcz.dep
		# or fetch via: https://ftp.nluug.nl/os/Linux/distr/tinycorelinux/
		
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		if [ "$URL18" = "" ];then
			:
		fi
		wget -c --no-check-certificate "${URL18}"
		echo "Extracting large files from iso. Please wait patiently ..."
		sync;sync
		mount "${URL18##*/}" /tmp/firstribit/miso
		cp -a /tmp/firstribit/miso/cde/optional/* .
		rm -f *.md5.txt
		c=11
		for f in *.tcz; do  # rename required tcz apps to numbered sfs form
		  mv "$f" "${c}${f}.sfs"
		  c=$((c+1))
		done
		mkdir -p 08core # for core TCL root filesystem (uncompressed)
		cp -a /tmp/firstribit/miso/boot/corepure64.gz 08core/
		cd 08core && zcat corepure64.gz | cpio -idm
		sync;sync
		# sed -i.bak '/USER="tc"/iNOZSWAP=1' etc/init.d/tc-config
		# now done by /usr/local/tc_installed.sh as is echo Xfbdev > etc/sysconfig/Xserver
		rm -f corepure64.gz
		cd ..
		_get_FR_initrd
		wget -c --no-check-certificate https://owncloud.rockedge.org/index.php/s/gK3wm2Z7QdoqHe7/download -O WDLfork_TCL.tar
		tar xvf WDLfork_TCL.tar
		sync;sync
		# fetch KLV-Airedale kernel components (TCL kernel has no overlayfs)
		echo "Fetching huge kernel/modules/firmware. Please wait patiently ..."
		mkdir -p kernelmodules
		cd kernelmodules
		wget -c --no-check-certificate https://rockedge.org/kernels/data/Kernels/64bit/5.14.1_untested/huge-5.14.1-bionicpup64.tar.bz2
		tar xjvf huge-5.14.1-bionicpup64.tar.bz2
		mv vmlinuz-5.14.1-bionicpup64 ../vmlinuz
		mv kernel-modules-5.14.1-bionicpup64.sfs ../00kernel-modules-5.14.1-bionicpup64.sfs
		mv fdrv-5.14.1-bionicpup64.sfs ../01fdrv-5.14.1-bionicpup64.sfs
		rm -f WDLfork_vdpup.tar
		sync;sync
		cd ..
		;;
  19)	distro="Airedale"
		subdir="$subdirectory19"
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		if [ "$URL19" = "" ];then
			URL19=`wget -q -O - https://forum.puppylinux.com/viewtopic.php?t=5215 | grep -oE 'https://rockedge.org/kernels/data/ISO/Kennel_Linux/Airedale.*iso'`
		fi
		wget -c --no-check-certificate "${URL19}"
		echo "Extracting large files from iso. Please wait patiently ..."
		sync;sync
		mount "${URL19##*/}" /tmp/firstribit/miso
		cp -a /tmp/firstribit/miso/* .
		;;
		# note: users are invited to add additional distros above!
  20)	distro="clearlinux"
		subdir="$subdirectory20"
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		if [ "$URL20" = "" ];then
			URL20=`wget -q -O - https://clearlinux.org/downloads | grep -oE 'https://cdn.download.clearlinux.org/releases.*desktop.iso' | head -n 1`
		fi
		wget -c --no-check-certificate "${URL20}"
		echo "Extracting large files from iso. Please wait patiently ..."
		sync;sync
		mount "${URL20##*/}" /tmp/firstribit/miso
		# horrible hack, but works to mount raw img "rootfs.img" as 08filesystem.sfs later:
		cp -a /tmp/firstribit/miso/images/rootfs.img 08filesystem.sfs
		sync;sync
		mount 08filesystem.sfs /tmp/firstribit/fsroot
		cp -a /tmp/firstribit/miso/kernel/kernel.xz vmlinuz
		_get_FR_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
#		_set_passwords
		;;
  21)	distro="Ubuntu"
		subdir="$subdirectory21"
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		if [ "$URL21" = "" ];then
			:
		fi
		wget -c --no-check-certificate "${URL21}" 
		sync;sync
		isoname="${URL21##*/}"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  22)	distro="MintCin"
		subdir="$subdirectory22"
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		if [ "$URL22" = "" ];then
			:
		fi
		wget -c --no-check-certificate "${URL22}" 
		sync;sync
		isoname="${URL22##*/}"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  23)	distro="linux-lite"
		subdir="$subdirectory23"
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		if [ "$URL23" = "" ];then
			:
		fi
		wget -c --no-check-certificate "${URL23}" 
		sync;sync
		isoname="${URL23##*/}"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  24)	distro="bunsen-lithium"
		subdir="$subdirectory24"
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		if [ "$URL24" = "" ];then
			URL24=`wget -q -O - https://www.bunsenlabs.org/installation.html | grep -oE 'https://ddl.bunsenlabs.org/ddl/.*hybrid.iso' | head -n 1`
		fi
		wget -c --no-check-certificate "${URL24}" 
		sync;sync
		isoname="${URL24##*/}"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  25)	distro="LXLE-Focal"
		subdir="$subdirectory25"
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		if [ "$URL25" = "" ];then
			URL25=`wget -q -O - https://www.lxle.net/download/ | grep -oE 'https://sourceforge.net/projects/lxle/files/Final/.*Release.iso'`
		fi
		wget -c --no-check-certificate "${URL25}" 
		sync;sync
		isoname="${URL25##*/}"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  26)	distro="tails-amd64"
		subdir="$subdirectory26"
		if [ "$URL26" = "" ];then
			URL26=`wget -q -O - https://tails.boum.org/install/download/ | grep -oE 'https://mirrors.wikimedia.org/tails/stable/tails-amd64.*.img'`
		fi
		wget -c --no-check-certificate "${URL26}" 
		sync;sync
		isoname="${URL26##*/}"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  27)	distro="Debian"
		subdir="$subdirectory27"
		if [ "$URL27" = "" ];then
			URL27=`wget -q -O - https://cdimage.debian.org/debian-cd/current-live/amd64/iso-hybrid | grep -oE 'debian-live.*amd64-xfce.iso' | head -n 1 | cut -d\" -f1`
			URL27="https://cdimage.debian.org/debian-cd/current-live/amd64/iso-hybrid/${URL27}"
		fi
		wget -c --no-check-certificate "${URL27}" 
		sync;sync
		isoname="${URL27##*/}"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  28)	distro="Regata_OS"
		subdir="$subdirectory28"
		if [ "$URL28" = "" ];then
			URL28=`wget -q -O - https://www.regataos.com/p/download.html | grep -oE 'https://osdn.net/dl/regataos.*.iso' | head -n 1`
			#URL28="https://pt.osdn.net/dl/regataos/Regata_OS_22-nv_en-US.x86_64-22.0.3.iso" # this alternative for modern NVIDIA support
		fi
		wget -c --no-check-certificate "${URL28}" 
		sync;sync
		isoname="${URL28##*/}"
		# Needing extra img decompress so cannot use this here: _extractiso
		echo "Extracting large files from iso. Please wait patiently ..."
		mount "$isoname" /tmp/firstribit/miso
		sleep 1
		mount /tmp/firstribit/miso/LiveOS/squashfs.img /tmp/firstribit/miso2
		sleep 1
		mount /tmp/firstribit/miso2/LiveOS/rootfs.img /tmp/firstribit/fsroot		
		sleep 1
		cp -a $(find /tmp/firstribit/fsroot -name "vmlinuz*default") vmlinuz
		mksquashfs /tmp/firstribit/fsroot/ 08filesystem.sfs
		_get_FR_initrd_kmod
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
#		_set_passwords (password is blank, can: sudo password root to user:pw root:root)
		;;
  29)	distro="Kodachi"
		subdir="$subdirectory29"
		if [ "$URL29" = "" ];then
			:
		fi
		wget -c --no-check-certificate "${URL29}" 
		sync;sync
		isoname="${URL29##*/}"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  30)	distro="SysLinuxOS"
		subdir="$subdirectory30"
		if [ "$URL30" = "" ];then
			URL30=`wget -q -O - https://syslinuxos.com/ | grep -oE 'https://sourceforge.net/projects/syslinuxos/files/.*.iso' | head -n 1`
		fi
		wget -c --no-check-certificate "${URL30}" 
		sync;sync
		isoname="${URL30##*/}"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  31)	distro="4MLinux"
		subdir="$subdirectory31"
		if [ "$URL31" = "" ];then
			:
		fi
		wget -c --no-check-certificate "${URL31}" 
		sync;sync
		isoname="${URL31##*/}"
		# Needing extra img decompress so cannot use this here: _extractiso
		echo "Extracting large files from iso. Please wait patiently ..."
		mount "$isoname" /tmp/firstribit/miso
		sleep 1
		mkdir -p 08initrd_decomp
		cd 08initrd_decomp
		zcat /tmp/firstribit/miso/boot/initrd.gz | cpio -idm
		tar xJvf /tmp/firstribit/miso/drivers/addon_modules-all*
		tar xJvf /tmp/firstribit/miso/drivers/addon_firmware*
		k=`ls lib/modules`
		depmod -a $k -b .
		cd ..
		mkdir -p initrd2_decomp
		cd initrd2_decomp
		zcat /tmp/firstribit/miso/boot/initrd2.gz | cpio -idm
		cd ..
		cp -a /tmp/firstribit/miso/boot/bzImage vmlinuz		
		_get_FR_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a ../08initrd_decomp/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		# _set_passwords (password is blank or maybe austrumi, can: sudo password root to user:pw root:root)
		;;
  32)	distro="austrumi"
		subdir="$subdirectory32"
		if [ "$URL32" = "" ];then
			URL32=`wget -q -O - https://austrumi.ru.lv/ | sed 's/a href=\"//' |  grep -o -E "[^<>]*austrumi64.*iso" | sort -V | tail -n 1 | grep -o austrumi64.*iso`
			URL32="https://austrumi.ru.lv/${URL32}"
		fi
		wget -c --no-check-certificate "${URL32}"
		echo "Extracting large files from iso. Please wait patiently ..."
		sync;sync
		mount "${URL32##*/}" /tmp/firstribit/miso
		sleep 1
		for i in austrumi.fs bzImage intel-ucode.cpio amd-ucode.cpio austrumi.tgz message.msg; do
			cp -a /tmp/firstribit/miso/austrumi/${i} .
		done
		sync;sync
		unsquashfs -d 08filesystem austrumi.fs
		mv bzImage vmlinuz
		tar xzvf austrumi.tgz -C 08filesystem
		cd 08filesystem/boot
		cat > FR_aust_rc.sysinit << "CODE_FOR_ROOTFS_RC_SYSINIT"
#!/bin/sh
# FR_austrumi_rc.sysinit (12 Aug 2022) is slight modification of following build_wiak_initrd script:
# rc.sysinit: Copyright William McEwan (wiak) 16 July 2019; Licence MIT (aka X11 license). Revision 2.0.9 28 Dec 2020

# Add to PATH in case not done. /usr/local/firstrib/bin last in preference here so overwritten by same-named core apps
export PATH=/usr/local/bin:/usr/local/sbin:/sbin:/usr/sbin:/bin:/usr/bin
# The first part of the following is modified/skeleton extract from
# Void Linux /etc/runit/core-services/00-pseudofs.sh so ready for runit mod if wanted
mkdir -p -m0755 /proc /sys/kernel/security /run /dev /tmp
#msg "Mounting pseudo-filesystems..."
mountpoint -q /proc || mount -o nosuid,noexec,nodev -t proc proc /proc
mountpoint -q /sys || mount -o nosuid,noexec,nodev -t sysfs sys /sys
mountpoint -q /run || mount -o mode=0755,nosuid,nodev,size=$((`free | grep 'Mem: ' | tr -s ' ' | cut -f 4 -d ' '`/4))k -t tmpfs run /run
mountpoint -q /dev || mount -o mode=0755,nosuid -t devtmpfs dev /dev
mkdir -p -m0755 /run/runit /run/lvm /run/user /run/lock /run/log /dev/pts /dev/shm
mountpoint -q /dev/pts || mount -o mode=0620,gid=5,nosuid,noexec -n -t devpts devpts /dev/pts
mountpoint -q /dev/shm || mount -o mode=1777,nosuid,nodev,size=$((`free | grep 'Mem: ' | tr -s ' ' | cut -f 4 -d ' '`/4))k -n -t tmpfs shm /dev/shm
mountpoint -q /tmp || mount -t tmpfs -o mode=1777,nosuid,nodev,size=$((`free | grep 'Mem: ' | tr -s ' ' | cut -f 4 -d ' '`/4))k tmpfs /tmp
mountpoint -q /sys/kernel/security || mount -n -t securityfs securityfs /sys/kernel/security
[ -x /etc/rc.local ] && /etc/rc.local	# If /etc/rc.local script exists and is executable, run it
										# User can add custom commands into that script
echo "Starting udev and waiting for devices to settle..." >/dev/console
udevd --daemon
udevadm trigger --action=add --type=subsystems
udevadm trigger --action=add --type=devices
udevadm settle
exec /sbin/init 4 </dev/console >/dev/console 2>&1
exit
CODE_FOR_ROOTFS_RC_SYSINIT
		chmod +x FR_aust_rc.sysinit
		cd ..
		cp -af etc/skel/. root
		sed -i "s@proc/cmdline)@proc/cmdline | grep -oE 'lang_.*[^ ]')@" etc/rc.d/rc.M
		cd ..
		_get_FR_initrd
		sed -i 's@/sbin/init@/boot/FR_aust_rc.sysinit@' w_init
#		_set_passwords2 "08filesystem"
		echo "Result being compressed. Please wait patiently ..."
		# doesn't mount it when sfs!!! so no good: mksquashfs 08filesystem/ 08filesystem.sfs
		sync;sync
		# doesn't mount it when sfs!!! so no good: rm -rf 08filesystem/ austrumi.fs austrumi.tgz 
		rm -rf austrumi.fs austrumi.tgz
		;;
  33)	distro="Fedora Rawhide"
		subdir="$subdirectory33"
		# Currently auto-downloading and using fdstrap () to create the base fedora rootfs build.
		# It simply downloads Fedora provided tar.gz of fedora rawhide filesystem. https://gitlab.com/tearch-linux/applications-and-tools
		if [ "$URL33" = "" ];then
			:
		fi
		wget -c https://gitlab.com/tearch-linux/applications-and-tools/fdstrap/-/raw/main/fdstrap.sh && chmod +x fdstrap.sh
		sed -i 's@fedora-release@fedora-release kernel linux-firmware NetworkManager NetworkManager-tui NetworkManager-wifi@' fdstrap.sh
		./fdstrap.sh 08firstrib_rootfs
		_get_FR_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a ../08firstrib_rootfs/usr/lib/modules/* usr/lib/modules/
		cp -a ../08firstrib_rootfs/boot/vmlinuz* ../vmlinuz
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords2 "08firstrib_rootfs"
		;;
  34)	distro="xubuntu"
		subdir="$subdirectory34"
		if [ "$URL34" = "" ];then
			:
		fi
		wget -c --no-check-certificate "${URL34}" 
		sync;sync
		isoname="${URL34##*/}"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  35)	distro="lubuntu"
		subdir="$subdirectory35"
		if [ "$URL35" = "" ];then
			:
		fi
		wget -c --no-check-certificate "${URL35}" 
		sync;sync
		isoname="${URL35##*/}"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/usr/lib/modules/* usr/lib/modules/
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  36)	distro="mabox" # thanks TerryH
		subdir="$subdirectory36"
		if [ "$URL36" = "" ];then
			# In practice not using this because gives the older 5.4 LTS kernel so using manual URL36 at top of script
			URL36=`wget -q -O - https://sourceforge.net/projects/mabox-linux/files/ | grep -oE 'title=\"/.*mabox.*.iso' | sed 's@title="/@@'`
			URL36="https://sourceforge.net/projects/mabox-linux/files/${URL36}"
		fi
		wget -c --no-check-certificate "${URL36}"
		echo "Extracting large files from iso. Please wait patiently ..."
		sync;sync
		mount "${URL36##*/}" /tmp/firstribit/miso
		cp -a /tmp/firstribit/miso/manjaro/x86_64/rootfs.sfs 04rootfs.sfs
		cp -a /tmp/firstribit/miso/manjaro/x86_64/desktopfs.sfs 05desktopfs.sfs
		cp -a /tmp/firstribit/miso/manjaro/x86_64/mhwdfs.sfs 06mhwdfs.sfs
		cp -a /tmp/firstribit/miso/manjaro/x86_64/livefs.sfs 07livefs.sfs
		sync;sync
		mount 04rootfs.sfs /tmp/firstribit/fsroot
		cp -a /tmp/firstribit/fsroot/boot/vmlinuz* vmlinuz
		_get_FR_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
#		_set_passwords
		;;
  37)	distro="sparky minimalgui" # Thanks esos
		subdir="$subdirectory37"
		if [ "$URL37" = "" ];then
			URL37=`wget -q -O - https://sparkylinux.org/download/rolling/ | grep -oE 'https://osdn.net/dl/sparkylinux/sparkylinux.*x86_64-minimalgui.*iso'`
		fi
		wget -c --no-check-certificate "${URL37}" 
		sync;sync
		isoname="${URL37##*/}"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  38)	distro="sparky lxqt"
		subdir="$subdirectory38"
		if [ "$URL38" = "" ];then
			URL38=`wget -q -O - https://sparkylinux.org/download/rolling/ | grep -oE 'https://osdn.net/dl/sparkylinux/sparkylinux.*x86_64-lxqt.*iso'`
		fi
		wget -c --no-check-certificate "${URL38}" 
		sync;sync
		isoname="${URL38##*/}"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  39)	distro="sparky mate"
		subdir="$subdirectory39"
		if [ "$URL39" = "" ];then
			URL39=`wget -q -O - https://sparkylinux.org/download/rolling/ | grep -oE 'https://osdn.net/dl/sparkylinux/sparkylinux.*x86_64-mate.*iso'`
		fi
		wget -c --no-check-certificate "${URL39}" 
		sync;sync
		isoname="${URL39##*/}"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  40)	distro="sparky xfce"
		subdir="$subdirectory40"
		if [ "$URL40" = "" ];then
			URL40=`wget -q -O - https://sparkylinux.org/download/rolling/ | grep -oE 'https://osdn.net/dl/sparkylinux/sparkylinux.*x86_64-xfce.*iso'`
		fi
		wget -c --no-check-certificate "${URL40}" 
		sync;sync
		isoname="${URL40##*/}"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  41)	distro="sparky kde"
		subdir="$subdirectory41"
		if [ "$URL41" = "" ];then
			URL41=`wget -q -O - https://sparkylinux.org/download/rolling/ | grep -oE 'https://osdn.net/dl/sparkylinux/sparkylinux.*x86_64-kde.*iso'`
		fi
		wget -c --no-check-certificate "${URL41}" 
		sync;sync
		isoname="${URL41##*/}"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  42)	distro="sparky gameover"
		subdir="$subdirectory42"
		if [ "$URL42" = "" ];then
			URL42=`wget -q -O - https://sparkylinux.org/download/rolling/ | grep -oE 'https://osdn.net/dl/sparkylinux/sparkylinux.*x86_64-gameover.*iso'`
		fi
		wget -c --no-check-certificate "${URL42}" 
		sync;sync
		isoname="${URL42##*/}"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  43)	distro="sparky multimedia"
		subdir="$subdirectory43"
		if [ "$URL43" = "" ];then
			URL43=`wget -q -O - https://sparkylinux.org/download/rolling/ | grep -oE 'https://osdn.net/dl/sparkylinux/sparkylinux.*x86_64-multimedia.*iso'`
		fi
		wget -c --no-check-certificate "${URL43}" 
		sync;sync
		isoname="${URL43##*/}"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_FR_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/firstribit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
	*)	printf "$progname ${version}${revision}\n"; exit 0;;
esac

# clean up
sync;sync
umount -l /tmp/firstribit/merged
umount -l /tmp/firstribit/fsroot
umount -l /tmp/firstribit/miso
umount -l /tmp/firstribit/miso2
rm -rf /tmp/firstribit/
rm -rf initrd_decompressed

_grub_config
exit 0
