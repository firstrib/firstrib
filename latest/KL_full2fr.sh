#!/bin/bash
progname="KL_full2fr.sh"; version="001"; revision="-rc1" #Revision Date: 21Jul2023
# To make an optional grub2-bootable KL firstrib-based frugal install
# from a full installed Linux distro (such as Linux Mint) 
# using FirstRib initrd to provide overlayfs full frugal install functionality

# Creation Date: 21Jul2023
# Copyright wiak (William McEwan) 14feb2022+; Licence MIT (aka X11 license)

# Usual Disclaimers: Use this at your own risk entirely. This works for me without damaging anything, but
# I take no responsibility for its effects on your system. This is a system boot level component so
# don't use unless you are confident you know what you are doing and are willing to take any risk involved.
# I have purposively left the simple final grub menu modification for the user to undertake manually
# though you can always automate some or all of that yourself later, at your own risk, if you so wish.

# Installation instructions:
# 1. Boot your target mainstream full installed distro (e.g. Linux Mint)
# 2. In that running distro run command: sudo mkdir -p /KL_full2fr
# 3. Put KL_full2fr.sh script inside /KL_full2fr and from terminal opened as
# root user inside that directory run script with command:
#   ./KL_full2fr.sh (or: ./KL_full2fr.sh --help for this help text)
# 4. Wait patiently till script builds boot components and outputs grub2 stanza.
# 5. Still with root user permissions, append that grub2 stanza
# to (end of) mainstream distro file: /etc/grud.d/40_custom
# and run command: sudo update-grub
# 6. On reboot you should find that new grub2 menu entry to 
# optionally frugal boot your mainstream distro.
# You can still instead boot your mainstream distro as
# normal full installed distro whenever you so should wish.
# Notes:
# By default the KL_full2fr will boot with no persistence RAM0 mode.
# If you wish to use save on demand RAM2 mode you will need to set
# up grub2 to put upper_changes folder on a separate partition.
# For further information read the produced /KL_full2fr/grub_config.txt

# Based on firstribit script (wiak 2022) code:

distro01="Linux Mint XFCE"
distro02="Other full installed distro"


_extract_kernel () {
    echo "Taking copy of kernel for use of KLfrugal. Please wait patiently ..."
    cp -a $(find /boot -name "vmlinuz?*" | tail -n 1) vmlinuz
    sync;sync
}

_KL_full2fr (){
  cd $HERE
  _extract_kernel
  echo "Fetching FirstRib boot components. Please wait patiently ..."
  wget -c https://gitlab.com/firstrib/firstrib/-/raw/master/latest/KL_full2fr.tar.gz
  result=$?
  if [ $result -ne 0 ]; then
    printf "\nFile likely not found; please report\n\n"
    exit 1
  fi
  tar xvf KL_full2fr.tar.gz
  sync;sync
  mv initrd-latest.gz initrdSKEL.gz
  mkdir -p initrd_decompressed
  cd initrd_decompressed
  sync;sync
  zcat ../initrdSKEL.gz | cpio -idm
  echo "Copying modules and compressing initrd.gz; Please wait patiently ..."
  cp -a /lib/modules/* usr/lib/modules/		
  sync;sync
  find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz
  sync;sync
  cd $HERE # back to directory /KL_full2fr
}

_grub_config (){
	cd "$HERE"
	subdir="$bootdir"
	bootuuid=`df . | awk '/^\/dev/ {print $1}' | xargs blkid -s UUID | awk -F\" '{print $2}'`
	bootlabel=`df . | awk '/^\/dev/ {print $1}' | xargs blkid -s LABEL | awk -F\" '{print $2}'`
	printf "
Assuming names: kernel is vmlinuz and initrd is initrd.gz and booting is
from this build directory and needed modules and firmware are present
with root permissions (e.g. sudo) append the following stanza to 
your /etc/grub.d/40_config and run command: sudo update-grub

menuentry \"${subdir}\" {
  insmod ext2
  search --no-floppy --fs-uuid --set $bootuuid
  linux /$subdir/vmlinuz w_bootfrom=UUID=${bootuuid}=/$subdir w_changes=RAM0 w_bottom=full2fr
  initrd /$subdir/initrd.gz
}

Refer to ${HERE}/grub_config.txt for
copy of this information plus blkid info.
Note that you can only use RAM2 save on demand mode with w_changes putting
upper_changes into a sessions folder on a separate partition. For example:
w_changes=UUID=different_partition_uuid=/sessions
w_changes1=RAM2 w_bottom=full2fr\n\n" | tee grub_config.txt
	blkid -s UUID >> grub_config.txt
# Optional for inclusion in the above:
#menuentry \"${subdir}\" {
#  insmod ext2
#  search --no-floppy --label $bootlabel --set
#  linux /$subdir/vmlinuz w_bootfrom=LABEL=${bootlabel}=/$subdir w_changes=RAM2
#  initrd /$subdir/initrd.gz
#}
# OR, using different partition for upper_changes persistence with /sessions folder:
# linux /$subdir/vmlinuz w_bootfrom=UUID=${bootuuid}=/$subdir w_changes=UUID=different_partition_uuid=/sessions w_changes1=RAM2 w_bottom=full2fr
}

if [ $1 ];then
  distro="$1"
else
  printf "
Make a KL firstrib-based frugal install using FR skeleton initrd
Does not prevent the normal operation of underlying full install
Choose a distro to KL_frugalit:

1 $distro01
2 $distro02
q quit (more choices may come in later release)
Enter distro choice (i.e. 1, 2, 3 ...etc)
OR press q to quit: " 
  read distro
fi

case "$distro" in
  '--version') printf "$progname ${version}${revision}\n"; exit 0;;
  ''|'-h'|'--help'|'-?') 
		printf "$progname ${version}${revision}\n\n"
		printf "Makes an optional grub2-bootable KL firstrib-based frugal install
from a full installed Linux distro (such as Linux Mint) using
FirstRib initrd to provide overlayfs full frugal install functionality
Installation instructions:
1. Boot your target mainstream full installed distro (e.g. Linux Mint)
2. In that running distro run command: sudo mkdir -p /KL_full2fr
3. Put KL_full2fr.sh script inside /KL_full2fr and from terminal opened as
root user inside that directory run script with command:
   ./$progname (or: ./$progname --help for this help text)
4. Wait patiently till script builds boot components and outputs grub2 stanza.
5. Still with root user permissions, append that grub2 stanza
to (end of) mainstream distro file: /etc/grud.d/40_custom
and run command: sudo update-grub
6. On reboot you should find that new grub2 menu entry to 
optionally frugal boot your mainstream distro.
You can still instead boot your mainstream distro as
normal full installed distro whenever you so should wish.
Notes:
By default the KL_full2fr will boot with no persistence RAM0 mode.
If you wish to use save on demand RAM2 mode you will need to set
up grub2 to put upper_changes folder on a separate partition.
For further information read the produced /KL_full2fr/grub_config.txt
\n"; exit 0;;
	'q') exit;;
esac

# where we started
HERE="`pwd`"
if [ $HERE == "/" ]; then
  printf "\nYou should only run this script in an immediate empty sub-directory of dir /
For example: sudo mkdir -p /KL_full2fr and run the script from
a terminal opened as root user from inside that directory\n"
  exit 1
fi
bootdir=`basename "$HERE"` # for use with grub config

# Using unique cases in case some distro types needs special treatment
case "$distro" in
  1)	_KL_full2fr
		;;
  2)	_KL_full2fr
		;;
	*)	printf "$progname ${version}${revision}\n"; exit 0;;
esac

# clean up
sync;sync
rm -rf initrd_decompressed

_grub_config
exit 0
